<?
if ($_SERVER['REQUEST_METHOD']=="POST") {	
	$xmlContent = $_POST['xmlContent'];
	// sanitize content for servers where magic_quotes_gdc is on
	$xmlContent = stripslashes($xmlContent);
	$xmlHeader = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	$xmlFooter = "";
	global $xmlFile;
	writePHP($xmlHeader . $xmlContent . $xmlFooter, "newSpecs.xml");
	//include "meta.php";
}
else {
?>
<html>
	<head>
		<title>Indoor Outdoor &middot; Spec Sheet Writer</title>
	<link rel='stylesheet' type='text/css' href='admin/main.css' />
	<script src='js/jquery.min.js' type='text/javascript'></script>
	<script src='xml_actions.js' type='text/javascript'></script>
	</head>
	<body>
	<div id='specsWriteContainer'>
		<form action='write_xml.php' method='post'>
		<div id='siteName' class='header'>Site Name : <input class='inputField' type='text' name='sitename'></div>
		<input class='inputField' type='hidden' name='globalsStart'>
		<div class='header'>Global Variables</div>
		<div id='globalsContainer'>
			<div class='globalVar'>Name : <div class='reqGlobal'>Images_Path</div><input class="inputField" type="hidden" name="globalName" value="Images_Path"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Thumbs_Path</div><input class="inputField" type="hidden" name="globalName" value="Thumbs_Path"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Audio_Path</div><input class="inputField" type="hidden" name="globalName" value="Audio_Path"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Movies_Path</div><input class="inputField" type="hidden" name="globalName" value="Movies_Path"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Host</div><input class="inputField" type="hidden" name="globalName" value="host"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Database</div><input class="inputField" type="hidden" name="globalName" value="database"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Username</div><input class="inputField" type="hidden" name="globalName" value="username"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>Password</div><input class="inputField" type="hidden" name="globalName" value="password"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>http_root</div><input class="inputField" type="hidden" name="globalName" value="http_root"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div class='globalVar'>Name : <div class='reqGlobal'>server_root</div><input class="inputField" type="hidden" name="globalName" value="server_root"/>
			Value : <input class="inputField" type="text" name="globalValue"/></div>	
			<div id='addGlobal' class='addButton'>+ Global</div><input class='inputField' type='hidden' name='globalsEnd'>
		</div>		
		<div id='newTable' class='addButton'>+ Table</div>
		<input class='inputField' type='hidden' name='siteEnd'>
		<input class='inputField' type='submit' name='submitBtn' id='submitBtn'>
		</form>
	</div>
	<div id='linkToXML'></div>
	</body>
</html>

<? }

function writePHP($output, $fileName) {
	global $xmlFile;
	$myfile = getcwd() . "/". $fileName;
	//echo "myfile = " . $myfile;
	echo "XML File:<br />\n<a href='". $fileName . "'>$fileName</a><br/><br /><div id='buildMetaBtn'>Build PHP Files</div>";
	$fh = fopen($myfile, 'w') or die("can't open file");
	fwrite($fh, $output);
	fclose($fh);
	$xmlFile = $fileName;
}

?>