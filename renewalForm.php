<script type="text/javascript" src="js/register.js"></script>
<?php

require_once("admin/database.php");

$fingerprint = $db->escape_value( $_GET["id"] );
$SQLuserQuery = "SELECT * FROM members WHERE fingerprint = '$fingerprint'";
$SQLuserResult = $db->query($SQLuserQuery);
while($SQLuserArray = $db->fetch_array($SQLuserResult)){
	$firstName = $SQLuserArray["firstName"];
	$lastName = $SQLuserArray["lastName"];
	$email = $SQLuserArray["email"];
	$street = $SQLuserArray["addressL1"];
	$city = $SQLuserArray["city"];
	$state = $SQLuserArray["state"];
	$zipcode = $SQLuserArray["zipcode"];
	$phone = $SQLuserArray["phone"];
	$contact = $SQLuserArray["contact"];
	$contactPref = ($contact == 1) ? "Yes" : "No"; 
	$region = $SQLuserArray["region"];
	$memberRegion = $regions[ $region-1];
	$fingerprint = $SQLuserArray['fingerprint'];
	$yearsEnrolled = $SQLuserArray['yearsEnrolled'];
	$id = $SQLuserArray['id'];
}
?>
<script type="text/javascript">
$(function () {
	// select the proper state option
	$("#stateselect").find("option[value='<?php echo strtoupper($state);?>']").attr("selected","selected");
	
	//select the proper contact preference
	var contact = <?php echo $contact;?>;
	if (contact == 0){
		$("input#no").attr("checked","checked");
	} else if (contact == 1){
		$("input#yes").attr("checked","checked");
	}
	
	//select the proper region
	$("#region").find("option[value='<?php echo strtoupper($region);?>']").attr("selected","selected");

});
</script>
<h1>Renew Your Membership</h1>
<h4>Part 1 of 2</h4>
<br/>
<p>Review the information below then click the button on the bottom to continue.</p>

<form id="renewalForm" class="nyssswaForm" name="form1" method="post" action="thankyou.php"> 
	<input type="hidden" name="fingerprint" value="<?php echo $fingerprint; ?>">
	<input type="hidden" name="yearsEnrolled" value="<?php echo $yearsEnrolled; ?>">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<input type="hidden" name="renewal" value="1">
<div id="errorDiv">
	<img src="images/warning-icon.gif" alt="Warning!" width="35" height="31" style="float:left; margin: -5px 10px 0px 0px; " />
	<span>Error Message Goes here.</span>
</div>
<table width="95%" border="0" align="center" cellpadding="10" cellspacing="10">
	<label>
		<div class="labelRow"><span id="fnameLabel">First Name</span><span id="lnameLabel">Last Name</span></div>
		<input class="required" type="text" name="firstName" id="firstName" size="30" maxlength="40" value="<?php echo $firstName; ?>" />
		<input class="required" type="text" name="lastName" id="lastName" size="30" maxlength="40" value="<?php echo $lastName; ?>" />
	</label>
	<label>
		Preferred E-Mail<br/>
		<input class="required" type="text" name="email" id="email" size="30" maxlength="50" value="<?php echo $email; ?>" />
	</label>
	<label>
		Confirm E-Mail<br/>
		<input class="required" type="text" name="emailConfirm" id="emailConfirm" size="30" maxlength="50" value="<?php echo $email; ?>" />
	</label>
	<label>Street Address<br/><input class="required" type="text" name="street" id="street" size="50" value="<?php echo $street; ?>" />	</label>
		<div class="labelRow"><span id="cityLabel">City</span><span id="stateLabel">State</span><span id="zipcodeLabel">Zipcode</span></div>

	<label>
		<input class="required" type="text" name="city" id="city" size="30" maxlength="30" value="<?php echo $city; ?>" />
		<?php include("states.php");?>
	</label>
	<label id="zipcodeInput"><input class="required" type="text" name="zipcode" id="zipcode" size="5" maxlength="5" value="<?php echo $zipcode; ?>" /></label>

	<label id="phoneLabel">Preferred Phone Number:	<input class="required" type="text" name="phone" id="phone" size="20" maxlength="15" placeholder=" (555)123-4567" value="<?php echo $phone; ?>" />	</label>
	<div class="formLabel">Are you interested in being contacted by a Regional Representative regarding local meetings and/or a more active local or Board role?</div>
	<label>
		<input class="required" type="radio" name="contact" id="yes" value="1" /> Yes
		<input type="radio" name="contact" id="no" value="0" /> No
		<div id="contactLabel"></div>
	</label>

	<div class="formLabel">Please choose a convenient networking region for yourself:</div>
	<label>
		<?php include("regions.php");?>
	</label>
  <div id="finishTxt" class="formLabel">If everything above appears to be correct, click the button below to finish your registration.</div>
  	<input class="required" type="submit" name="submitBtn" id="submitBtn" value="Renew My Membership!" />
	</table>
</form>
