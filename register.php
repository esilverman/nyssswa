<form id="newMemberForm" class="nyssswaForm" name="form1" method="post" action="thankyou.php"> 
<p id="renewAccident">* Renewing your membership?  <a href="renew.php">Follow this link</a>.</p>
<div id="errorDiv">
	<img src="images/warning-icon.gif" alt="Warning!" width="35" height="31" style="float:left; margin: -5px 10px 0px 0px; " />
	<span>Error Message Goes here.</span>
</div>
<table width="95%" border="0" align="center" cellpadding="10" cellspacing="10">
<!--
	<div class="formLabel">Membership Type</div>
	<label>
		<select class="required" name="membershipType" id="membershipType">
			<option value="" selected="selected">- Membership Type -</option>
			<option value="full">Full Membership ($75)</option>
			<option value="associate">Associate Membership ($65)</option>
			<option value="student_retiree">Student / Retiree ($45)</option>
		</select>
	</label>
-->
	<label>
		<div class="labelRow"><span id="fnameLabel">First Name</span><span id="lnameLabel">Last Name</span></div>
		<input class="required" type="text" name="firstName" id="firstName" size="30" maxlength="30" />
		<input class="required" type="text" name="lastName" id="lastName" size="30" maxlength="30" />
	</label>
	<label>
		Preferred E-Mail<br/>
		<input class="required" type="text" name="email" id="email" size="30" maxlength="30" />
	</label>
	<label>
		Confirm E-Mail<br/>
		<input class="required" type="text" name="emailConfirm" id="emailConfirm" size="30" maxlength="30" />
	</label>
	<label>Street Address<br/><input class="required" type="text" name="street" id="street" size="50" />	</label>
		<div class="labelRow"><span id="cityLabel">City</span><span id="stateLabel">State</span><span id="zipcodeLabel">Zipcode</span></div>

	<label>
		<input class="required" type="text" name="city" id="city" size="30" maxlength="30" />
		<?php include("states.php");?>
	</label>
	<label id="zipcodeInput"><input class="required" type="text" name="zipcode" id="zipcode" size="5" maxlength="5" /></label>

	<label id="phoneLabel">Preferred Phone Number:	<input class="required" type="text" name="phone" id="phone" size="20" maxlength="15" placeholder=" (555)123-4567"/>	</label>
	<div class="formLabel">Are you interested in being contacted by a Regional Representative regarding local meetings and/or a more active local or Board role?</div>
	<label>
		<input class="required" type="radio" name="contact" id="yes" value="1" /> Yes
		<input type="radio" name="contact" id="no" value="0" /> No
		<div id="contactLabel"></div>
	</label>

	<div class="formLabel">Please choose a convenient networking region for yourself:</div>
	<label>
		<?php include("regions.php");?>
	</label>
  <div id="finishTxt" class="formLabel">If everything above appears to be correct, click next to complete the last step of your registration.</div>
  	<input class="required" type="submit" name="submitBtn" id="submitBtn" value="Next >" />
	</table>
</form>
