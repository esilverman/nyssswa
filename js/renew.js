$(function () {


/* 	!Start jQuery Validation */
	jQuery.validator.messages.required = "";
	
	jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

	$("#renewalEmail").validate({
		rules: {
	    email: {
	      required: true,
	      email: true
	    },
	    emailConfirm: {
	      required: true,
	      email: true,
	      equalTo : "#email"
	    }
	  },
	  onkeyup: false,
		debug:true,
		messages: {
			emailConfirm: {
				required: " ",
				equalTo: "Please enter matching email addresses."	
			}
		},
		submitHandler: function(form) {
			//form.submit();
			var email = $("input#email").val();

			var dataString = 'email=' + email;
			//alert (dataString);return false;
			$(".ajaxLoader").show();
			$("#formBody, #renewalError").hide();
			$.ajax({
	      type: "POST",
	      url: "validateEmail.php",
	      data: dataString,
	      success: function(validEmail) {
    			$(".ajaxLoader").hide();
	      	if (validEmail == "valid") {
	      		$("#renewYourMembership").hide();
						$("#validationMsg").fadeIn("slow");
	      	} else {
						$("#formBody").show();
						$("#renewalError").show();
	      	}
	      }
	     }); //end .ajax

   	}
	}); //end renewalEmail.validate()
		$("#renewalForm").validate({
		rules: {
	    email: {
	      required: true,
	      email: true
	    },
	    emailConfirm: {
	      required: true,
	      email: true,
	      equalTo : "#email"
	    },
	    zipcode: {
	    	required: true,
		    digits: true
		    
	    },
	    phone: {
	    	phoneUS: true
	    }

	  },
	  onkeyup: false,
		invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'You missed 1 field. It has been highlighted below'
					: 'You missed ' + errors + ' fields.  They have been highlighted below';
				$("#errorDiv span").html(message);
				$("#errorDiv").show();
			} else {
				$("#errorDiv").hide();
			}
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "membership_status")
			 error.insertAfter("#membershipLabel");
			else if (element.attr("name") == "contact")
			 error.insertAfter("#contactLabel");
			else
			 error.insertAfter(element);
		},
		debug:true,
		messages: {
			emailConfirm: {
				required: " ",
				equalTo: "Please enter matching email addresses."	
			},
			membership_status: {
				required: "Please select your membership status."
			},
			contact: {
				required: "Please select your contact preference."
			}
		},
		submitHandler: function(form) {
       // do other stuff for a valid form
   	form.submit();
   	}
	}); //end renewalForm.validate()

});