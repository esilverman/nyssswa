/* START ONREADY FUNCTION */
$(function() {
	$("select").change(function() {
		$("#excelLink").hide();
	});
	
	$("#excelSubmitBtn").click( function() {
		var sortByValue = $("#excelSortByDropDown").find("option:selected").val();
		var selectedRegion = $("#region").find("option:selected");
		var region = parseInt( selectedRegion.val(), 10);
		//region = parseInt(region,10);
		var regionName = 	selectedRegion.html();
		//console.log("regionName : "+regionName );
		//console.log("region : "+region );
		if ( isNaN(region) ) {
			//console.log("isNaN: " + isNaN(region));
			regionName = "Full Membership";
			region = "";
		}
		
		var fromDate = $('#from').val(),
				toDate = $('#to').val(),
				fromString = fromDate.length > 0 ? '&fromDate='+fromDate : '',
				toString = toDate.length > 0 ? '&toDate='+toDate : '',
				dateRangeString = fromString+toString;
				
		var ajaxData = "sortByValue="+sortByValue+"&region="+region+"&regionName="+regionName+"&AJAXtask=printExcelLink"+dateRangeString;
		$("#excelLink").remove();
		$(this).after("<div id='loadingDiv'>Loading...</div>");
		console.log(ajaxData);
		$.ajax({
			url: "ajax.php",
			data: ajaxData,
			cache: false,
			success: function(excelLink){
				$("#loadingDiv").remove();
				
				$("#excelSubmitBtn").after("<div id='excelLink'>"+excelLink+"</div>").show();
			}		
		}); // END AJAX
	});

	// #### START DATERANGE #### //
	if ($( "#from, #to" ).length > 0 ) {
		var dates = $( "#from, #to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	}
	// #### END DATERANGE #### //


});
/* END ONREADY FUNCTION */