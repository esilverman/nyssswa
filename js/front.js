String.prototype.capitalize = function(){
   return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
};


/*!
 * cssSpriteHover function binds a set of jQuery hover functions for a 2-image CSS sprite
 */
function cssSpriteHover (btn_id, delta_y) {
	var btn = $("#"+btn_id);
	var bgPosFull = btn.css('background-position');
	// Strip away "%" and "A-z" and replace with "" >> leaves numbers only
	var cleanBgPos = bgPosFull.replace(/[\%A-z]/g,"");
	var bgPosArray = cleanBgPos.split(" ");
	var orig_x = bgPosArray[0];
	var orig_y = bgPosArray[1];
	
	btn.unbind("hover");
	btn.hover(
		function () {
			$(this).css({backgroundPosition: orig_x+"px "+delta_y+"px"});
		},
		function () {
			$(this).css({backgroundPosition: orig_x +"px "+orig_y+"px"});
		}
	);
} // end function cssSpriteHover()

function hoverIntentOver () {
	$(this).css({backgroundPosition: "0px -20px"});

} // end function hoverIntentOver()

function hoverIntentOut () {
	$(this).removeClass("navBtnHover");
	$(this).css({backgroundPosition: "0px 0px"});
} // end function hoverIntentOut()

/* !START ONREADY FUNCTION */
$(function() {
	
    /* !HEADER BEHAVIORS */
    cssSpriteHover("navHome", "-20");
    cssSpriteHover("navFilms", "-20");
    cssSpriteHover("navNews", "-20");
    cssSpriteHover("navBio", "-20");
    cssSpriteHover("navContact", "-20");
        
/*
    cssSpriteHover("productsHome", "0", "-204");
    //cssSpriteHover("productsProducts", "-40", "-204");
    cssSpriteHover("productsWhatWeDo", "0", "-284", "-80");
    cssSpriteHover("productsWhoWeAre", "0", "-324", "-120");
    cssSpriteHover("productsPress", "0", "-364", "-160");
*/

	var hoverIntentConfig = {    
	     over: hoverIntentOver, // function = onMouseOver callback (REQUIRED)    
	     timeout: 100, // number = milliseconds delay before onMouseOut    
	     out: hoverIntentOut, // function = onMouseOut callback (REQUIRED)    
		interval:50
	};

/* 	$(".navButton").hoverIntent(hoverIntentConfig); */
	$(".navButton").click( function () {
		var page = $(this).attr("name");
		window.location = "?page="+page;
	});
	
	$(".productsNavBtn").click( function () {
		var page = $(this).attr("name");
		if ( page != "products" ){
			window.location = "?page="+page;
		}
	});
	

    
	$("#masthead").click( function () {
		window.location = "?page=home";
	});
	$("#productsLogoTop").click( function () {
		window.location = "?page=home";
	});

	
/*
	if (BrowserDetect.browser !== "Explorer"){
		// Script RefURL: http://www.josephgiuffrida.com/jquery/jquery-fixed-sidebar-1/
		var theLoc = $('#improvoxIpad').position().top;
		if ($('#productsAppStoreFixed').length>0) {
			var productsLoc = $('#productsAppStoreFixed').position().top;
		}
		$(window).scroll(function() {
			if(theLoc >= $(document).scrollTop()) {
				if($('#improvoxIpad').hasClass('fixed')) {
					$('#improvoxIpad').removeClass('fixed');
					$('#iPadClickable').removeClass('fixed');
				}
			} else { 
				if(!$('#improvoxIpad').hasClass('fixed')) {
					$('#improvoxIpad').addClass('fixed');
					$('#iPadClickable').addClass('fixed');
				}
			}
			
			if(productsLoc >= $(document).scrollTop()) {
				if($('#productsAppStoreFixed').hasClass('fixedAppStore')) {
					$('#productsAppStoreFixed').removeClass('fixedAppStore');
					$("#improvoxApplogo").fadeOut("fast");
				}
			} else { 
				if(!$('#productsAppStoreFixed').hasClass('fixedAppStore')) {
					$('#productsAppStoreFixed').addClass('fixedAppStore');
					$("#improvoxApplogo").fadeIn();
				}
			}
		}); // end fixedSide.js
	} // end test for windows machines
*/
	
	if (typeof pageName !== "undefined") {
		var pageNameCamelCase = pageName.capitalize();
		$("#nav"+pageNameCamelCase).unbind().addClass("nav"+pageNameCamelCase+"Clicked");
	}
}); // ####### end onready function #######
