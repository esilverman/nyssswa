$(function () {
	
	$(".changeStatusBtn").hover(
			function () {
				$(this).find(".statusTooltip").addClass("visTooltip");
			},
			function () {
				$(this).find(".statusTooltip").removeClass("visTooltip");
			}
		);
	
	$(".changeStatusBtn").click( function () {
		var theBtn = $(this);
		var parentObj = $(this).parent();
		var memberId = parentObj.find(".memberId").val();
		var payStatus = "paid";
		if ( $(this).hasClass("paid") ) {
			payStatus = "unpaid";
		}
		$.ajax({
			type: "GET",
			url: "paymentStatus.php",
			data: "memberId="+memberId+"&status="+payStatus,
			success: function(statusResponse){
				//console.log(statusResponse);
				theBtn.replaceWith("<div class='statusResponse'>"+statusResponse+"</div>");
				parentObj.toggleClass("overduePayment");
				parentObj.find(".paymentReminderBtn").hide();
				if ( !parentObj.hasClass("overduePayment") ) {
					parentObj.addClass("statusChange");
				}
			  parentObj.effect("highlight", {}, 1000);
			 	$("#refreshBtn").slideDown("slow", function () {
			 		if (!$(this).hasClass("open")){ 
				 		$(this).effect("highlight", {}, 1500);
			 		}
			 		$(this).addClass("open");
			 	});
			 	parentObj.find('.statusResponse').fadeIn().delay(2000).fadeOut(); 
			}
		}); //end ajax()

	}); //end .changeStatusBtn
	
	$(".paymentReminderBtn").click( function () {
		var theBtn = $(this);
		var parentObj = $(this).parent();
		var memberId = parentObj.find(".memberId").val();
		var memberEmail = parentObj.find(".memberEmail").val();
		theBtn.hide();
		parentObj.find(".sendingMsg").show();
		$.ajax({
		   type: "GET",
		   url: "sendReminder.php",
		   data: "email="+memberEmail+"&memberId="+memberId+"&type=payment",
		   success: function(emailResult){
		   		console.log(emailResult);
					parentObj.find(".sendingMsg").hide();
					parentObj.find(".sentMsg").show();
		   }
		 });
	}); //end (".paymentReminderBtn").click

	$("#refreshBtn").click( function () {
		location.reload();
	}); //end ("#refreshBtn").click

	
});