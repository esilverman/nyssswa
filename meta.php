<!--
12-9-10 added table Display Name to <title> of _list pages.
				integrated database.php into related data SQL queries
9-22-10 integrated database.php into createWrite() > removed superflous junk for SQL queries
9-21-10 edited buildVisibleElement() > added "\" before "$" ... causing syntax highlighting issues
		integrated database.php into createList() > removed superflous junk for SQL queries
9-7-10 	added if/else in buildVisibleElement() to check if file is an image. if it isn't, it points to a movie icon thumbnail
		added table-specific classname
4-20-10 changed generated PHP scripts to include "?php" instead of "?" >> fixed IIS/Microsoft Compatibility 
		changed jquery.ui path from absolute to local path
		changed output on meta.php from super large text to reasonable size
9-4-09 merged with Jeff's cookie fixes; added ability to have custom Go To menu (meta now writes out to a "navlist.php" file)
8-2-09 code.Google.com/speed Recommendations:
			- Rearranged order of includes > CSS before JS allows "parallel downloading" >>> its faster 
			- NOT ACTUALLY DONE****Combine JS includes in to one file if possible
7-29-09 backend _write pages inputs have class "tableNameInput" in addition to "formElement" 
		backend _write pages forms now have id="tableNameForm"
		fixed _write page html include formatting
7-27-09 changed <title> to reflect the table display name (Table Name) instead of the db name (tableName) (ESS)
7-23-09 changed date elements such that *_write.php files pass the existing date value if($id) (ESS)
4-20-09 merged changes with sortable vs. linked table counting
4-9-09 commented out _grouplist file creation method, fixed field number counter to account for linked tables properly  

-->
<?php
$xmlFile = $_GET['xmlFile'];
if (!$xmlFile) {
	$xmlFile = "specs.xml";
}


parseXML();
dataToStrings();
createGlobalData();

$xml_data = array();
$displayNameArray = array();
$tableArray = array();

global $db_host;
global $db_username;
global $db_password;
global $db_name;
global $images_path,$movies_path,$audio_path;
global $siteName,$tableArray,$inputElementsString, $fieldVarsString, $postDataVarsString, $fieldsForDeletionString;
global $backListElements, $frontListElements, $frontListSQL, $backListSQL, $listPageNav;

// parseXML() reads from 'specs.xml' file 
function parseXML() {
	global $xmlFile;
	global $optionCounter;
	$optionCounter = 0;
	if (! ($xmlparser = xml_parser_create()) )
	{ 
		die ("Cannot create parser");
	} //instantiate the xml parser

	function start_tag($parser, $name, $attribs) { //function that processes start tags
		global $current;
		global $currentGrandparent;
		global $currentParent;
		global $siteName;
		global $xml_data;
		global $optionCounter, $listPageNav, $tableArray,$displayNameArray;
		
		 //global array for containing sql data

		$current = $name;
		switch ($name) {
		case "SITE":
			echo "<h1>Site Name: ".$attribs["NAME"]."</h1>";
			$siteName = $attribs["NAME"];
			break;
		case "TABLE":
			$xml_data[$attribs["NAME"]];
			$currentGrandparent = $attribs["NAME"];
			$tableArray[] = $currentGrandparent;
			$xml_data[$attribs["NAME"]]["DISPLAYNAME"];
			$xml_data[$attribs["NAME"]]["DISPLAYNAME"] = $attribs["DISPLAYNAME"];
			if ($attribs["SORTABLE"]) {
				$xml_data[$attribs["NAME"]]["SORTABLE"];
				$xml_data[$attribs["NAME"]]["SORTABLE"] = $attribs["SORTABLE"];
				$xml_data[$attribs["NAME"]]["sort_order"];
				$xml_data[$attribs["NAME"]]["sort_order"]["DISPLAYNAME"];
				$xml_data[$attribs["NAME"]]["sort_order"]["DISPLAYNAME"] = "Sort Order";				
				$xml_data[$attribs["NAME"]]["sort_order"]["SQLDATATYPE"] = "INT";
				$xml_data[$attribs["NAME"]]["sort_order"]["ENTRYMETHOD"] = "hidden";
				$xml_data[$attribs["NAME"]]["sort_order"]["FRONTLIST"] = "none";
				$xml_data[$attribs["NAME"]]["sort_order"]["BACKLIST"] = "none";
			}
			$displayNameArray[$currentGrandparent] = $attribs["DISPLAYNAME"];
			$listPageNav .= "<div id='" . $attribs["NAME"] . "' class='pageNav'><a href='" . $attribs["NAME"] . "_list.php'>" . $attribs["NAME"] . "</a></div>";
			//echo "<h2>Tablename: ";
			break;
		case "FIELD":
			$optionCounter = 0; //when you reach a new field, reset the option counters so the indexes are consistent
			$xml_data[$currentGrandparent][$attribs["NAME"]];
			$currentParent = $attribs["NAME"];
			$xml_data[$currentGrandparent][$attribs["NAME"]]["DISPLAYNAME"];
			$xml_data[$currentGrandparent][$attribs["NAME"]]["DISPLAYNAME"] = $attribs["DISPLAYNAME"];
			//echo "<div style='border:1px solid #000;'>";
			break;
		case "LINKED_TABLES":
			$xml_data[$name];
			$currentGrandparent = $name;
			break;
		case "RELATIONSHIP":
			$xml_data[$currentGrandparent];
			$currentParent = $attribs["LINK"];
			//echo "in RELATIONSHIP currentGP = $currentGrandparent currentParent : $currentParent <br>\n";
			break;
		case "BROTHER":
		case "SISTER":
			$xml_data[$currentGrandparent][$currentParent][$name];
			break;
		case "SQLDATATYPE":		//##################################################
		case "ENTRYMETHOD":		//for the info contained within each "FIELD" element
		case "FRONTLIST":		//##################################################
		case "BACKLIST":		//##################################################
			$xml_data[$currentGrandparent][$currentParent][$name];
			if ($attribs["CHILD_TABLENAME"]) {
				$xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"];
				$xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"] = $attribs["CHILD_TABLENAME"];
				$xml_data[$currentGrandparent][$currentParent]["CHILD_FIELDNAME"];
				$xml_data[$currentGrandparent][$currentParent]["CHILD_FIELDNAME"] = $attribs["CHILD_FIELDNAME"];
			}
			if ($attribs["LINK_TABLENAME"]) {
				$xml_data[$currentGrandparent][$currentParent]["LINK_TABLENAME"];
				$xml_data[$currentGrandparent][$currentParent]["LINK_TABLENAME"] = $attribs["LINK_TABLENAME"];			
				$xml_data[$currentGrandparent]["NUM_LINKTABLEFIELDS"] = $xml_data[$currentGrandparent]["NUM_LINKTABLEFIELDS"]+1;	
			}
			if ($attribs["WIDTH"]) {
				$xml_data[$currentGrandparent][$currentParent]["WIDTH"];
				$xml_data[$currentGrandparent][$currentParent]["WIDTH"] = $attribs["WIDTH"];
				$xml_data[$currentGrandparent][$currentParent]["HEIGHT"];
				$xml_data[$currentGrandparent][$currentParent]["HEIGHT"] = $attribs["HEIGHT"];
			}
			if ($attribs["SORTABLE"]) {
				$xml_data[$currentGrandparent][$currentParent]["SORTABLE"];
				$xml_data[$currentGrandparent][$currentParent]["SORTABLE"] = $attribs["SORTABLE"];
			}
			break;
		case "OPTION":
			//echo "in OPTION currentGP = $currentGrandparent currentParent : $currentParent <br>\n";
			$optionCounter++;
			break;
		case "GLOBALS":
			$xml_data[$name];
			$currentGrandparent = $name;
			break;
		case "AUDIO_PATH":		//###################################################
		case "IMAGES_PATH":		//###################################################
		case "THUMBS_PATH":		//
		case "MOVIES_PATH":		//
		case "USERNAME":		//for the info contained within each "GLOBAL" element
		case "PASSWORD":		//
		case "DATABASE":		//###################################################
		case "HOST":			//###################################################
		case "HTTP_ROOT":		//###################################################
		case "SERVER_ROOT":			//###################################################
			//echo "IN GLOBALS // currentGP : $currentGrandparent currentParent : $currentParent <br>\n";
			$xml_data[$currentGrandparent][$current];
			break;
		default:
			echo "<b>ERROR: incorrect XML structure // $value / $name</b><br>";
		}
/*
		echo "currentGP : $currentGrandparent currentParent : $currentParent <br>\n";
		echo "currentEntry : " . print_r($xml_data);
		echo "\n<br><br>";

	  	echo "Current tag : ".$name."<br />";
	  	if (is_array($attribs)) {
	      echo "Attributes : <br />";
	      while(list($key,$val) = each($attribs)) {
	         echo "Attribute ".$key." has value ".$val."<br />";
	       }
	    }
*/    
	} //end parseXML()
	
	function end_tag($parser, $name) { //function that processes end tags
//	   echo "Reached ending tag ".$name."<br /><br />";
		switch ($name) {
			case "SITENAME":
				break;
			case "TABLENAME":
				break;
			case "FIELD":
				break;
			case "NAME":
				break;
		}
	}
	xml_set_element_handler($xmlparser, "start_tag", "end_tag"); //calls the xml parser, sets the start/end tag functions
	
	function tag_contents($parser, $data) { //function that reads data within tags
		global $xml_data;
		global $current;
		global $currentParent;
		global $currentGrandparent;
		global $optionCounter;
		
		//echo "INSIDE TAG CONTENTS // currentGP : $currentGrandparent currentParent : $currentParent current : $current data : $data <br>\n";
		if ($currentGrandparent == "LINKED_TABLES") {
			$xml_data[$currentGrandparent][$currentParent][$current] = $data."_id";
			}
		else if ($currentGrandparent == "GLOBALS") {
			$xml_data[$currentGrandparent][$current] = $data;
		}else if ($current == "OPTION") {
			$xml_data[$currentGrandparent][$currentParent][$current][$optionCounter] = $data;
		}else {
			$xml_data[$currentGrandparent][$currentParent][$current] = $data;
		}
			
		//print_r($xml_data);
	}
	xml_set_character_data_handler($xmlparser, "tag_contents"); //calls the xml parser, sets the data read function
	$filename = $xmlFile; //file to read
	if (!($fp = fopen($filename, "r"))) { die("cannot open ".$filename); }

	while ($data = fread($fp, 4096)){ //read that file
   		$data=eregi_replace(">"."[[:space:]]+"."<","><",$data); //removes whitespace
   		if (!xml_parse($xmlparser, $data, feof($fp))) { //run the code 
			$reason = xml_error_string(xml_get_error_code($xmlparser)); //or tell me why not
      		$reason .= xml_get_current_line_number($xmlparser);
      		die($reason);
   		}
	}
	xml_parser_free($xmlparser);
	
	echo "<p>End of file reached: </p>";
	global $xml_data;
	//print_r($xml_data);
}

//dataToStrings() writes the database based on the parsed XML as well as constructs a backend HTML String
function dataToStrings() {
	$SQLhandle;
	$optionArray = array();
	global $tableArray,$optionArray, $displayNameArray, $xml_data, $db_host, $db_username, $db_password, $db_name;
	global $optionCounter;
	global $fieldsForDeletionString, $inputElementsString, $fieldVarsString, $postDataVarsString, $editPostDataSQLString, $newNamesPostDataSQLString, $newValuesPostDataSQLString; 
	global $backListElements, $frontListElements, $frontListSQL, $backListSQL, $listPageNav, $SQLconnect, $sortable;
	
//install table sql statement
	foreach ($xml_data as $currentGrandparent => $value) {
		$SQLhandle = "CREATE TABLE $currentGrandparent (id INT NOT NULL AUTO_INCREMENT, ";
		//echo  "<br />" . $currentGrandparent;
		$fieldNum = 1;
		$backListElements = "\n\t$"."bodyString .= \"\\"."n\\"."n <div class='backListRecord $currentGrandparent"."_record' id='$"."id'>\";\n";
		foreach ($xml_data[$currentGrandparent] as $currentParent => $value) {
			if ($currentGrandparent == "GLOBALS") {
				//echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $value;
				globalData($currentParent,$value);
			}else if ($currentGrandparent == "LINKED_TABLES") {
				echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $currentParent;
			}else {
				if ($currentParent == "DISPLAYNAME") {
					$displayNameArray[$currentGrandparent] = $value;
				}
				else if ($currentParent == "NUM_LINKTABLEFIELDS") { echo $value; }
				else if ($currentParent == "SORTABLE") {
/* 					$backListElements = "test"; */
					$backListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='backListElement'><div class='dragIcon'></div></div>\";";
					 //$sortable = $value;
				}
				else {
					echo "<p>SQL DATATYPE: ". $xml_data[$currentGrandparent][$currentParent]["SQLDATATYPE"] . "</p>" ;
					if ($xml_data[$currentGrandparent][$currentParent]["SQLDATATYPE"] != "NONE") {
						$numOfLinkTableFields = $numOfFields = $isSortable = "";
						if ($xml_data[$currentGrandparent]["NUM_LINKTABLEFIELDS"]) {
						//is there a link table field counter in the array for this table?
							$numOfLinkTableFields = $xml_data[$currentGrandparent]["NUM_LINKTABLEFIELDS"]+1;
							//echo "<h1>there is a link table counter in the array</h1>";
							//then make the link table counter variable for this table equal to that and include one for the counter as a field itself
						}
						if ($xml_data[$currentGrandparent]["SORTABLE"]) {
						$isSortable = 1;
						}
						$numOfFields = sizeof($xml_data[$currentGrandparent])-(1+$numOfLinkTableFields+$isSortable);
						//the number of fields is equal to the number of points in the array, minus one for the display name and the link table field counter
						echo "<p>[sizeof: ".sizeof($xml_data[$currentGrandparent])." ],[ numOfLinkTableFields: $numOfLinkTableFields ], [isSortable: $isSortable], [ numOfFields: $numOfFields]</p>";			
						$fieldVarsString .= "\t\t$$currentParent = $%rowArray[\"" . $currentParent . "\"];\n";
						$postDataVarsString .= "\t\t$$currentParent = htmlentities($%_POST[\"" . $currentParent . "\"],ENT_QUOTES);\n";
		
						if ($fieldNum < $numOfFields) {
							//echo "<br>fieldNum : $fieldNum // numOfFields : $numOfFields<br>";
							$editPostDataSQLString .= " $currentParent = '$$currentParent', ";
							$newNamesPostDataSQLString .= "$currentParent,";
							$newValuesPostDataSQLString .= "'$$currentParent',";
							
						} else {
							//echo "<br>fieldNum : $fieldNum // numOfFields : $numOfFields";
							$editPostDataSQLString .= " $currentParent = '$$currentParent'  ";
							$newNamesPostDataSQLString .= "$currentParent";
							$newValuesPostDataSQLString .= "'$$currentParent'";
						}
					}
				//echo $editPostDataSQLString;
				$fieldNum++;

				$SQLhandle .= "";
				} //end 2nd else
				if ($currentParent != "DISPLAYNAME" && $currentParent != "SORTABLE") {
					foreach ($xml_data[$currentGrandparent][$currentParent] as $current => $value) {
						//$optionName = $current . $optionCounter;
						switch ($current) {
							case "SQLDATATYPE":
								if ($value != "NONE")
									$SQLhandle .= "$currentParent " . SQLdatatype($value) . ", ";
								break;
							case "ENTRYMETHOD":
								$inputElementsString .= entryMethod($value,$currentParent, $currentGrandparent, $optionArray);
								if ($value == 'fileUpload') {$fieldsForDeletionString .= "$.fileDelete(ID,'$currentParent');\n";}
								if ($value == 'fileUploadCrop') {$fieldsForDeletionString .= "$.fileDelete(ID,'$currentParent');\n";}
								if ($value == 'fileUploadThumb') {$fieldsForDeletionString .= "$.fileDelete(ID,'$currentParent');\n";}
								break;
							case "FRONTLIST":
							case "BACKLIST":
								buildVisibleElement($value,$current,$currentParent,$currentGrandparent);
								break;
							case "OPTION":
								$optionArray = $value;
	/*
								$optionArray[]=$value;
								print_r($optionArray);
								$optionCounter++;
	*/
								break;
							case "DISPLAYNAME":
								$displayNameArray[$currentParent] = $value;
								//print_r($displayNameArray);
								break;
							case "CHILD":
							case "CHILD_TABLENAME":
							case "CHILD_FIELDNAME":
							case "LINK_TABLENAME":
							case "WIDTH":
							case "HEIGHT":
								break;
							default:
								echo "ERROR: Field attribute not recognized...";
								break;
						} //end switch
						
					} // end foreach CURRENT
				}
			} //end if
		} //end foreach CURRENTPARENT		
		$SQLhandle .= " PRIMARY KEY (id) );";
		echo "<br />" . $SQLhandle . "<br/>";
		//#### execute SQL calls and createWrite for TABLES only
		if (($currentGrandparent != "GLOBALS") && ($currentGrandparent != "LINKED_TABLES")) {
			$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
			$fieldVarsString .= "\t\t$%id = $%rowArray[\"id\"];\n";
			createWrite($currentGrandparent);
			createList($currentGrandparent);
			//#### createGroupWrite($currentGrandparent);
			//#### createGroupList($currentGrandparent);
			createLinkedTableList($currentGrandparent);
			createFrontEndSkeleton($currentGrandparent);
			$inputElementsString = $fieldsForDeletionString = $fieldVarsString = $postDataVarsString = $editPostDataSQLString = $newNamesPostDataSQLString = $newValuesPostDataSQLString = "";
			$backListElements = $frontListElements = $frontListSQL = $backListSQL = "";
			if (!$SQLconnect) { echo("SQL Connection Error: " . mysql_error() . "\n");	}
			else {
				$SQLexecute = mysql_db_query($db_name,$SQLhandle,$SQLconnect);
				if (!$SQLexecute) { echo("SQL Execution Error: " . mysql_error() . "\n$SQL\n");	}
				else {
				//echo "MySQL Table Created";
				}
			}
			//mysql_close($SQLconnect);
		} //end SQL build calls
	} //end foreach CURRENTGRANDPARENT
	//echo "meta write string:<br />" . $inputElementsString;
	//echo "field list string:<br />" . $fieldVarsString;
echo "<H2>FINISHED PRINTING</H2>";
createNav();
//echo "<b>listPageTabs: " . $listPageNav . "</b><br><br>";
print_r($xml_data);

}

function SQLdatatype($SQLdatatype) {
	$returnString;
	switch ($SQLdatatype) {
		case "VARCHAR":
			$returnString = "varchar(255)";
			break;
		case "TEXT":
			$returnString = "TEXT";
			break;
		case "DATE":
			$returnString = "DATE";
			break;
		case "INT":
			$returnString = "INT";
			break;
		case "DATETIME":
			$returnString = "DATETIME";
			break;
		case "TIME":
			$returnString = "TIME";
			break;
		default:
			echo " // <b>ERROR: Data Type not recognized...</b>";
			break;
	}
	return $returnString;
}

function entryMethod($entrymethod, $currentParent,$currentGrandparent,$optionArray) {
	global $db_name,$db_host,$db_username,$db_password;
	global $SQLconnect;
	global $displayNameArray;
	global $xml_data;
	$returnString;
	switch ($entrymethod) {
		case "textArea":
			$returnString = "echo \"<label for='$currentParent'>".$displayNameArray[$currentParent]."</label><textarea name='$currentParent' cols='40' rows='6' class='formElement $currentGrandparent"."Input'>$$currentParent</textarea>\";\n";
			break;
		case "textField":
			$returnString = "echo \"<label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input type='text' name='$currentParent' size='100' class='formElement $currentGrandparent"."Input' value='$$currentParent'>\";\n";
			break;

		case "textAreaAJAX":
			$returnString = "echo \"<label for='$currentParent'>".$displayNameArray[$currentParent]."</label><textarea name='$currentParent' cols='40' rows='6' class='formElement $currentGrandparent"."Input'>$$currentParent</textarea>\";\n";
			break;
		case "fileUpload":
				$returnString = "
if ($"."id) {
	echo \"<div id='\".$"."id.\"$currentParent' class='noCrop'>
				<label for='$currentParent'>".$displayNameArray[$currentParent]."</label>\".fileUploadDisplay($$currentParent).\"\";
	echo \"<input type='file' name='$currentParent' class='fileUpload' value='$$currentParent' id='\".$"."id.\"'/><div class='originalFile'><input type='hidden' name='$currentParent' value='$$currentParent' /></div></div>\";	
}
else {
	echo \"<div id='new_item_$currentParent'  class='noCrop'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><div class='new'></div>\";
	echo \"<input type='file' name='$currentParent' class='fileUpload' value='$$currentParent' /><div class='originalFile'><input type='hidden' name='$currentParent' value='$$currentParent' /></div></div>\";	
}";
	
			break;
		case "fileUploadCrop":
				$width = $xml_data[$currentGrandparent][$currentParent]["WIDTH"];
				$height = $xml_data[$currentGrandparent][$currentParent]["HEIGHT"];

				$returnString = " $"."varName = \"$currentParent\";\n
if ($"."id) {\n
	echo \"<div class='fileCropContainer'><div id='\";
	
	$"."recordId = $"."id;  echo $"."id;
	 echo \"$currentParent' class='fileUploadCrop'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label>\".fileUploadDisplay($$currentParent).\"\";\n
	echo \"<input type='file' name='$currentParent' class='fileUpload' value='$$currentParent' id='$"."id'/><div class='originalFile'><input type='hidden' name='$currentParent' class='formElement  $currentGrandparent"."Input' value='$$currentParent' /></div></div>
				<input type='hidden' id=\\\"$"."id\".\"$currentParent"."_width\\\" value='$width'>
				<input type='hidden' id=\\\"$"."id\".\"$currentParent"."_height\\\" value='$height'>
				\";
}\n
else {\n
	echo \"<div class='fileCropContainer'><div id='new_item_$currentParent' class='fileUploadCrop'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><div class='new'></div>\";\n
	echo \"<input type='file' name='$currentParent' class='fileUpload' value='$$currentParent' id='$"."id'/><div class='originalFile'><input class='formElement  $currentGrandparent"."Input' type='hidden' name='$currentParent' value='' /></div></div>
				<input type='hidden' id=\\\"$"."id\".\"new_item_$currentParent"."_width\\\" value='$width'>
				<input type='hidden' id=\\\"$"."id\".\"new_item_$currentParent"."_height\\\" value='$height'>
				\";}\n";
			break;
		case "fileUploadThumb":
				$returnString = "
if ($"."id) {\n
	echo \"<div id='\";
	
	$"."recordId = $"."id; echo $"."id;
	 echo \"$currentParent' class='fileUploadThumb'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label>\";\n
	if (!$$currentParent) {echo \"<div class='previewBox'><img src='' class='preview' /></div>\";}\n
	else {echo \"<div class='previewBox'><img src='$$currentParent' class='preview'></div>\";}\n
	echo \"<input type='hidden' class='formElement $currentGrandparent"."Input' name='$currentParent' value='$$currentParent' /></div></div><br clear='all' />\";	\n
}\n
else {\n
	echo \"<div id='new_item_$currentParent' class='fileUploadThumb' style='display:none;'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label>\";\n
	if (!$$currentParent) {echo \"<div class='previewBox'><img src='' class='preview' /></div>\";}\n
	else {echo \"<div class='previewBox'><img src='$$currentParent' class='preview'></div>\";}\n
	echo \"<input type='hidden' class='formElement $currentGrandparent"."Input' name='$currentParent' value='' /></div></div><br clear='all' />\";	\n
}
";			
			break;
		case "dropDown":
			$returnString = "echo \"<label for='$currentParent'>".$displayNameArray[$currentParent]."</label><select name='$currentParent' class='formElement $currentGrandparent"."Input'>\n";
			for ($i = 1; $i <= sizeof($optionArray);$i++) {	
				$returnString .= "<option \";";
				$returnString .= " if ($$currentParent == \"" . $optionArray[$i] . "\") {echo \" selected='selected' \";}";
				$returnString .= "echo \"value='". $optionArray[$i] . "'>". $optionArray[$i] . "</option>\n";
				//print_r($optionArray);
			}
			$returnString .= "</select>\n\";\n";
			break;
		case "dropDown_related":
			$child_tablename = $xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"];
			$child_fieldname = $xml_data[$currentGrandparent][$currentParent]["CHILD_FIELDNAME"];
			$returnString = "echo \"<div class='relationship'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><select name='$currentParent' class='dropDown_related formElement $currentGrandparent"."Input'><option value='$$currentParent'></option></select>
			<a class='dropDown_related_addBtn positiveBtns'>ADD OPTION</a>
			</div>
			<script type='text/javascript'>
			var $currentParent"."_child_tablename = '$child_tablename';
			var $currentParent"."_child_fieldname = '$child_fieldname';

			</script>\";\n";
			break;
		case "date":
			$returnString = "
			if ($"."id) {
				echo \"<div class='date' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input class='hasdatepicker formElement $currentGrandparent"."Input' type='text' id='$currentParent"."_datepicker' value=\\\"\"  . strftime('%m',strtotime($$currentParent)) . \"/\" . strftime('%d',strtotime($$currentParent)) .\"/\". strftime('%Y',strtotime($$currentParent)) . \"\\\" /><input type='hidden' name='$currentParent' id='$currentParent"."_datestore' value='$"."$currentParent'></div>\";\n
			}
			else {
				echo \"<div class='date' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input class='hasdatepicker formElement $currentGrandparent"."Input' type='text' id='$currentParent"."_datepicker' /><input type='hidden' name='$currentParent' id='$currentParent"."_datestore'></div>\";\n

			}
			";
			break;
		case "dateTime":
			//if we are editing a page print out the data already inside the row -> else print empty input fields
			$returnString = "
			if ($"."id) {
				echo \"<div class='dateTime' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input class='hasdatepicker formElement $currentGrandparent"."Input' type='text' id='$currentParent"."_datepicker' value=\\\"\"  . strftime('%m',strtotime($$currentParent)) . \"/\" . strftime('%d',strtotime($$currentParent)) .\"/\". strftime('%Y',strtotime($$currentParent)) . \"\\\" /><input type='hidden' id='$currentParent"."_datestore'> @ <input type='text' class='hastimepicker formElement $currentGrandparent"."Input' id='$currentParent"."_timepicker' value='\".strftime('%I',strtotime($$currentParent)).\":\".strftime('%M',strtotime($$currentParent)).\" \". strftime('%P',strtotime($$currentParent)) .\"' /><input type='hidden' id='$currentParent"."_timestore' value='\".strftime('%I',strtotime($$currentParent)).\":\".strftime('%M',strtotime($$currentParent)).\":\". strftime('%S',strtotime($$currentParent)) .\"' /></div>\";\n
			}
			else {
				echo \"<div class='date' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input type='text' class='formElement $currentGrandparent"."Input month' name='".$currentParent."_month' size='2'/>&nbsp;<input type='text' class='formElement $currentGrandparent"."Input day' name='".$currentParent."_day' size='2'/>&nbsp;<input type='text' class='formElement $currentGrandparent"."Input year' name='".$currentParent."_year' size='4'/>&nbsp;&nbsp;<input type='text' class='formElement $currentGrandparent"."Input hour' name='".$currentParent."_hour' size='4'/>:<input type='text' class='formElement $currentGrandparent"."Input minute' name='".$currentParent."_minutes' size='4'/>:<input type='text' class='formElement $currentGrandparent"."Input second' name='".$currentParent."_seconds' size='4'/></div>\";\n
			}";
			break;
		case "time":
			//if we are editing a page print out the data already inside the row -> else print empty input fields
			$returnString = "
			if ($"."id) {
				echo \"<div class='dateTime' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input type='text' class='hastimepicker formElement $currentGrandparent"."Input' id='$currentParent"."_timepicker' value='\".strftime('%I',strtotime($$currentParent)).\":\".strftime('%M',strtotime($$currentParent)).\" \". strftime('%P',strtotime($$currentParent)) .\"' /><input type='hidden' name='$currentParent' id='$currentParent"."_timestore' value='\".strftime('%I',strtotime($$currentParent)).\":\".strftime('%M',strtotime($$currentParent)).\":\". strftime('%S',strtotime($$currentParent)) .\"' /></div>\";\n
			}
			else {
				echo \"<div class='date' id='$currentParent'><label for='$currentParent'>".$displayNameArray[$currentParent]."</label><input type='text' class='hastimepicker formElement $currentGrandparent"."Input' id='$currentParent"."_timepicker' value='12:00 am' /><input type='hidden' name='$currentParent' id='$currentParent"."_timestore' value='00:00:00' /></div>\";\n
			}";
			break;
		case "groupItemEdit":
			$childTableBase = $xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"];
			$childTableInclude =  $childTableBase . "_groupwrite.php";
			$returnString .= "$$currentGrandparent"."_id = $"."id;";
			$returnString .= "$"."parentTable_SQL_where = \"$currentGrandparent"."_id = $"."id\";";
			$returnString .= "$"."parentTable = \"$currentGrandparent\";";
			$returnString .= "echo \"<script type='text/javascript'> child_tableName = '$childTableBase';</script>\";";
			//#######parentTable variable is not unique, we need to clear out this inconsistency in the FUTURE for multiple groupItemEdit's on one page!!!!!
			$returnString .= "include \"$childTableInclude\";";
			break;
		case "groupItemList":
			$childTableBase = $xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"];
			$childTableInclude =  $childTableBase . "_grouplist.php";
			$returnString .= "$$currentGrandparent"."_id = $"."id;";
			$returnString .= "$"."parentTable_id = \"$currentGrandparent"."_id\";";
			$returnString .= "$"."parentTable_SQL_where = \"$currentGrandparent"."_id = $"."id\";";
			$returnString .= "$"."parentTable = \"$currentGrandparent\";";
			$returnString .= "if ($"."id) {";
			$returnString .= "echo \"<label>".$displayNameArray[$currentParent]."</label>\";";						
			$returnString .= "echo \"<div class='groupItemList'>\";";			
			$returnString .= "include \"$childTableInclude\";";
			$returnString .= "echo \"</div>\";}";			
			break;
		case "linkedTableList":
			$child_tablename = $xml_data[$currentGrandparent][$currentParent]["CHILD_TABLENAME"];
			$child_fieldname = $xml_data[$currentGrandparent][$currentParent]["CHILD_FIELDNAME"];
			$isSortable = $xml_data[$child_tablename]["SORTABLE"];
			$sortableClass = $sortableSQL = "";
			if ($isSortable) { $sortableClass = "sortable"; $sortableSQL = " sort_order INT,";}
			$link_tablename = $xml_data[$currentGrandparent][$currentParent]["LINK_TABLENAME"];
			$link_sql = "CREATE TABLE $link_tablename (id INT NOT NULL AUTO_INCREMENT, $sortableSQL parent_id INT, child_id INT, PRIMARY KEY (id) );";
			$SQLlinkconnect = mysql_connect($db_host,$db_username,$db_password);
			if (!$SQLlinkconnect) { echo("<b>SQL Link Connection Error:</b> " . mysql_error() . "\n");	}
			else {
				$SQLlinkExecute = mysql_db_query($db_name,$link_sql,$SQLlinkconnect);
				if (!$SQLlinkExecute) { echo("<em>SQL Link Execution Error:</em> " . mysql_error() . "\n");	}
				else {
					echo "<em>".$link_sql . "</em>";
				}
			}
			echo "<em>".$link_sql . "</em>";
			
			$childTableInclude =  $child_tablename . "_linkedtablelist.php";
			$currentLinkTable = "link_table_test";
			$currentChildFieldName = "title";
			$currentChildTableName = "public_art_groups1";
			$returnString .= "$$currentGrandparent"."_id = $"."id;";
			$returnString .= "$"."linkTableName = \"$link_tablename\";";
			$returnString .= "$"."childTableName = \"$child_tablename\";";
			
			$returnString .= "$"."parentTable_id = \"$currentGrandparent"."_id\";";
			$returnString .= "$"."parentTable_SQL_where = \"$currentGrandparent"."_id = $"."id\";";
			$returnString .= "$"."parentTable = \"$currentGrandparent\";";
			$returnString .= "if ($"."id) {\n";
			$returnString .= "echo \"<label>".$displayNameArray[$currentParent]."</label>\";\n";		
			//$returnString .= "echo \"<script src='list_actions.js' type='text/javascript'></script>\";\n";		
			$returnString .= "echo \"<script type='text/javascript'>\n";			
			$returnString .= "$(function() {\n" . 
						"\t$('#$link_tablename .autocompleteSearchBox:first').autocomplete('ajax.php', {
								matchContains: true,
								cacheLength: 1,
								extraParams : {
									'child_tablename':'$child_tablename',
									'child_fieldname':'$child_fieldname',
									'link_tablename':'$link_tablename',
									'parent_tablename':parent_table,
									'parent_recordID': parent_id,
								},
								minChars: 0
							});

							$('#$link_tablename .sortable').sortable({ 
								items: '.backListRecord',
								handle: '.dragIcon',
								update: function() {
									var orderString = '';
									var order = $('#$link_tablename .sortable').sortable('toArray');
									for (i=0;i<order.length;i++) {
										if (i != 0) { 
											orderString = orderString + '&'; 
										}
										orderString = orderString + 'order[]='+order[i];
									}
									//alert(order);
									//alert(orderString);
									//test[]=2&test[]=1&test[]=3 	
									$.ajax({
										type: 'GET',
										url: 'ajax.php',
										data: 'AJAXtask=linksortorderUpdate&link_tablename=$link_tablename&parent_recordID='+parent_id+'&tablename=$child_tablename&'+orderString,
										success: function(msg){
											//alert( \'Data Saved: \' + msg );
										}
									});	
								}
							});

						});\n";			
			$returnString .= "</script>\n\";\n";		
			$returnString .= "echo \"<div class='linkedTable' id='$link_tablename'>\";\n";			
			$returnString .= "echo \"<div class='linkedTableList $sortableClass'>\";\n";			
			$returnString .= "include \"$childTableInclude\";";
			$returnString .= "echo \"</div>\";";
			$returnString .= "echo \"<input type='text' class='autocompleteSearchBox' value='Search ".$displayNameArray[$child_tablename]."'/>\";";
			$returnString .= "echo \"<a class='newBtn positiveBtns' href='" . $child_tablename . "_write.php?foreignKey=\".$".$currentGrandparent."_id.\"&sessionDepth=\".($"."_GET['sessionDepth']+1).\"&linkedChild=\".$child_tablename.\"'>NEW RECORD</a>\";";

			$returnString .= "						
			";			
			$returnString .= "echo \"</div>\";}";			
			break;
		case "hidden":
			$returnString .= "echo \"<input class='formElement' type='hidden' name='$currentParent' value='$$currentParent' />\";";
			break;
		case "foreignKey":
			$returnString .= "if (!$"."foreignKey) { $"."foreignKey = $$currentParent; }";
			$returnString .= "echo \"<input class='formElement' type='hidden' name='$currentParent' value='$"."foreignKey' />\";";
			break;
		default:
			echo " // <b>ERROR: Entry Type not recognized...</b>";
			break;	}
	return $returnString;
}

function globalData($paramName, $value) {
	global $db_host;
	global $db_username;
	global $db_password;
	global $db_name;
	global $images_path;
	global $movies_path;
	global $thumbs_path;
	global $audio_path;
	global $http_root;
	global $server_root;

	switch ($paramName) {
		case "USERNAME":
			$db_username = $value;
			break;
		case "PASSWORD":
			$db_password = $value;
			break;
		case "DATABASE":
			$db_name = $value;
			break;
		case "HOST":
			$db_host = $value;
			break;
		case "AUDIO_PATH":
			$audio_path = $value;
			break;
		case "IMAGES_PATH":
			$images_path = $value;
			break;
		case "THUMBS_PATH":
			$thumbs_path = $value;
			break;
		case "MOVIES_PATH":
			$movies_path = $value;
			break;
		case "THUMBS_PATH":
			break;
		case "HTTP_ROOT":
			$http_root = $value;
			break;
		case "SERVER_ROOT":
			$server_root = $value;
			break;
		default:
			echo " // <b>ERROR: Global Parameter not recognized...</b>";
			break;
	}
}

function buildVisibleElement($displayType, $page, $fieldName, $tableName) {
	global $db_host;
	global $db_username;
	global $db_password;
	global $db_name;

	global $frontListElements, $frontListSQL, $backListElements, $backListSQL, $displayNameArray,$xml_data;
	//echo "<b>displayType: $displayType // page : $page // fieldName : $fieldName</b>";
	if ($page == "FRONTLIST") {
		switch ($displayType) {
			case "text":
				$frontListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='frontListElement'>".$displayNameArray[$fieldName].": $$fieldName</div>\";";
				break;
			case "image":
				$frontListElements .= "\n\t\t if ($".$fieldName.") { \$"."bodyString .= \"\\"."n<img src='$"."$fieldName' class='frontListElement'>\";}";
				break;
			case "date":
				$frontListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='frontListElement'>".$displayNameArray[$fieldName].": \".strftime('%m',strtotime($$fieldName)).\" \".strftime('%d',strtotime($$fieldName)).\" \".strftime('%Y',strtotime($$fieldName)).\"</div>\";";
				break;
			case "dateTime":
				$frontListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='frontListElement'>".$displayNameArray[$fieldName].": \".strftime('%m',strtotime($$fieldName)).\" \".strftime('%d',strtotime($$fieldName)).\" \".strftime('%Y',strtotime($$fieldName)).\" \".strftime('%I',strtotime($$fieldName)).\":\".strftime('%M',strtotime($$fieldName)). \":\".strftime('%S',strtotime($$fieldName)). \"</div>\";";
				break;
			case "time":
				$frontListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='frontListElement'>".$displayNameArray[$fieldName].": \".strftime('%I',strtotime($$fieldName)).\":\".strftime('%M',strtotime($$fieldName)). \":\".strftime('%S',strtotime($$fieldName)). \"</div>\";";
				break;
			case "relatedData":
				$child_tablename = $xml_data[$tableName][$fieldName]["CHILD_TABLENAME"];
				$child_fieldname = $xml_data[$tableName][$fieldName]["CHILD_FIELDNAME"];
				$frontListElements .= "\n$"."SQLrelatedData = \"SELECT $child_fieldname AS fieldname, id FROM $child_tablename WHERE id = '$$fieldName' \";\n";
				$frontListElements .= "$"."SQLrelatedExecute = $"."db->query($"."SQLrelatedData);\n";
				$frontListElements .= "while( $"."relatedRowArray = $"."db->fetch_array($"."SQLrelatedExecute) ) {\n";
				$frontListElements .= "\t$"."fieldName = $"."relatedRowArray[\"fieldname\"];\n";
				$frontListElements .= "\t$"."relatedId = $"."relatedRowArray[\"id\"];\n";
				$frontListElements .= "\t$"."bodyString .= \"\\"."n<div class='frontListElement'>".$displayNameArray[$fieldName].": $"."fieldName</div>\";\n";
				$frontListElements .= "}";
				break;
			case "none":
				break;
			default:
				echo " // <b>ERROR: Back List Display Type not recognized...</b>";
				break;
		}
		if ($displayType != "none") {
			$frontListSQL = "$fieldName, ";
		}
	}
	if ($page == "BACKLIST") {
		switch ($displayType) {
			case "text":
				$backListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='backListElement'>".$displayNameArray[$fieldName].": $$fieldName</div>\";";
				break;
			case "image":
				$backListElements .= "\n\t\t$"."imgFileTypes = array('JPG','PEG','GIF','PNG');";
				if( substr($fieldName,-6) == "_thumb" ) { // if this is a thumbnail field, do the thumb checkeroo
					$backListElements .= "\n\t\t$"."fileEXT = strtoupper( substr($".substr($fieldName,0, strlen($fieldName)-6).", -3) );";
					$backListElements .= "\n\t\tif ( !in_array($"."fileEXT, $"."imgFileTypes) ) {";
					$backListElements .= "\n\t\t\t$"."bodyString .= \"\\"."n<img src='images/defaultThumbnail.png' class='backListElement'>\";";
					$backListElements .= "\n\t\t} else if ($".$fieldName.") {";
					$backListElements .= "\n\t\t\t$"."bodyString .= \"\\"."n<img src='$".$fieldName."' class='backListElement'>\";";
					$backListElements .= "\n\t\t}";
				} else {
					$backListElements .= "\n\t\t if ($".$fieldName.") { "."$"."bodyString .= \"\\"."n<img src='$"."$fieldName' class='backListElement'>\";}";
				}
				break;
			case "date":
				$backListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='backListElement'>\".strftime('%m',strtotime($$fieldName)).\" \".strftime('%d',strtotime($$fieldName)).\" \".strftime('%Y',strtotime($$fieldName)).\"</div>\";";
				break;
			case "dateTime":
				$backListElements .= "\n\t\t$"."bodyString .= \"\\"."n<div class='backListElement'>\".strftime('%m',strtotime($$fieldName)).\" \".strftime('%d',strtotime($$fieldName)).\" \".strftime('%Y',strtotime($$fieldName)).\" \".strftime('%i',strtotime($$fieldName)).\":\".strftime('%s',strtotime($$fieldName)).\"</div>\";";
				break;
			case "relatedData":
				$child_tablename = $xml_data[$tableName][$fieldName]["CHILD_TABLENAME"];
				$child_fieldname = $xml_data[$tableName][$fieldName]["CHILD_FIELDNAME"];
				$backListElements .= "\n$"."SQLrelatedData = \"SELECT $child_fieldname AS fieldname, id FROM $child_tablename WHERE id = '$$fieldName' \";\n";
				$backListElements .= "$"."SQLrelatedExecute = $"."db->query($"."SQLrelatedData);\n";
				$backListElements .= "while($"."relatedRowArray = $"."db->fetch_array($"."SQLrelatedExecute)) {\n";
				$backListElements .= "\t$"."fieldName = $"."relatedRowArray[\"fieldname\"];\n";
				$backListElements .= "\t$"."relatedId = $"."relatedRowArray[\"id\"];\n";
				$backListElements .= "\t$"."bodyString .= \"\\"."n<div class='backListElement'>".$displayNameArray[$fieldName].": $"."fieldName</div>\";\n";
				$backListElements .= "}";
				break;
			case "none":
				break;
			default:
				echo " // <b>ERROR: Back List Display Type not recognized...</b>";
				break;
		}
		if ($displayType != "none") {
			$backListSQL = "$fieldName, ";
		}
	}
}

function writePHP($output, $fileName) {
	$myfile = getcwd() . "/admin/". $fileName;
	//echo "myfile = " . $myfile;
	echo "<a href='admin/". $fileName . "'>$fileName</a><br/>";
	$fh = fopen($myfile, 'w') or die("can't open file");
	fwrite($fh, $output);
	fclose($fh);
}

function createNav() {
	global $tableArray,$displayNameArray;
	$navSource = "";
	for ($i=0; $i < sizeof($tableArray);$i++) {
			$navSource .= "<div class='otherTable <?php if ($"."currentNavTable == \"".$tableArray[$i]."\") {echo \"currentTable\";} ?>'><a href='".$tableArray[$i]."_list.php'>".$displayNameArray[$tableArray[$i]]."</a></div>\n";
	}
	$fileName = "navlist.php";
	writePHP($navSource,$fileName);
}

function createWrite($tableName) {
	global $tableArray,$displayNameArray,$siteName,$inputElementsString, $fieldVarsString, $postDataVarsString, $db_password, $db_username, $db_host, $db_name;
	$writeHead = "<?php
	
require_once(\"database.php\");
$%id = $%_GET['id'];
$%foreignKey = $%_GET['foreignKey'];
$%bodyString = \"\";
if ($%id) {
	$%SQLreadQuery = \"SELECT * FROM $tableName WHERE id = $%id\";
	$%SQLreadResult = $%db->query($%SQLreadQuery);
	while ($%rowArray = $%db->fetch_array($%SQLreadResult)) {\n";
	$writeBody = "".
"	}						
}

include \"io_library.php\";
if (session_id() != \"\") {
	//session_start(); 
	//echo \"starting a new session now\";
}
session_start();
if ($%_GET['sessionDepth'] && $%_SERVER['HTTP_REFERER']) { // if it needs a ref URL and they have SERVER data
	$%sessionDepth =  'depth' . $%_GET['sessionDepth'];
	if (!$%_SESSION[$%_GET['sessionDepth']] || $%_SESSION[$%sessionDepth] == \"\") {
		//session_register($%sessionDepth);
		$%_SESSION[$%sessionDepth] = $%_SERVER['HTTP_REFERER'];
	}
	$%referringPage = $%_SESSION[$%sessionDepth]; // then give 'em what DEY need
}
//print_r($%_SESSION);
	
if (!$%referringPage)						// else send to parent 'list' page							
	$%referringPage = \"".$tableName."_list.php\";
?>
<html>
	<head>
		<title>$siteName > " .$displayNameArray[$tableName]. "</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='stylesheet' type='text/css' href='theme/ui.all.css'  />
	<link rel='stylesheet' type='text/css' href='main.css' />
	<link rel='stylesheet' type='text/css' href='jquery.Jcrop.css' />
	<link rel='stylesheet' type='text/css' media='screen'  href='colorpicker/css/colorpicker.css' />
	<link rel='stylesheet' type='text/css' href='ui.datepicker.css' />
	<link rel='stylesheet' type='text/css' href='jquery.autocomplete.css' />
	<link rel='stylesheet' href='jquery.timepickr.css' type='text/css'>
	<script type='text/javascript' src='jquery.js' ></script>
	<script type='text/javascript' src='jquery.flash.js'></script>
	<script type='text/javascript' src='jquery.jqUploader.js'></script>
	<script type='text/javascript' src='jquery.Jcrop.js'></script>
	<script type='text/javascript' src='write_actions.js'></script>
	<script type='text/javascript' src='nav_actions.js'></script>
	<script type='text/javascript' src='jquery.ajaxQueue.js'></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<script type='text/javascript' src='colorpicker/js/colorpicker.js'></script>
	<script type='text/javascript' src='ui.core.js'></script> 
	<script type='text/javascript' src='ui.datepicker.js'></script>
	<script type='text/javascript' src='jquery.ui.all.js'></script> 
	<script type='text/javascript' src='jquery.timepickr.js'></script> 
	<script type='text/javascript'>
		var refererURL = '<?php echo $%referringPage; ?>';
		var parent_id = '<?php echo $%id; ?>';
		var parent_table = '$tableName';
	</script>
	</head>
	<body>
	<div class='navigation'><div id='goTo'>Go To</div>
	<div id=\"tableList\">\n";
	$writeBody .= "<?php $"."currentNavTable = '$tableName'; include 'navlist.php'; ?>\n";
/*
	for ($i=0; $i < sizeof($tableArray);$i++) {
			$selectedClass = "";
			if ($tableArray[$i] == $tableName) {$selectedClass = "currentTable";}
			$writeBody .= "<div class='otherTable $selectedClass'><a href='".$tableArray[$i]."_list.php'>".$displayNameArray[$tableArray[$i]]."</a></div>\n";
	}
*/
	//$writeBody .= print_r($tableArray);
	$writeBody .= "</div>
	<div class='siteName'>$siteName</div><div id='currentTableTitle'><a href='".$tableName."_list.php'>".$displayNameArray[$tableName]."</a></div>
	</div>
	<div id=\"fieldsList\">
		<form id='$tableName"."Form' method=\"post\" action=\"<?php echo "."$"."referringPage; ?>\"><?php ";
	$writeFooter = " ?>
		<input type=\"hidden\" name=\"id\" value=\"<?php echo $%_GET['id'];?>\" />
		<?php 
		if ($"."_GET['linkedChild']) {
			echo \"<input type='hidden' name='linkedChild' value='\".\$"."_GET['linkedChild'].\"' />\";
		}
		?>
		<input type=\"hidden\" name=\"TABLENAME\" value=\"$tableName\" />
		<input type=\"submit\" value=\"Save\" id='saveRecord' />
		</form>
		<input type=\"submit\" value=\"Cancel\" id='cancelRecord' />
	</div>
	</body>
</html>";
	$pageSource = str_replace("%", "", $writeHead) . str_replace("%", "", $fieldVarsString) . str_replace("%", "", $writeBody) . $inputElementsString . str_replace("%", "", $writeFooter);
	//echo str_replace("%", "", $pageSource);
	
	echo "<h2>tableName = $tableName</h2> ";
	$fileName = $tableName . "_write.php";
	writePHP($pageSource,$fileName);
} //end function creatWrite()

function createList($tableName) {
	global $xml_data,$siteName,$tableArray,$displayNameArray,$postDataVarsString, $editPostDataSQLString, $fieldsForDeletionString, $inputElementsString, $fieldVarsString, $db_password, $db_username, $db_host, $db_name, $backListElements, $backListSQL,$newNamesPostDataSQLString, $newValuesPostDataSQLString, $listPageNav;
	$isSortable = $xml_data[$tableName]["SORTABLE"];
	$writePost = "<?php
require_once(\"database.php\");
$%_SESSION[$%sessionDepth] == \"\";
$%bodyString = \"\";
if ($%_SERVER['REQUEST_METHOD']==\"POST\") {
	$%id = $%_POST['id'];\n
	if ($%id) {
$postDataVarsString
		$%SQLeditQuery = \"UPDATE $tableName SET \";	
		$%SQLeditQuery .= \" $editPostDataSQLString \"; 
		$%SQLeditQuery .= \" WHERE id = '$%id' \";
		$%SQLeditResult = $%db->query($%SQLeditQuery);
	}
	else {
$postDataVarsString
		$%SQLnewQuery = \"INSERT INTO $tableName ($newNamesPostDataSQLString) VALUES ($newValuesPostDataSQLString)\";
		$%SQLnewResult = $%db->query($%SQLnewQuery);
	} //end if(id) else
} // end if (request method = POST)
";
	if ($isSortable) { $sortFlag = "ORDER BY sort_order ASC"; }
	$writeHead = "
$%SQLlistQuery = \"SELECT * FROM $tableName $sortFlag\";
$%SQLlistResult = $%db->query($%SQLlistQuery);
while ($%rowArray = $%db->fetch_array($%SQLlistResult)) {\n";

	$writeBody = "\n						
		$%bodyString .= \"\%n<div class='listPageButtons'><a class='editBtn positiveBtns' href='$tableName" . "_write.php?id=$%id'>EDIT</a><div class='deleteBtn'><a class='negativeBtns'>DELETE</a>\";
		$%bodyString .= \"\%n<div class='deleteConfirmation' id='deleteConfirmation$%id'>Do you really want to delete? <a class='deleteYes'>YES</a> // <a class='deleteNo'>NO</a></div>\";
		$%bodyString .= \"</div></div></div>\";
}
?>
<html>
	<head>
		<title>$siteName > " .$displayNameArray[$tableName]. "</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\" />
	<script src=\"jquery.js\" type='text/javascript'></script>
	<script src=\"jquery.ui.all.js\" type='text/javascript'></script>
	<script src=\"nav_actions.js\" type='text/javascript'></script>
	<script src=\"list_actions.js\" type='text/javascript'></script>
	<script type='text/javascript'>
	var jsTableName = '$tableName';

	$(function() {
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: \"ajax.php\",
			data: \"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\",
			cache: false,
			complete: function(){
				//alert(\"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\");
			}
		});

	};	

	jQuery.deleteAllFiles = function(id,field) {
		$fieldsForDeletionString		
	};		
});
	</script>
	</head>
	<body>
	
		<div class='navigation'><div id='goTo'>Go To</div>
	<div id=\"tableList\">\n";
	$writeBody .= "<?php $"."currentNavTable = '$tableName'; include 'navlist.php'; ?>\n";
/*
	for ($i=0; $i < sizeof($tableArray);$i++) {
			$selectedClass = "";
			if ($tableArray[$i] == $tableName) {$selectedClass = "currentTable";}
			$writeBody .= "<div class='otherTable $selectedClass'><a href='".$tableArray[$i]."_list.php'>".$displayNameArray[$tableArray[$i]]."</a></div>\n";
	}
*/
	//$writeBody .= print_r($tableArray);
	$writeBody .= "</div>
	<div class='siteName'>$siteName</div><div id='currentTableTitle'><a href='".$tableName."_list.php'>".$displayNameArray[$tableName]."</a></div>
	<div id='listBtns'><div id='listViewBtn'><img src='images/list-icon.gif' alt='List View' /></div><div id='gridViewBtn'><img src='images/grid-icon.gif' alt='Grid View' /></div></div>
	</div>";
	if ($isSortable) { $sortClass = "sortable"; }
	$writeBody .= "<div class='recordsList $sortClass'>
	<?php echo $%bodyString;";
	$writeFooter = " ?>
		<a class='newBtn positiveBtns' href='" . $tableName . "_write.php'>NEW RECORD</a>
	</div>
	\n
	</body>
</html>";
	$pageSource = str_replace("%", "", $writePost) . str_replace("%", "", $writeHead) . str_replace("%", "", $fieldVarsString) .  $backListElements . str_replace("%", "", $writeBody) . str_replace("%", "", $writeFooter);
	//echo "<br> <br> backlistelements:" . $backListElements;
	//echo "<br> <br> createList pagesource = " . $pageSource;
	//echo "<h2>tableName = $tableName</h2> ";
	$fileName = $tableName . "_list.php";
	writePHP($pageSource,$fileName);
}

function createGroupWrite($tableName) {
	global $fieldsForDeletionString,$tableArray,$displayNameArray,$siteName,$inputElementsString, $fieldVarsString, $postDataVarsString, $db_password, $db_username, $db_host, $db_name;
		$writeHead = "<?php if (!$"."_GET['addNew']) { ?><script type='text/javascript'>$(function() {
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: \"ajax.php\",
			data: \"tablename=\"+child_tableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\",
			cache: false,
			complete: function(){
				//alert(\"tablename=\"+child_tableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\");
			}
		});

	};	

	jQuery.deleteAllFiles = function(ID,field) {
		
		$fieldsForDeletionString		
	};		
});
</script>";
	$writeHead .= "<div class='groupItemsContainer'><?php
\n
if ($%id) {
	$%SQLread = \"SELECT * FROM $tableName WHERE $"."parentTable_SQL_where\";
	
$%SQLconnect = mysql_connect(\"$db_host\",\"$db_username\",\"$db_password\");
if (!$%SQLconnect) { echo(\"SQL Connection Error: \" . mysql_error() . \"\\n\");	}\n
else {
	$%SQLexecute = mysql_db_query(\"$db_name\",$%SQLread,$%SQLconnect);
	if (!$%SQLexecute) { echo(\"SQL Execution Error: \" . mysql_error() . \"\\n$%SQL\\n\");	}
	else {
		while($%rowArray = mysql_fetch_array($%SQLexecute,MYSQL_ASSOC)) {
			echo \"<div class='groupItem'>\";
			";
	$writeBody = "\n
			echo \"</div>\";
			}						
		}
	}
	mysql_close($%SQLconnect);
}
}
$%id =\"\";

?>
<div class='groupItem'><?";

$writeBottom = "?>
<div class='saveNewGroupItem'>Save</div>
</div>
</div>
";
	//$writeBody .= print_r($tableArray);
	$writeFooter = " ?>
		<input type=\"hidden\" class='formElement' name=\"id\" value=\"<?php echo $%id;?>\" />
		<div class='saveGroupItem'>SAVE ITEM</div> <div class='deleteGroupItem'>DELETE ITEM</div>
		<?";
	$pageSource = str_replace("%", "", $writeHead) . str_replace("%", "", $fieldVarsString)  .  $inputElementsString . str_replace("%", "", $writeFooter) . str_replace("%", "", $writeBody) . $inputElementsString . str_replace("%", "", $writeBottom);
	//echo str_replace("%", "", $pageSource);
	
	echo "<h2>tableName = $tableName</h2> ";
	$fileName = $tableName . "_groupwrite.php";
	writePHP($pageSource,$fileName);
} //end function creatGroupWrite()

function createGroupList ($tableName) {
	global $siteName,$tableArray,$displayNameArray,$postDataVarsString, $editPostDataSQLString, $fieldsForDeletionString, $inputElementsString, $fieldVarsString, $db_password, $db_username, $db_host, $db_name, $backListElements, $backListSQL,$newNamesPostDataSQLString, $newValuesPostDataSQLString, $listPageNav;
	$writePost = "<?php
$%SQLconnect = mysql_connect(\"$db_host\",\"$db_username\",\"$db_password\");
$%bodyString = \"\";
if ($%_SERVER['REQUEST_METHOD']==\"POST\") {
	$%id = $%_POST['id'];\n
	if ($%id) {
$postDataVarsString
		$%SQLedit = \"UPDATE $tableName SET \";	
		$%SQLedit .= \" $editPostDataSQLString \"; 
		$%SQLedit .= \" WHERE id = '$%id' \";
		$%SQLeditResult = mysql_db_query(\"$db_name\",\"$%SQLedit\",$%SQLconnect);
		if (!$%SQLeditResult) { echo(\"EDIT ERROR: \" . mysql_error() . \"$%SQLedit\");	}
	}
	else {
$postDataVarsString
		$%SQLnew = \"INSERT INTO $tableName ($newNamesPostDataSQLString) VALUES ($newValuesPostDataSQLString)\";
		$%SQLnewResult = mysql_db_query(\"$db_name\",\"$%SQLnew\",$%SQLconnect);
		if (!$%SQLnewResult) { echo(\"NEW ERROR: \" . mysql_error() . \"$%SQLnew\");	}
	}
}
";
	$writeHead = "$%SQLlist = \"SELECT * FROM $tableName WHERE $"."parentTable_SQL_where\";
	
if (!$%SQLconnect) { echo(\"SQL Connection Error: \" . mysql_error() . \"\\n\");	}\n
else {
	$%SQLexecute = mysql_db_query(\"$db_name\",$%SQLlist,$%SQLconnect);
	if (!$%SQLexecute) { echo(\"SQL Execution Error: \" . mysql_error() . \"\\n$%SQL\\n\");	}
	else {
		while($%rowArray = mysql_fetch_array($%SQLexecute,MYSQL_ASSOC)) {";

	$writeBody = "\n						
		$%bodyString .= \"\%n<div class='listPageButtons'><a class='editBtn positiveBtns' href='$tableName" . "_write.php?id=$%id&groupItem=\".$%parentTable.\"_write.php?id=\".$$"."parentTable_id.\"'>EDIT</a><div class='deleteBtn'><a class='negativeBtns' href='#'>DELETE</a>\";
		$%bodyString .= \"\%n<div class='deleteConfirmation' id='deleteConfirmation$%id'>Do you really want to delete? <a class='deleteYes'>YES</a> // <a class='deleteNo'>NO</a></div>\";
		$%bodyString .= \"</div></div></div>\";
		}
	}
}
mysql_close($%SQLconnect);
?>
	<script type='text/javascript'>
	var jsTableName = '$tableName';

	$(function() {
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: \"ajax.php\",
			data: \"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\",
			cache: false,
			complete: function(){
				//alert(\"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\");
			}
		});

	};	

	jQuery.deleteAllFiles = function(id,field) {
		$fieldsForDeletionString		
	};		
});
	</script>
	<script src=\"list_actions.js\" type='text/javascript'></script>
	
	<?php echo $%bodyString;";
	$writeFooter = " ?>
	\n
	<a class='newBtn positiveBtns' href='" . $tableName . "_write.php?foreignKey=<?php echo $$"."parentTable_id . \"&groupItem=\".$%parentTable.\"_write.php?id=\".$$"."parentTable_id; ?>'>NEW RECORD</a>
";
	$pageSource = str_replace("%", "", $writePost) . str_replace("%", "", $writeHead) . str_replace("%", "", $fieldVarsString) .  $backListElements . str_replace("%", "", $writeBody) . str_replace("%", "", $writeFooter);
	//echo "<br> <br> backlistelements:" . $backListElements;
	//echo "<br> <br> createList pagesource = " . $pageSource;
	//echo "<h2>tableName = $tableName</h2> ";
	//echo "<br> <br> backlistelements:" . $backListElements;
	//echo "<br> <br> createList pagesource = " . $pageSource;
	//echo "<h2>tableName = $tableName</h2> ";
	$fileName = $tableName . "_grouplist.php";
	writePHP($pageSource,$fileName);
} //end function creatGroupList()

function createGlobalData () {
	global $db_host;
	global $db_username;
	global $db_password;
	global $db_name;
	global $images_path,$audio_path,$movies_path,$thumbs_path;
	global $http_root,$server_root;
	
	if ($audio_path) {
	mkdir($server_root . $audio_path);
	}
	if ($images_path) {
	mkdir($server_root . $images_path);
	}
	if ($movies_path) {
	mkdir($server_root . $movies_path);
	}
	if ($thumbs_path) {
	mkdir($server_root . $thumbs_path);
	}
	
	$pageSource = "<?";
	$pageSource .= "\n$"."images_path = \"$images_path\";";
	$pageSource .= "\n$"."audio_path = \"$audio_path\";";
	$pageSource .= "\n$"."movies_path = \"$movies_path\";";
	$pageSource .= "\n$"."thumbs_path = \"$thumbs_path\";";
	$pageSource .= "\n";
	$pageSource .= "\n$"."db_host = \"$db_host\";";
	$pageSource .= "\n$"."db_username = \"$db_username\";";
	$pageSource .= "\n$"."db_password = \"$db_password\";";
	$pageSource .= "\n$"."db_name = \"$db_name\";";
	$pageSource .= "\n";
	$pageSource .= "\n$"."http_root = \"$http_root\";";
	$pageSource .= "\n$"."server_root = \"$server_root\";";

	$pageSource .= "\n?>";
	$fileName = "global_vars.php";
	writePHP($pageSource,$fileName);
}


function createLinkedTableList ($tableName) {
	global $siteName,$tableArray,$displayNameArray,$postDataVarsString, $editPostDataSQLString, $fieldsForDeletionString, $inputElementsString, $fieldVarsString, $db_password, $db_username, $db_host, $db_name, $backListElements, $backListSQL,$newNamesPostDataSQLString, $newValuesPostDataSQLString, $listPageNav,$xml_data;
	
	$isSortable = $xml_data[$tableName]["SORTABLE"];
	if ($isSortable) { $sortFlag = "ORDER BY $%linkTableName\".\".sort_order ASC"; }
	
	$writePost = "<?php
$%SQLconnect = mysql_connect(\"$db_host\",\"$db_username\",\"$db_password\");
$%bodyString = \"\";
if (!$%parentId) { $%parentId = $%_GET['id']; }\n
if ($%_SERVER['REQUEST_METHOD']==\"POST\") {
	$%id = $%_POST['id'];\n
	$%linkedChild = $%_POST['linkedChild'];\n
	if ($%linkedChild == '$tableName') {
		if ($%id) {
	$postDataVarsString
			$%SQLedit = \"UPDATE $tableName SET \";	
			$%SQLedit .= \" $editPostDataSQLString \"; 
			$%SQLedit .= \" WHERE id = '$%id' \";
			$%SQLeditResult = mysql_db_query(\"$db_name\",\"$%SQLedit\",$%SQLconnect);
			if (!$%SQLeditResult) { echo(\"EDIT ERROR: \" . mysql_error() . \"$%SQLedit\");	}
		}
		else {
	$postDataVarsString
			$%SQLnew = \"INSERT INTO $tableName ($newNamesPostDataSQLString) VALUES ($newValuesPostDataSQLString)\";
			$%SQLnewResult = mysql_db_query(\"$db_name\",\"$%SQLnew\",$%SQLconnect);
			if (!$%SQLnewResult) { echo(\"NEW ERROR: \" . mysql_error() . \"$%SQLnew\");	}
			$%newChildId = mysql_insert_id();
			$%SQLnewAssociation = \"INSERT INTO $%linkTableName (parent_id,child_id) VALUES ($%parentId,$%newChildId)\";
			$%SQLnewAssociationResult = mysql_db_query(\"$db_name\",\"$%SQLnewAssociation\",$%SQLconnect);
		}
	}
}
";
	$writeHead = "
	$%SQLlist = \"SELECT * FROM $%linkTableName LEFT JOIN $%childTableName ON $%childTableName\".\".id = $%linkTableName\".\".child_id WHERE $%linkTableName\".\".parent_id = $%parentId $sortFlag\";
	//echo $%SQLlist;
	
	
	//$%SQLlist = \"SELECT * FROM $tableName WHERE $"."parentTable_SQL_where\";
	
if (!$%SQLconnect) { echo(\"SQL Connection Error: \" . mysql_error() . \"\\n\");	}\n
else {
	$%SQLexecute = mysql_db_query(\"$db_name\",$%SQLlist,$%SQLconnect);
	if (!$%SQLexecute) { echo(\"SQL Execution Error: \" . mysql_error() . \"\\n$%SQL\\n\");	}
	else {
		while($%rowArray = mysql_fetch_array($%SQLexecute,MYSQL_ASSOC)) {";
		
		$linkCheck = " if ($%id) { ";

	$writeBody = "\n						
		$%bodyString .= \"\%n<div class='listPageButtons'><a class='editBtn positiveBtns' href='$tableName" . "_write.php?id=$%id&sessionDepth=\".($"."_GET['sessionDepth']+1).\"&linkedChild=\".$tableName.\"'>EDIT</a><a class='removeBtn negativeBtns'>REMOVE</a>\";
		$%bodyString .= \"</div></div>\";
		
		}
		else {
		$%SQLlinkCheck = \"DELETE $%linkTableName.* FROM $%linkTableName LEFT JOIN $%childTableName ON $%childTableName\".\".id = $%linkTableName\".\".child_id WHERE $%childTableName\".\".id IS NULL\";
		$%SQLlinkExecute = mysql_db_query(\"$db_name\",$%SQLlinkCheck,$%SQLconnect);

		}
		
		}
	}
}
mysql_close($%SQLconnect);
?>
	<script type='text/javascript'>
	var jsTableName = '$tableName';
	$(function() {
	
	

	
	
	
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: \"ajax.php\",
			data: \"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\",
			cache: false,
			complete: function(){
				//alert(\"tablename=\"+jsTableName+\"&id=\"+id+\"&field=\"+field+\"&AJAXtask=deleteFile\");
			}
		});

	};	

	jQuery.deleteAllFiles = function(id,field) {
		$fieldsForDeletionString		
	};		
});
	</script>
	
	<?php echo $%bodyString;";
	$writeFooter = " ?>
	\n
";
	$pageSource = str_replace("%", "", $writePost) . str_replace("%", "", $writeHead) . str_replace("%", "", $fieldVarsString) . str_replace("%", "", $linkCheck) .  $backListElements .  str_replace("%", "", $writeBody) . str_replace("%", "", $writeFooter);
	//echo "<br> <br> backlistelements:" . $backListElements;
	//echo "<br> <br> createList pagesource = " . $pageSource;
	//echo "<h2>tableName = $tableName</h2> ";
	//echo "<br> <br> backlistelements:" . $backListElements;
	//echo "<br> <br> createList pagesource = " . $pageSource;
	//echo "<h2>tableName = $tableName</h2> ";
	$fileName = $tableName . "_linkedtablelist.php";
	writePHP($pageSource,$fileName);
} //end function creatGroupList()




function createFrontEndSkeleton($tableName) {
	global $postDataVarsString, $editPostDataSQLString, $fieldsForDeletionString, $inputElementsString, $fieldVarsString, $db_password, $db_username, $db_host, $db_name, $frontListElements, $backListSQL,$newNamesPostDataSQLString, $newValuesPostDataSQLString, $listPageNav;
		$frontHead = "<?php
$%SQLconnect = mysql_connect(\"$db_host\",\"$db_username\",\"$db_password\");
$%SQLlist = \"SELECT * FROM $tableName\";
	
if (!$%SQLconnect) { echo(\"SQL Connection Error: \" . mysql_error() . \"\\n\");	}\n
else {
	$%SQLexecute = mysql_db_query(\"$db_name\",$%SQLlist,$%SQLconnect);
	if (!$%SQLexecute) { echo(\"SQL Execution Error: \" . mysql_error() . \"\\n$%SQL\\n\");	}
	else {
		while($%rowArray = mysql_fetch_array($%SQLexecute,MYSQL_ASSOC)) {;
			$%bodyString .= \"\n<div class='".$tableName."_record'>\";";
			
			$frontBody = "\n	
			$%bodyString .= \"\n</div>\";					
		}
	}
}
mysql_close($%SQLconnect);
?>
<html>
	<head>
		<title>$tableName Front</title>
	<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\" />
	</head>
	<body><?php echo $%bodyString;";
		$frontFooter = " ?>
	\n
	</body>
</html>";
		$pageSource = str_replace("%", "", $frontHead) . str_replace("%", "", $fieldVarsString) .  $frontListElements . str_replace("%", "", $frontBody) . str_replace("%", "", $frontFooter);
		$fileName = $tableName . ".php";
		writePHP($pageSource,$fileName);


}

?>