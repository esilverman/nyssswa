<?php

 $emailBody = "$firstName $lastName,\n<br/>"
  . "\n<br/>"
  . "You have received this message to help complete your NYSSSWA Membership Renewal. \n<br/>"
  . "\n<br/>"
  . "To continue the renewal process, follow this link: <a href='http://www.nyssswa.org/renew.php?id=$fingerprint'>http://www.nyssswa.org/renew.php?id=$fingerprint</a>\n<br/>"
	. "\n<br/>"
  . "If you have received this email in error, please ignore it. For any questions or comments please email info@nyssswa.org\n<br/>"
  . "\n<br/>"
  . "Thanks for your continued support!\n<br/>"
  . "-NYS School School Social Workers Association\n<br/>";



$to_name = "$firstName $lastName";
$to = "$email";
$subject = "NYSSSWA Member Renewal Link";

$from_name = "NYSSSWA";
$from = "no-reply@nyssswa.org";
$replyTo_name = "NYSSSWA Info";

/*
$emailHeader = "From: no-reply@nyssswa.org\n"
. "MIME-Version: 1.0\n"
. "Content-type: text/plain; charset=\"ISO-8859-1\"\n"
. "Content-transfer-encoding: 7bit\n";
  
mail($to, $subject, $emailBody, $emailHeader);
*/


// PHP SMTP version 
$mail = new PHPMailer();

$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->SMTPSecure = "tls";
$mail->Host		= "smtp.accountsupport.com";
$mail->Port		= 587;

$mail->Username = "support@nyssswa.org";
$mail->Password	= "nyssswa341";
/* $mail->SMTPDebug  = 2;  */

$mail->SetFrom($from, $from_name);
$mail->AddReplyTo($from, $replyTo_name);
$mail->AddAddress($to, $to_name);
$mail->Subject	= $subject;
$mail->MsgHTML($emailBody);


$result = $mail->Send();
echo $result ? 'Sent' : $mail->ErrorInfo;


?>