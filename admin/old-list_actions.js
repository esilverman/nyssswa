
$(function() {
	$(".currentTable").toggle(
		function () {
			$(".otherTable").fadeIn("fast");
			$(".currentPage").css("display","none");
		},
		function () {
			$(".otherTable").fadeOut("fast", function () {
			$(".currentPage").css("display","block");      });
		}
	);

	$(".deleteBtn").click(
		function() {
			$(".deleteConfirmation").hide();
			ID = this.parentNode.id;
			confirmationID = "#" + ID + " .deleteConfirmation";
			$(confirmationID).css({display:"inline"});
		}
	);
	$(".deleteYes").click(
		function() {
			ID = this.parentNode.parentNode.id;
			buttonsID = this.parentNode.id;
			$("#"+buttonsID).html('Deleting...');
			$.deleteAllFiles(ID);
			$.ajax({
				url: "ajax.php",
				data: "tablename="+jsTableName+"&id="+ID+"&AJAXtask=deleteRecord",
				cache: false,
				complete: function(){
					$("#"+ID).fadeOut();
				}
			});
		}
	);
	$(".deleteNo").click(
		function() {
			$(".deleteConfirmation").hide();
		}
	);
	$("#goTo").click(function () {
		$("#tableList").slideToggle();		
	});
	
	$("#tableList").hover(
		function() {},
		function() {
			$(this).hide();
		}
	);

});


