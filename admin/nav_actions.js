function _getVars(vars) {
	//console.log(getVars);
	if (typeof vars == 'object') {
		var r = '';
		for (i in vars) {
			//console.log(vars[i],getVars[vars[i]].length,getVars[vars[i]]);
			if (0) {
			
			} else {
				r += (getVars[vars[i]].length > 0) ? "&"+vars[i]+"="+getVars[vars[i]] : "";
			}
		}
	} else {
		varName = vars;
		r = (getVars[varName].length > 0) ? "&"+varName+"="+getVars[varName] : "";
	}
	//console.log(r);
	return getVars['currPage']+"?_=a3j0ss3"+r;
}


$(function() {
	$("#goTo").click(function () {
		$("#tableList").slideToggle();		
	});
	
	$("#tableList").hover(
		function() {},
		function() {
			$(this).hide();
		}
	);
	
	$("#viewCorruptBtn").click( function () {
		var currLocation = window.location+"";
		//console.log(currLocation.indexOf("corrupt"));
		if(currLocation.indexOf("corrupt") == -1) {
			window.location += "&corrupt=true";
		} else {
			alert("You are already viewing members with corrupt data only.");
		}
	});
	
	$("#viewAllMembersBtn").click( function () {
		uriString = _getVars(new Array('regionId', 'view','sortBy','sortField', 'fromDate', 'toDate'));

		window.location = uriString;
	});
	
	$("#sortBySelect").change( function () {	
		uriString = _getVars(new Array('regionId', 'corrupt', 'view', 'fromDate', 'toDate'));
		
		var sortByClause = $(this).find("option:selected").val();
		var sortField = $(this).find("option:selected").html();
		window.location = uriString+"&sortBy="+sortByClause+"&sortField="+sortField;
	});

	$("#membersRegionSelect, #accountRegionSelect").change( function () {
		uriString = _getVars(new Array('corrupt','sortBy','sortField', 'fromDate', 'toDate'));
		var regionId = $(this).find("option:selected").val();
		window.location = uriString+"&regionId="+regionId;
	});
	
	$("#viewAllRegionsBtn").click( function () {
		uriString = _getVars(new Array('corrupt','sortBy','sortField', 'fromDate', 'toDate'));
		uriString = uriString+"&view=all";
		
		window.location = uriString;
	});

	$("#showExpiringMembers").click( function () {
		var currLocation = window.location+"";
		//console.log(currLocation.indexOf("corrupt"));
		if(currLocation.indexOf("expiring") == -1) {
			window.location += "?expiring=true";
		} else {
			window.location = "email.php";
		}

	});
	
	$('#date_range_btn').click(function () {
		var fromDate = $('#from').val(),
				toDate = $('#to').val(),
				uriString = getVars['currPage']+"?_=a3j0ss3",
				dateRangeString = '&fromDate='+fromDate+'&toDate='+toDate;
		
		uriString = _getVars(new Array('regionId','corrupt','view','sortBy','sortField'));
		
		if ((fromDate.length > 0 && toDate.length > 0) && (fromDate !== toDate) ) {
				window.location = uriString+dateRangeString;
		} 
		else {
			if (fromDate === toDate) {
				$('#date_range .error_msg').html('You must provide two different dates.');
			} else {
				$('#date_range .error_msg').html('Please select both a start and end date.');
			}
			$('#date_range .required').addClass('error');
			$('#date_range .error_msg').show();
			return false;
		}
		
	});

	$('#date_range_reset').click(function () {
		window.location = _getVars(new Array('regionId','corrupt','view','sortBy','sortField'));		
	});


/*
	for (varName in getVars){
		console.log(varName+' = ',getVars[varName]);
	}
*/
});