<?php
	
require_once("database.php");
$id = $_GET['id'];
$foreignKey = $_GET['foreignKey'];
$refRegionId = $_GET['regionId'];
$refPage = $_GET['ref'];
$bodyString = "";
if ($id) {
	$SQLreadQuery = "SELECT * FROM members WHERE id = $id";
	$SQLreadResult = $db->query($SQLreadQuery);
	while ($rowArray = $db->fetch_array($SQLreadResult)) {
		$firstName = $rowArray["firstName"];
		$lastName = $rowArray["lastName"];
		$addressL1 = $rowArray["addressL1"];
		$city = $rowArray["city"];
		$state = $rowArray["state"];
		$zipcode = $rowArray["zipcode"];
		$phone = $rowArray["phone"];
		$email = $rowArray["email"];
		$contact = $rowArray["contact"];
		$region = $rowArray["region"];
		$yearsEnrolled = $rowArray["yearsEnrolled"];
		$lastEnrollment = $rowArray["lastEnrollment"];
		$membershipStatus = $rowArray["membershipStatus"];
		$fingerprint = $rowArray["fingerprint"];
		$paid = $rowArray["paid"];
		$id = $rowArray["id"];
	}						
}

include "io_library.php";
if (session_id() != "") {
	//session_start(); 
	//echo "starting a new session now";
}
session_start();
if ($_GET['sessionDepth'] && $_SERVER['HTTP_REFERER']) { // if it needs a ref URL and they have SERVER data
	$sessionDepth =  'depth' . $_GET['sessionDepth'];
	if (!$_SESSION[$_GET['sessionDepth']] || $_SESSION[$sessionDepth] == "") {
		//session_register($sessionDepth);
		$_SESSION[$sessionDepth] = $_SERVER['HTTP_REFERER'];
	}
	$referringPage = $_SESSION[$sessionDepth]; // then give 'em what DEY need
}
//print_r($_SESSION);
	
if (!$referringPage)						// else send to parent 'list' page							
	$referringPage = "members_list.php";
?>
<html>
	<head>
		<title>NYSSSWA.org > Members</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='stylesheet' type='text/css' href='theme/ui.all.css'  />
	<link rel='stylesheet' type='text/css' href='main.css' />
	<link rel='stylesheet' type='text/css' href='jquery.Jcrop.css' />
	<link rel='stylesheet' type='text/css' href='ui.datepicker.css' />
	<link rel='stylesheet' type='text/css' href='jquery.autocomplete.css' />
	<link rel='stylesheet' href='jquery.timepickr.css' type='text/css'>
	<script type='text/javascript' src='jquery.1.4.min.js' ></script>
	<script type='text/javascript' src='jquery.flash.js'></script>
	<script type='text/javascript' src='jquery.jqUploader.js'></script>
	<script type='text/javascript' src='jquery.Jcrop.js'></script>
	<script type='text/javascript' src='write_actions.js'></script>
	<script type='text/javascript' src='nav_actions.js'></script>
	<script type='text/javascript' src='jquery.ajaxQueue.js'></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<script type='text/javascript' src='ui.core.js'></script> 
	<script type='text/javascript' src='ui.datepicker.js'></script>
	<script type='text/javascript' src='jquery.ui.all.js'></script> 
	<script type='text/javascript' src='jquery.timepickr.js'></script> 
	<script type='text/javascript'>
		var refererURL = '<?php echo $referringPage; ?>';
		var parent_id = '<?php echo $id; ?>';
		var parent_table = 'members';
	</script>
	</head>
	<body>
	<div class='navigation'>
<?php $currentNavTable = 'members'; include 'navlist.php'; ?>
	<div class='siteName'>NYSSSWA.org</div><div id='currentTableTitle'><a href='members_list.php'>Members</a></div>
	</div>
	<div id="fieldsList">
		<form id='membersForm' method="post" action="<?php echo $referringPage; ?>"><?php echo "<label for='firstName'>First Name</label><input type='text' name='firstName' size='100' class='formElement membersInput' value='$firstName'>";
echo "<label for='lastName'>Last Name</label><input type='text' name='lastName' size='100' class='formElement membersInput' value='$lastName'>";
echo "<label for='addressL1'>Street Address</label><input type='text' name='addressL1' size='100' class='formElement membersInput' value='$addressL1'>";
echo "<label for='city'>City</label><input type='text' name='city' size='100' class='formElement membersInput' value='$city'>";
echo "<label for='state'>State</label><input type='text' name='state' size='100' class='formElement membersInput' value='$state'>";
echo "<label for='zipcode'>Zipcode</label><input type='text' name='zipcode' size='100' class='formElement membersInput' value='$zipcode'>";
echo "<label for='phone'>Phone Number</label><input type='text' name='phone' size='100' class='formElement membersInput' value='$phone'>";
echo "<label for='email'>Email</label><input type='text' name='email' size='100' class='formElement membersInput' value='$email'>";
			if ($id) {
				echo "<div class='date' id='lastEnrollment'><label for='lastEnrollment'>Last Enrollment Date</label><input class='hasdatepicker formElement membersInput' type='text' id='lastEnrollment_datepicker' value=\""  . strftime('%m',strtotime($lastEnrollment)) . "/" . strftime('%d',strtotime($lastEnrollment)) ."/". strftime('%Y',strtotime($lastEnrollment)) . "\" /><input type='hidden' name='lastEnrollment' id='lastEnrollment_datestore' value='$lastEnrollment'></div>";

			}
			else {
				echo "<div class='date' id='lastEnrollment'><label for='lastEnrollment'>Last Enrollment Date</label><input class='hasdatepicker formElement membersInput' type='text' id='lastEnrollment_datepicker' /><input type='hidden' name='lastEnrollment' id='lastEnrollment_datestore'></div>";
			}

echo "<label for='contact'>Contact</label><select name='contact' class='formElement membersInput'>
<option "; if ($contact == "Yes") {echo " selected='selected' ";}echo "value='Yes'>Yes</option>
<option "; if ($contact == "No") {echo " selected='selected' ";}echo "value='No'>No</option>
</select>
";
		echo "<div class='relationship'><label for='region'>Region</label><select name='region' class='dropDown_related formElement membersInput'><option value='$region'></option></select>
			<a class='dropDown_related_addBtn positiveBtns'>ADD OPTION</a>
			</div>
			<script type='text/javascript'>
			var region_child_tablename = 'regions';
			var region_child_fieldname = 'regionName';

			</script>";
			echo "<input class='formElement' type='hidden' name='yearsEnrolled' id='yearsEnrolledInput' value='$yearsEnrolled' />";
			echo "<div class='yearsEnrolled formElement'><b>Years Enrolled: <span class='yearsEnrolledVal'>$yearsEnrolled</span></b>
				<div class='addBtn btn'>+</div>
				<div class='subtractBtn btn'>&ndash;</div>
			</div>";
			echo "<input class='formElement' type='hidden' name='membershipStatus' value='$membershipStatus' />";echo "<input class='formElement' type='hidden' name='fingerprint' value='$fingerprint' />";echo "<label for='paid'>Fees are Paid</label><select name='paid' class='formElement membersInput'>
<option "; if ($paid == "0") {echo " selected='selected' ";}echo "value='0'>No, there is an outstanding balance.</option>
<option "; if ($paid == "1") {echo " selected='selected' ";}echo "value='1'>Yes. This account is paid up.</option>
</select>
";
 ?>
		<input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
		<?php 
		if ($_GET['linkedChild']) {
			echo "<input type='hidden' name='linkedChild' value='".$_GET['linkedChild']."' />";
		}
		?>
		<input type="hidden" name="TABLENAME" value="members" />
		<input type="hidden" name="refRegionId" value="<?php echo $refRegionId; ?>" />
		<input type="hidden" name="refPage" value="<?php echo $refPage; ?>" />

		<input type="submit" value="Save" id='saveRecord' />
		</form>
		<input type="submit" value="Cancel" id='cancelRecord' />
	</div>
	</body>
</html>