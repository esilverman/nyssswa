<?
// 12-29-09 Changed fileNameThumb to exclude non alphanumeric chars

$tableName = $_GET['tablename'];
$id = $_GET['id'];

global $recordId;
$recordId = $_GET['recordId'];

$field = $_GET['field'];
global $varName;
$varName = $_GET['varName'];
$child_tablename = $_GET['child_tablename'];
$child_fieldname = $_GET['child_fieldname'];
$link_tablename = $_GET['link_tablename'];
$currentValue = $_GET['currentValue'];
$parentId = $_GET['parent_recordID'];

$sortByValue = $_GET['sortByValue'];
$region = $_GET['region'];
$regionName = $_GET['regionName'];
$fromDate = $_GET['fromDate'];
$toDate = $_GET['toDate'];

$fileName = $_GET['fileName'];
global $AJAXtask;
$AJAXtask = $_GET['AJAXtask'];
global $currFileType;
$currFileType = strtoupper($_GET['currFileType']);
global $isThumb;
$isThumb = $_GET['isThumb'];

//##### jCrop vars parsed here
$xCoord = $_GET['xCoord'];
$yCoord = $_GET['yCoord'];
$width = $_GET['width'];
$height = $_GET['height'];
$targetWidth = $_GET['targetWidth'];
$targetHeight = $_GET['targetHeight'];

include "io_library.php";
include "global_vars.php";

switch ($AJAXtask) {
	case "deleteRecord":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLdelete = " DELETE FROM $tableName WHERE id = $id ";
		mysql_db_query($db_name, $SQLdelete, $SQLconnect);//remember to write out db_name here
		
		break;
	case "deleteFile":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLfileToDelete = "SELECT $field AS fileToDelete FROM $tableName WHERE id = $id ";
		echo $SQLfileToDelete;
		$SQLexecute = mysql_db_query($db_name, $SQLfileToDelete, $SQLconnect);//remember to write out db_name here
		while($rowArray = mysql_fetch_array($SQLexecute,MYSQL_ASSOC)) {
			$file = $rowArray["fileToDelete"];
			$relativePath = str_replace($http_root, "", $file);
			$fullPath = $server_root . $relativePath;
			unlink($fullPath);
		}
		break;
	case "upload":	
		$fileUploadString = fileUploadDisplay($fileName, true);
		// added boolean on fileUploadDisplay() as 2nd param for margovswimwear.com display issues
		// If you want to remove it, alter fileUploadDisplay()
		// on io_library.php to accept 1 param only
		echo $fileUploadString;									
		break;
	case "crop":
		$targ_w = $targetWidth;
		$targ_h = $targetHeight;
		$jpeg_quality = 90;
		$png_quality = 1;
		$filePathArray = explode("/",$fileName);
		$fileWithEXT = $filePathArray[sizeof($filePathArray) - 1];
		$filePathArray[sizeof($filePathArray) - 1] = "";
		$fileNameArray = explode(".",$fileWithEXT);
		$fileEXT = array_pop($fileNameArray);
		$newFileArray = array();
		$newFileArray = $fileNameArray;
		$origFileBase = rawurlencode(implode(".",$fileNameArray));
		$newFileBase = rawurlencode(implode("_",$newFileArray));
		$fileNameArray[sizeof($fileNameArray) - 2] = $fileBase;
		$thumbSuffix = "_thumb";
		
		$safeFilePath =  implode("/", $filePathArray) . $origFileBase . ".$fileEXT";
		$newFileBase = preg_replace("/[^a-zA-Z0-9\s]/", "", $newFileBase);
		$thumbFilePath = $newFileBase . $thumbSuffix . ".$fileEXT";

		switch (strtoupper($fileEXT)) {
			case "JPEG":
			case "JPG":
				$img_r = imagecreatefromjpeg($safeFilePath);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xCoord,$yCoord,$targ_w,$targ_h,$width,$height);
				// compensate for faded color on crop
				imagefilter($dst_r, IMG_FILTER_BRIGHTNESS, -16);
				imagefilter($dst_r, IMG_FILTER_CONTRAST, -2);
				imagefilter($dst_r, IMG_FILTER_COLORIZE, 5, 0, 0);
				imagejpeg($dst_r,$server_root  . $thumbs_path . $thumbFilePath,$jpeg_quality);
				break;
			case "PNG":
				$img_r = imagecreatefrompng($safeFilePath);
				//echo "thumbFilePath : " . $thumbFilePath . "\n";
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xCoord,$yCoord,$targ_w,$targ_h,$width,$height);
				imagepng($dst_r,$server_root . $thumbs_path . $thumbFilePath,$png_quality);
				break;
			case "GIF":
				$img_r = imagecreatefromgif($safeFilePath);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$img_r,0,0,$xCoord,$yCoord,$targ_w,$targ_h,$width,$height);
				imagegif($dst_r,$server_root . $thumbs_path . $thumbFilePath);
				break;
			default:
				echo "ERROR: Unrecognized File Type to Crop...";
		}	
	
		imagedestroy($dst_r);
		//echo "safe file path:".$safeFilePath;
		echo $http_root . $thumbs_path . $thumbFilePath;
		break;
	case "populateDropDown":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLdropDown = "SELECT $child_fieldname AS fieldname, id FROM $child_tablename";
		$SQLexecute = mysql_db_query($db_name, $SQLdropDown, $SQLconnect);//remember to write out db_name here
		while($rowArray = mysql_fetch_array($SQLexecute,MYSQL_ASSOC)) {
			$fieldName = $rowArray["fieldname"];
			$id = $rowArray["id"];
			echo "<option value=\"$id\"";
			if ($id == $currentValue)
				echo "selected=\"selected\" ";
			echo ">$fieldName</option>";
		}
		break;
	case "populateCoursesBackend":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLdropDown = "SELECT * FROM courses";
		$SQLexecute = mysql_db_query($db_name, $SQLdropDown, $SQLconnect);//remember to write out db_name here
		while($rowArray = mysql_fetch_array($SQLexecute,MYSQL_ASSOC)) {
			$title = $rowArray["title"];
			$session = $rowArray["session"];
			$id = $rowArray["id"];
			$SQLsessionQuery = "SELECT title FROM sessions WHERE id = $session";
			$SQLsessionResult = mysql_db_query($db_name, $SQLsessionQuery, $SQLconnect);
			while ($SQLsessionArray = mysql_fetch_array($SQLsessionResult,MYSQL_ASSOC)) {
				$sessionTitle = $SQLsessionArray['title'];
			}
			echo "<option value=\"$id\"";
			if ($id == $currentValue)
				echo "selected=\"selected\" ";
			echo ">$title ($sessionTitle)</option>";
		}
		break;
	case "saveNewOpt":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLnewOpt = "INSERT INTO " . $_GET['child_tablename'] . " (".$_GET['child_fieldname'].") VALUES ('".$_GET['currentValue']."')";
		$SQLexecute = mysql_db_query($db_name, $SQLnewOpt, $SQLconnect);
		echo mysql_insert_id();
		break;
	case "saveGroupItem":
		$groupItemDataArray = array();
		foreach ($_GET as $key => $value) {
			if (($key != "child_tablename") && ($key != "id") && ($key != "_") && ($key != "AJAXtask")) 
				$groupItemDataArray[] = "$key = '$value'";
		}
		$groupItemDataStr = implode(", ", $groupItemDataArray);
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		echo $SQLsaveGroupItem = "UPDATE " . $_GET['child_tablename'] . " SET $groupItemDataStr WHERE id = '" . $_GET['id'] ."'";
		$SQLexecute = mysql_db_query($db_name, $SQLsaveGroupItem, $SQLconnect);

		break;
	case "saveNewGroupItem":
		$groupItemKeyArray = array();
		$groupItemValueArray = array();
		foreach ($_GET as $key => $value) {
			if (($key != "child_tablename") && ($key != "id") && ($key != "_") && ($key != "AJAXtask")) {
				$groupItemKeyArray[] = "$key";
				$groupItemValueArray[] = "'$value'";
			}
		}
		$groupItemKeyStr = implode(", ", $groupItemKeyArray);
		$groupItemValueStr = implode(", ", $groupItemValueArray);
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		echo $SQLnewGroupItem = "INSERT INTO " . $_GET['child_tablename'] . " ($groupItemKeyStr) VALUES ($groupItemValueStr)";
		//INSERT INTO images (image,image_thumb,Table3_id) VALUES ('ddd','$image_thumb','$Table3_id')
		$SQLexecute = mysql_db_query($db_name, $SQLnewGroupItem, $SQLconnect);

		break;
	case "deleteGroupItem":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$child_tableName = $_GET["child_tablename"];
		$SQLdelete = " DELETE FROM $child_tableName WHERE id = $id ";
		echo $SQLdelete;
		mysql_db_query($db_name, $SQLdelete, $SQLconnect);//remember to write out db_name here
		break;
	case "autocompleteSearch":
		$parentId = $_GET['parent_recordID'];
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		//$SQLautocompleteSearch = "SELECT child_id FROM $link_tablename WHERE parent_id = '$parentId'";
		$SQLautocompleteSearch = "SELECT $child_tablename.$child_fieldname AS fieldname, $child_tablename.id AS id FROM $child_tablename WHERE $child_tablename.id NOT IN (SELECT child_id FROM $link_tablename WHERE parent_id = '$parentId')";
		//$SQLautocompleteSearch = "SELECT $child_tablename.$child_fieldname AS fieldname, $child_tablename.id AS id, $link_tablename.parent_id AS parent_id FROM $child_tablename LEFT JOIN $link_tablename ON  $child_tablename.id = $link_tablename.child_id WHERE $link_tablename.parent_id = '$parentId' ORDER BY fieldname";  //hard code these vars in the extra params
		/*
		SELECT public_art_groups.title AS fieldname, public_art_groups.id AS id FROM public_art_groups WHERE public_art_groups.id NOT IN (SELECT child_id FROM link_table_test WHERE parent_id = '28')
		*/
		//echo $SQLautocompleteSearch . "\n";
		$SQLexecute = mysql_db_query($db_name, $SQLautocompleteSearch, $SQLconnect);
		$items = array();
		//SELECT * FROM $child_tablename LEFT JOIN $linktablename ON $linktablename.child_id = $child_tablename.id WHERE $linktablename.child_id IS NULL
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		while($rowArray = mysql_fetch_array($SQLexecute,MYSQL_ASSOC)) {
			$fieldName = $rowArray["fieldname"];
			$id = $rowArray["id"];
			$parent_id = $rowArray["parent_id"];
			$items[$fieldName] = $id;
			//echo $parent_id . "\n";
		}
		//print_r($items);
		foreach ($items as $key=>$value) {
			if (strpos(strtolower($key), $q) !== false) {
				//echo htmlspecialchars("$key|$value\n");
				echo strip_tags("$key|$value\n");
			}
		}
		break;
	case "autocompleteSave":
		$child_recordID = $_GET['child_recordID'];
		$SQLautocompleteSave .= "INSERT INTO " . $_GET['link_tablename'] . " (child_id, parent_id) VALUES ($child_recordID, ". $_GET['parent_recordID'] .")";
		//echo $SQLautocompleteSave;

		/*$child_recordIDs = explode(",", $_GET['child_recordIDs']);
				foreach ($child_recordIDs as $child_recordID) {
					if($child_recordID != "")
						$SQLautocompleteSave .= "INSERT INTO " . $_GET['link_tablename'] . " (child_id, parent_id) VALUES ($child_recordID, ". $_GET['parent_recordID'] .");\n";
		}*/

		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		//INSERT INTO images (image,image_thumb,Table3_id) VALUES ('ddd','$image_thumb','$Table3_id')
		//echo "\n db_name: $db_name \n $SQLautocompleteSave: $SQLautocompleteSave"+
		//"\n SQLconnect: ($db_host , $db_username, $db_password" ;
		//echo $SQLautocompleteSave;
		$SQLexecute = mysql_db_query($db_name, $SQLautocompleteSave, $SQLconnect);//remember to write out db_name here
		$linkTableName = $_GET['link_tablename'];
		$childTableName = $_GET['child_tablename'];
		$parentId = $_GET['parent_recordID'];
		include $_GET['child_tablename'] . "_linkedtablelist.php";
		break;
	case "linkedTableRemove":
		$SQLlinkedTableRemove .= "DELETE FROM " . $_GET['linkedTableName'] . " WHERE child_id='" . $_GET['child_id']."' AND parent_id='" . $_GET['parent_id'] . "'";
		echo $SQLlinkedTableRemove;
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLexecute = mysql_db_query($db_name, $SQLlinkedTableRemove, $SQLconnect);//remember to write out db_name here
		break;
	case "sortorderUpdate":
/*
		$serializedorder = $_GET['serializedOrder'];
		echo $serializedorder;
*/
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		foreach($_GET['order'] as $order => $itemid) :
			$sqlorderupdate = "UPDATE $tableName SET sort_order = '$order' WHERE id = '$itemid'";
			echo $sqlorderupdate . "\n";
			$SQLexecute = mysql_db_query($db_name, $sqlorderupdate, $SQLconnect);//remember to write out db_name here
		endforeach;
		break;
	case "linksortorderUpdate":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		foreach($_GET['order'] as $order => $itemid) :
			$sqlorderupdate = "UPDATE $link_tablename SET sort_order = '$order' WHERE child_id = '$itemid' AND parent_id = '$parentId'";
			echo $sqlorderupdate . "\n";
			$SQLexecute = mysql_db_query($db_name, $sqlorderupdate, $SQLconnect);//remember to write out db_name here
		endforeach;
		break;
	/* 	!printExcelLink */
	case "printExcelLink":
		$excelLink = "excelgen/print_excel.php?sortByValue=$sortByValue&region=$region&regionName=$regionName&fromDate=$fromDate&toDate=$toDate";
		echo "<a href='$excelLink'>Click this link to save your Excel file.</a>";
		break;
	/* !addRegRecord */
	case "addRegRecord":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);

		$fingerprint = $_GET['fingerprint'];
		
		// Check if record exists
		$SQLcheckRecordQuery = "SELECT * FROM registration WHERE fingerprint = '$fingerprint' and date = CURDATE() ";
		$SQLcheckRecordExecute = mysql_db_query($db_name, $SQLcheckRecordQuery, $SQLconnect);//remember to write out db_name here
		$recordExists = mysql_affected_rows() > 0;
				
		// Update the registration table
		$SQLregistrationQuery = "INSERT INTO registration (fingerprint, date) VALUES ( '$fingerprint', CURDATE() )";
		
		if ($recordExists) {
			echo "A record of this registration already exists";
		} else {
			$SQLexecute = mysql_db_query($db_name, $SQLregistrationQuery, $SQLconnect);//remember to write out db_name here
			echo "Successfully added registration record for fingerprint [$fingerprint]";
		}
		break;
	default:
		echo "<h2>ERROR: AJAX task not supported...</h2>";
		break;
}





/*
	case "facelistSearch":
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		$SQLdropDown = "SELECT $child_fieldname AS fieldname, id FROM $child_tablename";  //hard code these vars in the extra params
		$SQLexecute = mysql_db_query($db_name, $SQLdropDown, $SQLconnect);
		$items = array();
		
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		while($rowArray = mysql_fetch_array($SQLexecute,MYSQL_ASSOC)) {
			$fieldName = $rowArray["fieldname"];
			$id = $rowArray["id"];
			$items[$fieldName] = $id;
		}
		//print_r($items);
		foreach ($items as $key=>$value) {
			if (strpos(strtolower($key), $q) !== false) {
				//echo htmlspecialchars("$key|$value\n");
				echo strip_tags("$key|$value\n");
			}
		}
		break;
	case "facelistSave":
		$child_recordID = $_GET['child_recordID'];
		$SQLautocompleteSave .= "INSERT INTO " . $_GET['link_tablename'] . " (child_id, parent_id) VALUES ($child_recordID, ". $_GET['parent_recordID'] .")";

		/*$child_recordIDs = explode(",", $_GET['child_recordIDs']);
				foreach ($child_recordIDs as $child_recordID) {
					if($child_recordID != "")
						$SQLautocompleteSave .= "INSERT INTO " . $_GET['link_tablename'] . " (child_id, parent_id) VALUES ($child_recordID, ". $_GET['parent_recordID'] .");\n";
		}*/
		
/*
		$SQLconnect = mysql_connect($db_host,$db_username,$db_password);
		//INSERT INTO images (image,image_thumb,Table3_id) VALUES ('ddd','$image_thumb','$Table3_id')
		//echo "\n db_name: $db_name \n $SQLautocompleteSave: $SQLautocompleteSave \n SQLconnect: ($db_host , $db_username, $db_password" ;
		echo $SQLautocompleteSave;
		$SQLexecute = mysql_db_query($db_name, $SQLautocompleteSave, $SQLconnect);//remember to write out db_name here
		break;
*/
?>