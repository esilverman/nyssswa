<html>
<head>
	<link rel="stylesheet" type="text/css" href="main.css" />
	<link rel="stylesheet" type="text/css" href="theme/ui.all.css" />
</head>
<body>
	
<div class='navigation'>
	<?php $currentNavTable = 'excel'; include 'navlist.php'; ?>
	<div class='siteName'>NYSSSWA.org</div>
	<div id='currentTableTitle'><a href='selector.php'>Excel Exporter</a></div>
	<div id='listBtns'>
		<div id='listViewBtn'><img src='images/list-icon.gif' alt='List View' /></div>
		<div id='gridViewBtn'><img src='images/grid-icon.gif' alt='Grid View' /></div>
	</div>
</div>
<div id='excelWrapper' class='recordsList'>
	<div id="excelInstructions">
		Select the options for the Excel spreadsheet you would like to export and then click "Submit"
	</div>
	<form id='excelForm' method='get'>
		<label for='classOf'>Region</label>
		<?php $currentNavTable = 'excel'; require_once('../regions.php'); ?>
		<label for="sortByDropDown">Sort By</label>
		<select id="excelSortByDropDown">
			<option value="region ASC">Region</option>
			<option value="membershipStatus ASC">Membership Status</option>
			<option value="paid ASC">Payment Status</option>
			<option value="lastName ASC" selected="selected">Last Name: A-Z</option>
			<option value="lastName DESC">Last Name: Z-A</option>
		</select>
	</form>
		<?php require_once("excelgen/date_range.php"); ?>
	<button id='excelSubmitBtn'>Submit</button>
	<div id="mailInstrux">
		<h2>Mail Chimp Instructions</h2>
		<ol>
			<li>Choose your Excel output options and download your file.</li>
			<li>Open the file and save it as a ".csv" file in a place where you know how to find it again.</li>
			<li>Login to Mail Chimp</li>
			<li>Click the "Lists" Tab</li>
			<li>To Add A New List : </li>
				<ol type="a">
					<li>Click the "Create New List" button on the left hand side.</li>
					<li>Fill out the new list form properly and hit "save" when you're finished.</li>
					<li>When you are prompted after clicking save, click "import my list".</li>
					<li>Select the "Upload File" option (the first option).</li>
					<li>Click the "browse" button at the bottom right side of the page.</li>
					<li>** IMPORTANT : Check off "Auto-update my existing list" before clicking "import list" </li>
					<li>Locate the CSV file you saved earlier and click OK.</li>
					<li>Finally, click "Import List".</li>
					<li>Name the fields appropriately but DELETE all fields after the "Email Address" field.</li>
					<li>Click "all done" above the list of fields.</li>	
				</ol>
			<li>To Update An Existing List : </li>
				<ol>
					<li>Start from the "Lists" tab again.</li>
					<li>Locate the list you would like to update and click the "import" button for that list.</li>
					<li>Follow steps "d" through "i" above.</li>
				</ol>
		</ol>
	</div>

</div>	
	<script src="jquery.js" type='text/javascript'></script>
	<script src="jquery.ui.all.js" type='text/javascript'></script>
	<script src="jquery.ui.widget.js" type='text/javascript'></script>
	<script src="ui.core.js" type='text/javascript'></script>
	<script src="ui.datepicker.js" type='text/javascript'></script>
	<script src="../js/excel.js" type='text/javascript' ></script>
	<script src="nav_actions.js" type='text/javascript' ></script>
	<script type='text/javascript'>
		currentPage = "selector";
	</script>


</body>
</html>

