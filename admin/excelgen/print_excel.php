<?php
	/* 
	 * @version V1.0 2002/July/18 (c) Erh-Wen,Kuo (erhwenkuo@yahoo.com). All rights reserved.
	 * Released under both BSD license and Lesser GPL library license. 
	 * Whenever there is any discrepancy between the two licenses, 
	 * the BSD license will take precedence. 
	 *
	 * purpose: This is a example how to use "sql2excel" class to write mysql sql content
	 *          to excel file format and stream the output to user's browser directly.
	 */
	require_once('../global_vars.php'); 
	include_once('sql2excel.class.php');
		
	//the query string you want to show
	$region = $_GET['region'];
	$regionName = $_GET['regionName'];
	
	if ($region == "") {
		$regionSQL = "";
	} else {
		$regionSQL = "WHERE region = $region";
	}
	
	$dateClause = "";
	if( strlen($_GET["fromDate"]) > 0 && strlen($_GET["toDate"]) > 0 ) { // If They're both set
		$fromDate = date ( 'Y-m-d', strtotime ( $_GET["fromDate"]) );
		$toDate = date ( 'Y-m-d', strtotime ( $_GET["toDate"]) );
		$dateClause = "WHERE lastEnrollment BETWEEN '$fromDate' AND '$toDate'";
	} else if ( strlen($_GET["fromDate"]) > 0 ) { // if just from is set
		$fromDate = date ( 'Y-m-d', strtotime ( $_GET["fromDate"]) );
		$dateClause = "WHERE lastEnrollment >= '$fromDate' AND CURDATE()";
	} else if( strlen($_GET["toDate"]) > 0 ) { // if just from is set
		$toDate = date ( 'Y-m-d', strtotime ( $_GET["toDate"]) );
		$dateClause = "WHERE lastEnrollment BETWEEN '1980-01-01' AND '$toDate'";
	}

	$sortByValue = $_GET['sortByValue'];
	$query="SELECT * FROM members $regionSQL $dateClause ORDER BY $sortByValue";
	
	$today = getdate();
	$date = $today["mon"]."-".$today["mday"]."-".$today["year"];
	$xlFileName = "$regionName-$date";
	
	//echo $query."</br>";
	//echo $xlFileName;
	//initiating Sql2Excel class instance
	$excel=new Sql2Excel($db_host,$db_username,$db_password,$db_name,$xlFileName);
	//Output excel file to user's browser
	$excel->ExcelOutput($query);
	//echo "<h1>Exel Generate Completed!!</h1>";
?>