		<div id="date_range">
			<h4>Show registrants by date:</h4>
			<label for="from">From</label>
			<input type="text" id="from" name="from" class="required" value="<?php echo $_GET["fromDate"];?>"/>
			<label for="to">to</label>
			<input type="text" id="to" name="to" class="required" value="<?php echo $_GET["toDate"];?>"/>	
			<button id="date_range_btn" class="css3Button" type="button">Show Results</button>
			<button id="date_range_reset" class="css3Button" type="button">Show All Dates</button>
			<span class="error_msg">Please select both a start and end date.</span>
		</div><!-- End date_range -->
