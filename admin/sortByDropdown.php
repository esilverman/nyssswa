		<div id="sortByDropdown">
			<label>Select A Field To Sort By: </label>
			<select class="required" name="sortBy" id="sortBySelect">
				<option value="" selected="selected">- Select a Field -</option>
				<option value="lastEnrollment DESC">Date Enrolled: New to Old</option>
				<option value="lastEnrollment ASC">Date Enrolled: Old to New</option>
				<option value="paid ASC">Payment Status</option>
				<option value="lastName ASC">Last Name: A-Z</option>
				<option value="lastName DESC">Last Name: Z-A</option>
			</select>
		</div>
