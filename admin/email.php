<?php
require_once("database.php");
$emailsString = "<h3>All Member Emails:</h3>";
$emailsByClass = "<h3>Emails By Region</h3>";
$MEMBER_COUNT = 0;

/* Convert Region Table to Array */
$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
$regions[] = "0 index";
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionId = $SQLregionsArray["id"];
	$regionName = $SQLregionsArray["regionName"];
	$regions[$regionId] = $regionName;
}

$expiring = false;
if($_GET['expiring']) {$showExpiring = true;}

// display results if and only if there is an explicit
// request for some or all of the results
foreach ( $regions as $regionId => $regionName) { 
	if ($regionId !== 0) {
		$headerString = "Member Emails for [<i>$regionName</i>]";
		$numEmails = 0;
		$emailsString = "";
		$SQLlistQuery = "SELECT * FROM members WHERE region = $regionId ORDER BY lastEnrollment DESC, lastName ASC";
/* 		$SQLlistQuery = "SELECT * FROM members WHERE region = $regionId AND membershipStatus = 1 ORDER BY paid ASC, lastName ASC"; */
		$SQLlistResult = $db->query($SQLlistQuery);
		while ($rowArray = $db->fetch_array($SQLlistResult)) {
			$lastEnrollment = $rowArray["lastEnrollment"];
			$email = $rowArray["email"];
			//echo "Last Enrollment: ".strftime('%m',strtotime($lastEnrollment))."-".strftime('%d',strtotime($lastEnrollment))."-".strftime('%Y',strtotime($lastEnrollment))."\n";

			$willExpire = false;
			$enrollmentMonth = (int)strftime('%m',strtotime($lastEnrollment));
			$enrollmentYear = (int)strftime('%Y',strtotime($lastEnrollment));
			$beforeAprilFirst = ( $enrollmentMonth < 4 ) ? 1 : 0;
			$wasThisYear = ( $enrollmentYear == date("Y") ) ? 1 : 0;
			$wasLastYear = ( $enrollmentYear == (date("Y")-1) ) ? 1 :0;;
			// The membership will expire in one of two conditions:
			// 1. The member enrolled this calendar year, before the first of April
			// 2. The member enrolled last calendar year, on or after the first of April
			if ( ($beforeAprilFirst && $wasThisYear) || ($wasLastYear && !$beforeAprilFirst) ){
				$willExpire = true;
			}
			$validEmail = ( substr_count($email,"@") == 1);
			if (strlen($email) > 0 && $validEmail){
				if ( ($showExpiring && $willExpire) || (!$showExpiring) ) {
					$emailsString .= "$email, ";
					$numEmails ++;
				}
			}
			
		} //end while
		$bodyString .= "<h2 class='regionEmailHeader'>$headerString <span class='totalEmails'>Num. of Valid Emails : $numEmails</span></h2>"; 
		$bodyString .= "<p>$emailsString</p>";
	}
} //end foreach

?>

<html>
	<head>
		<title>NYSSSWA.org > Emails</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel="stylesheet" type="text/css" href="main.css" />
	</head>
	<body>
	
	<div class='navigation'>
		<?php $currentNavTable = 'email'; include 'navlist.php'; ?>
		<div class='siteName'>NYSSSWA.org</div>
		<div id='currentTableTitle'><a href='email.php'>Email Lists</a></div>
	</div>
	<div id="emailLists" class='recordsList'>
		<p>To visit the Excel printout page and view Mail Chimp instructions <a href="excel.php">click here</a>.</p>
		<button type="button" id="showExpiringMembers">
		<?php if (!isset($_GET['expiring']) ) { echo "Show Only Expiring Members"; } else { echo "Show All Members"; } ?>
		</button>
		<div style="clear:both;"></div>
		<?php echo $bodyString; ?>
	</div>

	<?php include_once("uriJS.php");?>
	<script src="jquery.js" type='text/javascript'></script>
	<script src="jquery.ui.all.js" type='text/javascript'></script>
	<script src="nav_actions.js" type='text/javascript'></script>
	<script src="list_actions.js" type='text/javascript'></script>
	<script src="../js/accounting.js" type='text/javascript'></script>

</body>
</html>

