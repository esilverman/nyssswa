<?php

/*
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
jqUploader serverside example: (author : pixeline, http://www.pixeline.be)

when javascript is available, a variable is automatically created that you can use to dispatch all the possible actions

This file examplifies this usage: javascript available, or non available.

1/ a form is submitted
1.a javascript is off, so jquploader could not be used, therefore the file needs to be uploaded the old way
1.b javascript is on, so the file, by now is already uploaded and its filename is available in the $_POST array sent by the form

2/ a form is not submitted, and jqUploader is on
jqUploader flash file is calling home! process the upload.



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

*/

global $relFilePath;
$isUploading = 1;

include "global_vars.php";
include "io_library.php";

$fileDisplayString = fileUploadDisplay(basename($_FILES['Filedata']['name']), true);
// added boolean on fileUploadDisplay() as 2nd param for margovswimwear.com display issues
// If you want to remove it, alter fileUploadDisplay()
// on io_library.php to accept 1 param only

$uploadDir = $server_root . $relFilePath;
$uploadFile = $uploadDir . basename($_FILES['Filedata']['name']);


$whatever1 = print_r($_POST);
$whatever2 = print_r($_GET);

$output = "Upload Dir : $uploadDir <br/><br/>POST : $whatever1";
$output .= "<br /><br />GET : $whatever2 ";
$output .= "<br /><br />post[sumbit]: " . $_POST['submit'];
$output .= "<br /><br />GET[jqUploader] : " . $_GET['jqUploader'];
$output .= "<br /><br />GET[test] : " .  $_GET['test'];
$output .= "<br /><br />Inside the loop and upload File: " . $uploadFile;

	
if ($_POST['submit'] != '') {
	echo "<br /> inside the whale";
    // 1. submitting the html form
    if (!isset($_GET['jqUploader'])) {
        // 1.a javascript off, we need to upload the file
        if (move_uploaded_file ($_FILES[0]['tmp_name'], $uploadFile)) {
            // delete the file
            // @unlink ($uploadFile);
            $html_body = '<h1>File successfully uploaded!</h1><pre>';
            $html_body .= print_r($_FILES, true);
            $html_body .= '</pre>';
        } else {
            $html_body = '<h1>File upload error!</h1>';

            switch ($_FILES[0]['error']) {
                case 1:
                    $html_body .= 'The file is bigger than this PHP installation allows';
                    break;
                case 2:
                    $html_body .= 'The file is bigger than this form allows';
                    break;
                case 3:
                    $html_body .= 'Only part of the file was uploaded';
                    break;
                case 4:
                    $html_body .= 'No file was uploaded';
                    break;
                default:
                    $html_body .= 'unknown errror';
            }
            $html_body .= 'File data received: <pre>';
            $html_body .= print_r($_FILES, true);
            $html_body .= '</pre>';
        }
        $html_body = '<h1>Full form</h1><pre>';
        $html_body .= print_r($_POST, true);
        $html_body .= '</pre>';
    } else {
        // 1.b javascript on, so the file has been uploaded and its filename is in the POST array
        $html_body = '<h1>Form posted!</h1><p>Error:<pre>';
        $html_body .= print_r($_POST, false);
        $html_body .= '</pre>';
    }
    myHtml($html_body);
} else {
    if ($_GET['jqUploader'] == 1) {
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // 2. performing jqUploader flash upload
        if ($_FILES['Filedata']['name']) {
            if (move_uploaded_file ($_FILES['Filedata']['tmp_name'], $uploadFile)) {
                // delete the file
                //  @unlink ($uploadFile);
				$output .= "<br/>Inside the loop and upload File: " . $uploadFile;
                return $uploadFile;
            }
        } else {
            if ($_FILES['Filedata']['error']) {
                return $_FILES['Filedata']['error'];
            }
        }
    }
}

$myfile = getcwd() . "/debug.html";
//echo "myfile = " . $myfile;
//$output .= "<pre>".print_r($_FILES,true)."</pre>";
$fh = fopen($myfile, 'w') or die("can't open file");
echo $output;
fwrite($fh, $output);
fclose($fh);

// /////////////////// HELPER FUNCTIONS
function myHtml($bodyHtml)
{

    ?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>jqUploader demo - Result</title>
<link rel="stylesheet" type="text/css" media="screen" href="style.css"/>
</head>
<body>
	<?php echo $bodyHtml;

    ?>
	</body>
</html>
<?php
}

?>
