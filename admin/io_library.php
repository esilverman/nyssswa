<?

function fileUploadDisplay ($fileName) {
	global $images_path, $audio_path, $movies_path, $server_root, $http_root;
	global $varName;
	global $relFilePath;
	global $AJAXtask;
	global $isUploading;
	global $recordId;
	$splitArray = explode(".",$fileName);
	$fileEXT = $splitArray[sizeof($splitArray) - 1];
	switch (strtoupper($fileEXT)) {
		//MOVIES
		case "MOV":
			$newFileType = "mov";
			//if ($newFileType != $currFileType) {include "global_vars.php";}
			if ($AJAXtask || $isUploading)
				$relFilePath = $movies_path;
			$fileDisplayString = "<div class='$newFileType'><embed src='$http_root$relFilePath$fileName' /><input class='formElement' type='hidden' name='$varName' value='$http_root$relFilePath$fileName' /></div>";
			break;
		case "FLV":
			$newFileType = "flv";
			//if ($newFileType != $currFileType) {include "global_vars.php";}
			if ($AJAXtask || $isUploading)
				$relFilePath = $movies_path;
			$fileDisplayString = "<div class='$newFileType'><embed src='mediaplayer.swf?video=$http_root$relFilePath$fileName' /><input class='formElement' type='hidden' name='$varName' value='$http_root$relFilePath$fileName' /></div>";
			break;
		//IMAGES
		case "JPG":
		case "PNG":
		case "GIF":
			$newFileType = "img";
			//if ($newFileType != $currFileType) {include "global_vars.php";}
			if ($AJAXtask || $isUploading)
				$relFilePath = $images_path;
			$fileDisplayString = "<div class='$newFileType'><div class='cropCoords' id='".$recordId.$varName."_croppable'><image src='$http_root$relFilePath$fileName' class=\"cropbox\" />
			<input type=\"hidden\" class=\"x\" name=\"x\" />
			<input type=\"hidden\" class=\"y\" name=\"y\" />
			<input type=\"hidden\" class=\"w\" name=\"w\" />
			<input type=\"hidden\" class=\"h\" name=\"h\" />
			</div>
<input class='formElement' type='hidden' name='$varName' value='$http_root$relFilePath$fileName' /></div>";
			break;
		case "PDF":
			break;
		//AUDIO
		case "MP3":
		case "MP4":
		case "AIFF":
			$newFileType = "audio";
			//if ($newFileType != $currFileType) {include "global_vars.php";}
			if ($AJAXtask || $isUploading)
				$relFilePath = $audio_path;
			$fileDisplayString = "<div class='$newFileType'><embed src='$http_root$relFilePath$fileName' /><input class='formElement' type='hidden' name='$varName' value='$http_root$relFilePath$fileName' /></div>";
			break;
		default:
			$fileDisplayString = "<div class='new'></div>";
			break;
	}
	return $fileDisplayString;
}

/**
 * Replaces double line-breaks with paragraph elements.
 *
 * A group of regex replaces used to identify text formatted with newlines and
 * replace double line-breaks with HTML paragraph tags. The remaining
 * line-breaks after conversion become <<br />> tags, unless $br is set to '0'
 * or 'false'.
 *
 * @since 0.71
 *
 * @param string $pee The text which has to be formatted.
 * @param int|bool $br Optional. If set, this will convert all remaining line-breaks after paragraphing. Default true.
 * @return string Text which has been converted into correct paragraph tags.
 */
function wpautop($pee, $br = 1) {

	if ( trim($pee) === '' )
		return '';
	$pee = $pee . "\n"; // just to make things a little easier, pad the end
	$pee = preg_replace('|<br />\s*<br />|', "\n\n", $pee);
	// Space things out a little
	$allblocks = '(?:table|thead|tfoot|caption|col|colgroup|tbody|tr|td|th|div|dl|dd|dt|ul|ol|li|pre|select|option|form|map|area|blockquote|address|math|style|input|p|h[1-6]|hr|fieldset|legend|section|article|aside|hgroup|header|footer|nav|figure|figcaption|details|menu|summary)';
	$pee = preg_replace('!(<' . $allblocks . '[^>]*>)!', "\n$1", $pee);
	$pee = preg_replace('!(</' . $allblocks . '>)!', "$1\n\n", $pee);
	$pee = str_replace(array("\r\n", "\r"), "\n", $pee); // cross-platform newlines
	if ( strpos($pee, '<object') !== false ) {
		$pee = preg_replace('|\s*<param([^>]*)>\s*|', "<param$1>", $pee); // no pee inside object/embed
		$pee = preg_replace('|\s*</embed>\s*|', '</embed>', $pee);
	}
	$pee = preg_replace("/\n\n+/", "\n\n", $pee); // take care of duplicates
	// make paragraphs, including one at the end
	$pees = preg_split('/\n\s*\n/', $pee, -1, PREG_SPLIT_NO_EMPTY);
	$pee = '';
	foreach ( $pees as $tinkle )
		$pee .= '<p>' . trim($tinkle, "\n") . "</p>\n";
	$pee = preg_replace('|<p>\s*</p>|', '', $pee); // under certain strange conditions it could create a P of entirely whitespace
	$pee = preg_replace('!<p>([^<]+)</(div|address|form)>!', "<p>$1</p></$2>", $pee);
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee); // don't pee all over a tag
	$pee = preg_replace("|<p>(<li.+?)</p>|", "$1", $pee); // problem with nested lists
	$pee = preg_replace('|<p><blockquote([^>]*)>|i', "<blockquote$1><p>", $pee);
	$pee = str_replace('</blockquote></p>', '</p></blockquote>', $pee);
	$pee = preg_replace('!<p>\s*(</?' . $allblocks . '[^>]*>)!', "$1", $pee);
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*</p>!', "$1", $pee);
	if ($br) {
		$pee = preg_replace_callback('/<(script|style).*?<\/\\1>/s', create_function('$matches', 'return str_replace("\n", "<WPPreserveNewline />", $matches[0]);'), $pee);
		$pee = preg_replace('|(?<!<br />)\s*\n|', "<br />\n", $pee); // optionally make line breaks
		$pee = str_replace('<WPPreserveNewline />', "\n", $pee);
	}
	$pee = preg_replace('!(</?' . $allblocks . '[^>]*>)\s*<br />!', "$1", $pee);
	$pee = preg_replace('!<br />(\s*</?(?:p|li|div|dl|dd|dt|th|pre|td|ul|ol)[^>]*>)!', '$1', $pee);
	if (strpos($pee, '<pre') !== false)
		$pee = preg_replace_callback('!(<pre[^>]*>)(.*?)</pre>!is', 'clean_pre', $pee );
	$pee = preg_replace( "|\n</p>$|", '</p>', $pee );

	// Modified to add auto-linking
	
	return auto_link($pee);
}

function auto_link($text) {
  $pattern = "/(((http[s]?:\/\/)|(www\.))?(([a-z][-a-z0-9]+\.)?[a-z][-a-z0-9]+\.[a-z]+(\.[a-z]{2,2})?)\/?[a-z0-9._\/~#&=;%+?-]+[a-z0-9\/#=?]{1,1})/is";
  $text = preg_replace($pattern, " <a href='$1'>$1</a>", $text);
  // fix URLs without protocols
  $text = preg_replace("/href=\"www/", "href=\"http://www", $text);
  return $text;
}
?>