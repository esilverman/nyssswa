<?php
	
require_once("database.php");
$id = $_GET['id'];
$foreignKey = $_GET['foreignKey'];
$bodyString = "";
if ($id) {
	$SQLreadQuery = "SELECT * FROM regions WHERE id = $id";
	$SQLreadResult = $db->query($SQLreadQuery);
	while ($rowArray = $db->fetch_array($SQLreadResult)) {
		$regionName = $rowArray["regionName"];
		$managerName = $rowArray["managerName"];
		$managerEmail = $rowArray["managerEmail"];
		
		$id = $rowArray["id"];
	}						
}

include "io_library.php";
if (session_id() != "") {
	//session_start(); 
	//echo "starting a new session now";
}
session_start();
if ($_GET['sessionDepth'] && $_SERVER['HTTP_REFERER']) { // if it needs a ref URL and they have SERVER data
	$sessionDepth =  'depth' . $_GET['sessionDepth'];
	if (!$_SESSION[$_GET['sessionDepth']] || $_SESSION[$sessionDepth] == "") {
		//session_register($sessionDepth);
		$_SESSION[$sessionDepth] = $_SERVER['HTTP_REFERER'];
	}
	$referringPage = $_SESSION[$sessionDepth]; // then give 'em what DEY need
}
//print_r($_SESSION);
	
if (!$referringPage)						// else send to parent 'list' page							
	$referringPage = "regions_list.php";
?>
<html>
	<head>
		<title>NYSSSWA.org > Regions</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='stylesheet' type='text/css' href='theme/ui.all.css'  />
	<link rel='stylesheet' type='text/css' href='main.css' />
	<link rel='stylesheet' type='text/css' href='jquery.Jcrop.css' />
	<link rel='stylesheet' type='text/css' media='screen'  href='colorpicker/css/colorpicker.css' />
	<link rel='stylesheet' type='text/css' href='ui.datepicker.css' />
	<link rel='stylesheet' type='text/css' href='jquery.autocomplete.css' />
	<link rel='stylesheet' href='jquery.timepickr.css' type='text/css'>
	<script type='text/javascript' src='jquery.js' ></script>
	<script type='text/javascript' src='jquery.flash.js'></script>
	<script type='text/javascript' src='jquery.jqUploader.js'></script>
	<script type='text/javascript' src='jquery.Jcrop.js'></script>
	<script type='text/javascript' src='write_actions.js'></script>
	<script type='text/javascript' src='nav_actions.js'></script>
	<script type='text/javascript' src='jquery.ajaxQueue.js'></script>
	<script type='text/javascript' src='jquery.autocomplete.js'></script>
	<script type='text/javascript' src='colorpicker/js/colorpicker.js'></script>
	<script type='text/javascript' src='ui.core.js'></script> 
	<script type='text/javascript' src='ui.datepicker.js'></script>
	<script type='text/javascript' src='jquery.ui.all.js'></script> 
	<script type='text/javascript' src='jquery.timepickr.js'></script> 
	<script type='text/javascript'>
		var refererURL = '<?php echo $referringPage; ?>';
		var parent_id = '<?php echo $id; ?>';
		var parent_table = 'regions';
	</script>
	</head>
	<body>
	<div class='navigation'>
<?php $currentNavTable = 'regions'; include 'navlist.php'; ?>
	<div class='siteName'>NYSSSWA.org</div><div id='currentTableTitle'><a href='regions_list.php'>Regions</a></div>
	</div>
	<div id="fieldsList">
		<form id='regionsForm' method="post" action="<?php echo $referringPage; ?>">
		<?php echo "<label for='regionName'>Region Name</label><input type='text' name='regionName' size='100' class='formElement regionsInput' value='$regionName'>";?>
		<?php echo "<label for='managerName'>Regional Board Member Name</label><input type='text' name='managerName' size='100' class='formElement regionsInput' value='$managerName'>";?>
		<?php echo "<label for='managerEmail'>Regional Board Member Email</label><input type='text' name='managerEmail' size='100' class='formElement regionsInput' value='$managerEmail'>";?>
		<input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
		<?php 
		if ($_GET['linkedChild']) {
			echo "<input type='hidden' name='linkedChild' value='".$_GET['linkedChild']."' />";
		}
		?>
		<input type="hidden" name="TABLENAME" value="regions" />
		<input type="submit" value="Save" id='saveRecord' />
		</form>
		<input type="submit" value="Cancel" id='cancelRecord' />
	</div>
	</body>
</html>