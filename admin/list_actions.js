function deleteBtnEvents () {
	$(".deleteBtn").unbind();
	
/*
	$(".deleteBtn").hover(
		function () {
			//$("#response").html("over");
			$(this).css({background: "#8B0909"});
			$(this).find(".deleteConfirmation").css({background: "#8B0909"});
		}, 
		function () {
			//$("#response").html("out");
			$(this).css({background: "#8B4646"});
			$(this).find(".deleteConfirmation").css({background: "#8B4646"});
		}
	)
*/
	
	$(".deleteBtn").click(
		function() {
			//$("#response").html("clicked");
			$(this).hide();
			$(this).parent().find(".editBtn").hide();
			$(this).css({background: "#8B0909"});
			$(this).unbind();
			$(this).after(deleteConfirmationHTML);
			confirmation = $(this).next();
			confirmation.css({opacity: 0.0, display: "inline", background: "#8B0909"});
			confirmation.animate({opacity: 1}, 300);
			deleteConfirmationEvents();
		}
	);
}

function deleteConfirmationEvents() {
	$(".deleteYes,.deleteNo").unbind("click");
	$(".deleteYes").click( function() {
		ID = $(this).parent().parent().parent().attr("id");
		buttonsID = $(this).parent().attr("id");
		$(this).parent().html('Deleting...');
		$.deleteAllFiles(ID);
		$.ajax({
			url: "ajax.php",
			data: "tablename="+jsTableName+"&id="+ID+"&AJAXtask=deleteRecord",
			cache: false,
			complete: function(){
				$("#"+ID).fadeOut();
			}
		});
	});
	$(".deleteNo").click(function () {
		$(this).parent().fadeOut("fast", function () {
			$(this).parent().parent().find(".deleteBtn").css({background: "#8B4646"});
		});//css({opacity: 0.0});
		$(this).parent().parent().find(".editBtn").show();
		$(this).parent().parent().find(".deleteBtn").show();
		$(this).parent().remove();
		deleteBtnEvents();
		}
	);	
}

var deleteConfirmationHTML = "<div class='deleteConfirmation'>Do you really want to delete? <a class='deleteYes'>YES</a> // <a class='deleteNo'>NO</a></div";

$(function() {
	deleteBtnEvents();
	//$("img:first").before("<div id='response'>response</div>");
	$("#listViewBtn").click( function() {
		$(".backListRecord").removeClass("gridView").addClass("listView");//.css({ "float":"none", "width":"300px" });
		$(this).css({"background":"#8F8E79 none repeat scroll 0 0"});
		$("#gridViewBtn").css({"background":"none"});
	});
	$("#gridViewBtn").click( function() {
		$(".backListRecord").removeClass("listView").addClass("gridView"); //.css({ "float":"left", "width":"200px" });
		$(this).css({"background":"#8F8E79 none repeat scroll 0 0"});
		$("#listViewBtn").css({"background":"none"});
	})
	.click(); //default to gridview
	
	$(".sortable").sortable({ 
		items: '.backListRecord',
		handle: '.dragIcon',
		update: function() {
			var orderString = '';
			var order = $('.sortable').sortable('toArray');
			for (i=0;i<order.length;i++) {
				if (i !== 0) { 
					orderString = orderString + '&'; 
				}
				orderString = orderString + 'order[]='+order[i];
			}
			//alert(order);
			//alert(orderString);
			//test[]=2&test[]=1&test[]=3 	
			$.ajax({
				type: "GET",
				url: "ajax.php",
				data: "AJAXtask=sortorderUpdate&tablename="+jsTableName+"&"+orderString,
				success: function(msg){
					//alert( "Data Saved: " + msg );
				}
			});	
		}
	});
	
	$(".dragIcon").hover(
		function() {$(this).css("background-position","0px -15px");},
		function() {$(this).css("background-position","0px 0px");}
	);
/*
	$(".currentTable").toggle(
		function () {
			$(".otherTable").fadeIn("fast");
			$(".currentPage").css("display","none");
		},
		function () {
			$(".otherTable").fadeOut("fast", function () {
				$(".currentPage").css("display","block");      
			});
		}
	);
*/
	
/*
	$(".newBtn,.editBtn").hover(
		function () {
			$(this).css({background: "#5F5E50"});
		}, 
		function () {
			$(this).css({background: "#8F8E79"});
		}
	);
*/

/*
$('input#id_search2').quicksearch('table#table_example2 tbody tr', {
	'delay': 300,
	'selector': 'th',
	'stripeRows': ['odd', 'even'],
	'loader': 'span.loading',
	'bind': 'keyup click',
	'show': function () {
		this.style.color = '';
	},
	'hide': function () {
		this.style.color = '#ccc';
	},
	'prepareQuery': function (val) {
		return new RegExp(val, "i");
	},
	'testQuery': function (query, txt, _row) {
		return query.test(txt);
	}
});
*/
	// #### START QUICKSEARCH #### //
	var opts = {
			'delay': 300,
			'selector': '.searchable_name',
			'stripeRows': ['odd', 'even'],
			'loader': 'span.loading',
			'bind': 'keyup click',
				onAfter: function () {
					return;
				}			
			};
	
	//var qs = $('input#search_input').quicksearch('#search_results tbody tr');
	if ($('input#search_input').length > 0) {
		var qs = $('input#search_input').quicksearch('.recordsList .backListRecord', opts);		
	}
	// #### END QUICKSEARCH #### //
	
	// #### START DATERANGE #### //
	if ($( "#from, #to" ).length > 0 ) {
		var dates = $( "#from, #to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
				
				var fromDate = $('#from').val(),
						toDate = $('#to').val();
				if ((fromDate.length > 0 && toDate.length > 0)) {
					$('#date_range .required').removeClass('error');
					$('#date_range .error_msg').hide();
				}
			}
		});
	}
	// #### END DATERANGE #### //
});


