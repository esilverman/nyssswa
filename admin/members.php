<?php

include("database.php");

$SQLlistQuery = "SELECT * FROM members ";
$SQLlistResult = $db->query($SQLlistQuery);
while ($rowArray = $db->fetch_array($SQLlistResult)) {
		$firstName = $rowArray["firstName"];
		$lastName = $rowArray["lastName"];
		$addressL1 = $rowArray["addressL1"];
		$city = $rowArray["city"];
		$state = $rowArray["state"];
		$zipcode = $rowArray["zipcode"];
		$phone = $rowArray["phone"];
		$email = $rowArray["email"];
		$contact = $rowArray["contact"];
		$region = $rowArray["region"];
		$yearsEnrolled = $rowArray["yearsEnrolled"];
		$lastEnrollment = $rowArray["lastEnrollment"];
		$membershipStatus = $rowArray["membershipStatus"];
		$fingerprint = $rowArray["fingerprint"];
		$paid = $rowArray["paid"];
		$id = $rowArray["id"];

		$bodyString .= "\n<div class='members_record'>";
		$bodyString .= "\n<div class='frontListElement'>First Name: $firstName</div>";
		$bodyString .= "\n<div class='frontListElement'>Last Name: $lastName</div>";
		$bodyString .= "\n<div class='frontListElement'>Street Address: $addressL1</div>";
		$bodyString .= "\n<div class='frontListElement'>City: $city</div>";
		$bodyString .= "\n<div class='frontListElement'>State: $state</div>";
		$bodyString .= "\n<div class='frontListElement'>Zipcode: $zipcode</div>";
		$bodyString .= "\n<div class='frontListElement'>Phone Number: $phone</div>";
		$bodyString .= "\n<div class='frontListElement'>Email: $email</div>";
		$bodyString .= "\n<div class='frontListElement'>contact: $contact</div>";

		$SQLrelatedData = "SELECT regionName AS fieldname, id FROM regions WHERE id = '$region' ";
		$SQLrelatedExecute = $db->query($SQLrelatedData);
		while( $relatedRowArray = $db->fetch_array($SQLrelatedExecute) ) {
			$fieldName = $relatedRowArray["fieldname"];
			$relatedId = $relatedRowArray["id"];
			$bodyString .= "\n<div class='frontListElement'>Region: $fieldName</div>";
		}

		$bodyString .= "\n<div class='frontListElement'>Years Enrolled: $yearsEnrolled</div>";
		$bodyString .= "\n<div class='frontListElement'>Last Enrollment Date: ".strftime('%m',strtotime($lastEnrollment))." ".strftime('%d',strtotime($lastEnrollment))." ".strftime('%Y',strtotime($lastEnrollment))."</div>";
		$bodyString .= "\n<div class='frontListElement'>Membership Status: $membershipStatus</div>";
	
		$bodyString .= "</div>";
		$bodyString .= "</div>";
							
}
?>
<html>
	<head>
		<title>members Front</title>
	<link rel="stylesheet" type="text/css" href="main.css" />
	</head>
	<body><?php echo $bodyString; ?>
	

	</body>
</html>