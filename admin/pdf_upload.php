<html>
	<head>
		<title>NYSSSWA.org > Upload Membership Form</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='stylesheet' type='text/css' href='theme/ui.all.css'  />
	<link rel='stylesheet' type='text/css' href='main.css' />
	<link rel='stylesheet' type='text/css' href='jquery.Jcrop.css' />
	<link rel='stylesheet' type='text/css' href='ui.datepicker.css' />
	<link rel='stylesheet' type='text/css' href='jquery.autocomplete.css' />
	<link rel='stylesheet' href='jquery.timepickr.css' type='text/css'>
	
	<script type="text/javascript" src="../uploadify/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="../uploadify/swfobject.js"></script>
	<script type="text/javascript" src="../uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<script type="text/javascript">
	// <![CDATA[
	$(document).ready(function() {
	  $('#file_upload').uploadify({
	    'uploader'  : '../uploadify/uploadify.swf',
	    'script'    : '../uploadify/uploadify.php',
	    'cancelImg' : '../uploadify/cancel.png',
	    'folder'    : '../uploads/',
	    'auto'      : true,
	    'buttonText': 'SELECT PDF'
	  });
	});
	// ]]>
	</script>

	
	<script type='text/javascript' src='nav_actions.js'></script>
	<script type='text/javascript'>
		var refererURL = '<?php echo $referringPage; ?>';
		var parent_id = '<?php echo $id; ?>';
		var parent_table = 'members';
	</script>
	</head>
	<body>
	<div class='navigation'>
<?php $currentNavTable = 'pdf_upload'; include 'navlist.php'; ?>
	<div class='siteName'>NYSSSWA.org</div><div id='currentTableTitle'><a href='pdf_upload.php'>Upload Membership Form</a></div>
	</div>
	<div class="recordsList">
		<h2>Upload Instructions</h2>
		<p><strong>NOTE: The following instructions assume that you have created a PDF with the exact name " membershipform.pdf ".</strong></p>
		<ol>
			<li>Click "Select PDF" below.</li>
			<li>Find your file in the window that opens</li>
			<li>Click "OPEN" or "OK"</li>
			<li>Wait until the download completes.</li>
			<li>You're done!</li>
		</ol>
	</div>	
	<div id="fieldsList" style="margin-top:0px;">
		<form id='membersForm' method="post" action="<?php echo $referringPage; ?>">
			<input id="file_upload" type="file" name="file_upload" />
			<input type="hidden" name="TABLENAME" value="pdf_upload" />
			<input type="hidden" name="refRegionId" value="<?php echo $refRegionId; ?>" />
			<input type="hidden" name="refPage" value="<?php echo $refPage; ?>" />
		</form>
	</div>
	</body>
</html>