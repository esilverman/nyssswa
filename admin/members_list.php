<?php

session_start();

require_once("database.php");

$remoteAddr = $_SERVER['REMOTE_ADDR'];

/*
print_r($_SERVER);
echo "http cookie : ".$_SERVER['HTTP_COOKIE'];
echo "remote addr : $remoteAddr";
*/

$SQLviewQuery = "SELECT * FROM default_views WHERE remote_addr = '$remoteAddr'";
$SQLviewResult = $db->query($SQLviewQuery);
$SQLviewArray = $db->fetch_array($SQLviewResult);

$entryExists = false;
if (mysql_affected_rows() > 0 ) {
	$userId = $SQLviewArray["id"];
	$entryExists = true;
}

/* $lastQuery = isset($_SESSION["lastQuery"]) ? $_SESSION["lastQuery"] : "view=all"; */
$lastQuery = isset($SQLviewArray["lastQuery"]) ? $SQLviewArray["lastQuery"] : "view=all";

// create array out of lastQuery
parse_str($lastQuery, $queryArray);

$URIary = parse_url($_SERVER["REQUEST_URI"]);
$thisQuery = $URIary["query"];
$hasQuery = count($thisQuery) > 0;

/* echo "<pre>thisQuery : $thisQuery <br/> lastQuery : $lastQuery</pre>"; */

// Set session to this query or last

$userLastQuery = $hasQuery ? $thisQuery : $lastQuery;
$SQLinsertQuery = "
INSERT INTO default_views
	(remote_addr, lastQuery)
VALUES
	('$remoteAddr', '$userLastQuery')";
$SQLupdateQuery = "UPDATE default_views SET lastQuery = '$userLastQuery' WHERE id = $userId";

$SQLquery = $entryExists ? $SQLupdateQuery : $SQLinsertQuery;
//echo $SQLquery;
$SQLinsertResult = $db->query($SQLquery);


if (!$hasQuery) { // If there is no query, redirect to lastQuery
	/* Redirect to the last view */
	$query = http_build_query($queryArray);
	header("Location: index.php?$query");
}

$currURL = explode("/",$_SERVER["SCRIPT_FILENAME"]);
$currPage = array_pop($currURL);

$refRegionId = $_POST['refRegionId'];
$refPage = $_POST['refPage'];

/* echo "<pre>".print_r($_POST, TRUE)."</pre>"; */

$_SESSION[$sessionDepth] == "";
$bodyString = $sortedString = "";
if( isset($_GET["sortBy"]) ) {
	$sortByClause = $_GET["sortBy"].", ";
	$sortedString = "Sorted By [".$_GET["sortField"]."]";
}
if ($_SERVER['REQUEST_METHOD']=="POST") {
	$id = $_POST['id'];

	if ($id) {
		$firstName = htmlentities($_POST["firstName"],ENT_QUOTES);
		$lastName = htmlentities($_POST["lastName"],ENT_QUOTES);
		$addressL1 = htmlentities($_POST["addressL1"],ENT_QUOTES);
		$city = htmlentities($_POST["city"],ENT_QUOTES);
		$state = htmlentities($_POST["state"],ENT_QUOTES);
		$zipcode = htmlentities($_POST["zipcode"],ENT_QUOTES);
		$phone = htmlentities($_POST["phone"],ENT_QUOTES);
		$email = htmlentities($_POST["email"],ENT_QUOTES);
		$contact = htmlentities($_POST["contact"],ENT_QUOTES);
		$region = htmlentities($_POST["region"],ENT_QUOTES);
		$yearsEnrolled = htmlentities($_POST["yearsEnrolled"],ENT_QUOTES);
		$lastEnrollment = htmlentities($_POST["lastEnrollment"],ENT_QUOTES);
		$membershipStatus = htmlentities($_POST["membershipStatus"],ENT_QUOTES);
		$fingerprint = htmlentities($_POST["fingerprint"],ENT_QUOTES);
		$paid = htmlentities($_POST["paid"],ENT_QUOTES);

		$SQLeditQuery = "UPDATE members SET ";	
		$SQLeditQuery .= "  firstName = '$firstName',  lastName = '$lastName',  addressL1 = '$addressL1',  city = '$city',  state = '$state',  zipcode = '$zipcode',  phone = '$phone',  email = '$email',  contact = '$contact',  region = '$region',  yearsEnrolled = '$yearsEnrolled',  lastEnrollment = '$lastEnrollment',  membershipStatus = '$membershipStatus',  fingerprint = '$fingerprint',  paid = '$paid'   "; 
		$SQLeditQuery .= " WHERE id = '$id' ";
		$SQLeditResult = $db->query($SQLeditQuery);
	}
	else {
		$firstName = htmlentities($_POST["firstName"],ENT_QUOTES);
		$lastName = htmlentities($_POST["lastName"],ENT_QUOTES);
		$addressL1 = htmlentities($_POST["addressL1"],ENT_QUOTES);
		$city = htmlentities($_POST["city"],ENT_QUOTES);
		$state = htmlentities($_POST["state"],ENT_QUOTES);
		$zipcode = htmlentities($_POST["zipcode"],ENT_QUOTES);
		$phone = htmlentities($_POST["phone"],ENT_QUOTES);
		$email = htmlentities($_POST["email"],ENT_QUOTES);
		$contact = htmlentities($_POST["contact"],ENT_QUOTES);
		$region = htmlentities($_POST["region"],ENT_QUOTES);
		$yearsEnrolled = htmlentities($_POST["yearsEnrolled"],ENT_QUOTES);
		$lastEnrollment = htmlentities($_POST["lastEnrollment"],ENT_QUOTES);
		$membershipStatus = htmlentities($_POST["membershipStatus"],ENT_QUOTES);
		$fingerprint = htmlentities($_POST["fingerprint"],ENT_QUOTES);
		$paid = htmlentities($_POST["paid"],ENT_QUOTES);

		$SQLnewQuery = "INSERT INTO members (firstName,lastName,addressL1,city,state,zipcode,phone,email,contact,region,yearsEnrolled,lastEnrollment,membershipStatus,fingerprint,paid) VALUES ('$firstName','$lastName','$addressL1','$city','$state','$zipcode','$phone','$email','$contact','$region','$yearsEnrolled','$lastEnrollment','$membershipStatus','$fingerprint','$paid')";
		$SQLnewResult = $db->query($SQLnewQuery);
	} //end if(id) else
} // end if (request method = POST)

/* Convert Region Table to Array */
$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
$regions[] = "0 index";
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionId = $SQLregionsArray["id"];
	$regionName = $SQLregionsArray["regionName"];
	$dropdownString .= "\t\t<option value='$regionId'>$regionName</option>\n";
	$regions[] = $regionName;
}

$WHERE = "";
$regionClause = $regionTitle = "";
$headerString = "";
if( isset($_GET["regionId"]) ) {
	$currRegion = $_GET["regionId"];
	$regionName = $regions[$currRegion];
	$regionClause = "region = $currRegion";	
	$headerString = "Viewing Members in <b>[$regionName]</b> Region $sortedString";
	$regionTitle = "from [$regionName]";
	$WHERE = "WHERE";
} else if ( isset($_GET["view"]) ){
	$headerString = "Viewing All Regions $sortedString";
}

$dateClause = "";
if( isset($_GET["fromDate"]) && isset($_GET["toDate"]) ) {
	$fromDate = date ( 'Y-m-d', strtotime ( $_GET["fromDate"]) );
	$toDate = date ( 'Y-m-d', strtotime ( $_GET["toDate"]) );
	$dateClause = "lastEnrollment BETWEEN '$fromDate' AND '$toDate'";
	if (strlen($regionClause) > 0){
		$dateClause = " AND ".$dateClause;
	}
	$WHERE = "WHERE";
}

// display results if and only if there is an explicit
// request for some or all of the results
if ( isset($_GET["view"]) || isset($_GET["regionId"]) ) { 	
	$SQLlistQuery = "SELECT * FROM members $WHERE $regionClause $dateClause ORDER BY $sortByClause membershipStatus DESC, lastName ASC";
	$SQLlistResult = $db->query($SQLlistQuery);
	$numResults = mysql_affected_rows();	
	
/* 	echo "<pre>$SQLlistQuery</pre>"; */
	
	$members = $numResults == 1 ? "Member" : "Members";
	$bodyString .= "<div id='membersHeader'>$headerString<span id='num_results'>Showing $numResults $members</span></div>";
	
	while ($rowArray = $db->fetch_array($SQLlistResult)) {
			$isCorrupt = false;
			$firstName = $rowArray["firstName"];
			$lastName = $rowArray["lastName"];
			$addressL1 = $rowArray["addressL1"];
			$city = $rowArray["city"];
			$state = $rowArray["state"];
			$zipcode = $rowArray["zipcode"];
			$phone = $rowArray["phone"];
			$email = $rowArray["email"];
			$contact = $rowArray["contact"];
			$region = $rowArray["region"];
			$yearsEnrolled = $rowArray["yearsEnrolled"];
			$newOrRenewed = ($yearsEnrolled > 1) ? "Renewed" : "New";
			$newOrRenewedClass = ($yearsEnrolled > 1) ? "renewedMembership" : "newMembership";
			$lastEnrollment = $rowArray["lastEnrollment"];
			$membershipStatus = $rowArray["membershipStatus"];
			$fingerprint = $rowArray["fingerprint"];
			$paid = $rowArray["paid"];
			$id = $rowArray["id"];

			$isCorrupt = ( strlen($zipcode) != 5 || substr_count($phone,"*") >= 1 );
			
			$validEmail = ( substr_count($email,"@") == 1 ||  strlen($email) > 0  );
			if ($validEmail){
				$emailString = "\n<div class='backListElement member_mailto'><a href='mailto:$email'>$email</a></div>"; 
			} else {
				$emailString = "\n<div class='backListElement no_email'><i>No Email</i></div>"; 			
				if ( strlen($email) > 0 ) {$emailString = "\n<div class='backListElement member_mailto'>Invalid : [$email]</div>";}
				$isCorrupt = true;
			}
			
			$corruptClass =  ($isCorrupt === true ) ? "corruptRecord" : "";

			if ( (!isset( $_GET["corrupt"] ))	||	(isset( $_GET["corrupt"] ) && $isCorrupt === true)	) 	{
				$statusClass = ($membershipStatus == 1) ? "currentMember" : "lapsedMembership";
				$bodyString .= "\n\n <div class='backListRecord members_record $statusClass $corruptClass $newOrRenewedClass' id='$id'>";	
				$bodyString .= "\n<div class='backListElement member_name searchable_name'>$lastName, $firstName</div>";
				//$bodyString .= "\n<div class='backListElement newOrRenewed'>$newOrRenewed</div>";
	
				
				$bodyString .= $emailString;
					
				$SQLrelatedData = "SELECT regionName AS fieldname, id FROM regions WHERE id = '$region' ";
				$SQLrelatedExecute = $db->query($SQLrelatedData);
				while($relatedRowArray = $db->fetch_array($SQLrelatedExecute)) {
					$fieldName = $relatedRowArray["fieldname"];
					$relatedId = $relatedRowArray["id"];
					$bodyString .= "\n<div class='backListElement'>[ $fieldName ]&nbsp;&nbsp;&nbsp;$phone</div>";
				}
				$bodyString .= "\n<div class='backListElement'>Last Enrollment: ".strftime('%m',strtotime($lastEnrollment))."-".strftime('%d',strtotime($lastEnrollment))."-".strftime('%Y',strtotime($lastEnrollment))."</div>";
				$bodyString .= "\n<div class='listPageButtons'><a class='editBtn positiveBtns' href='members_write.php?id=$id&regionId=$region'>EDIT</a><div class='deleteBtn'><a class='negativeBtns'>DELETE</a>";
				$bodyString .= "\n<div class='deleteConfirmation' id='deleteConfirmation$id'>Do you really want to delete? <a class='deleteYes'>YES</a> // <a class='deleteNo'>NO</a></div>";
				$bodyString .= "</div></div></div>";
			}
	}
	$bodyString .= "<a class='newBtn positiveBtns' href='members_write.php'>NEW RECORD</a>";
} //end if ( isset($_GET["view"]) || isset($_GET["regionId"]) ) { 
?>
<html>
	<head>
		<title>NYSSSWA.org > Members</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel="stylesheet" type="text/css" href="main.css" />
	<link rel="stylesheet" type="text/css" href="theme/ui.all.css" />
	</head>
	<body>
	
	<div class='navigation'>
		<?php $currentNavTable = 'members'; include 'navlist.php'; ?>
		<div class='siteName'>NYSSSWA.org</div>
		<div id='currentTableTitle'><a href='members_list.php'>Members <?php echo $regionTitle; ?></a></div>
		<div id='listBtns'>
			<div id='listViewBtn'><img src='images/list-icon.gif' alt='List View' /></div>
			<div id='gridViewBtn'><img src='images/grid-icon.gif' alt='Grid View' /></div>
		</div>
		
		<div id="search_box">
			<form>
				<label for="search" id="search_label">Search:</label>
				<input name="search"type="text" id="search_input" placeholder="Type to Search Names..." />
			</form>
		</div>
		
	</div>
	<div id='memberRecords' class='recordsList'>
		<div id="accountingRegionDropdown">
			<label>Select a Region to View: </label>
			<select class="required" name="region" id="membersRegionSelect">
				<option value="" selected="selected">- Choose a Location -</option>
					<?php echo $dropdownString; ?>
			</select>
		</div>
		<div id="orViewAll">
			or <button type="button" id="viewAllRegionsBtn">View All Regions</button>
		</div>		
		<?php require_once("sortByDropdown.php"); ?>
		<div id="viewCorrupt">
<?php
	if (!isset( $_GET["corrupt"] ) ) { 
			echo '<button type="button" id="viewCorruptBtn">View Corrupt Only</button>';
	} else {
			echo '<button type="button" id="viewAllMembersBtn">View All Members</button>';
	}
?>
		</div>
		<?php require_once("date_range.php"); ?>
		
		<div class="clearDiv" style="clear:both">&nbsp;</div>
	<?php echo $bodyString; ?>
	</div>
	
	<?php include_once("uriJS.php");?>
<!-- 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script> -->
	<script src="jquery.js" type='text/javascript'></script>
	<script src="jquery.ui.all.js" type='text/javascript'></script>
	<script src="jquery.ui.widget.js" type='text/javascript'></script>
	<script src="ui.core.js" type='text/javascript'></script>
	<script src="ui.datepicker.js" type='text/javascript'></script>
	<script src="jquery.quicksearch.js" type='text/javascript'></script>
	<script src="nav_actions.js" type='text/javascript'></script>
	<script src="list_actions.js" type='text/javascript'></script>
	<script type='text/javascript'>
	var jsTableName = 'members';
	
	var currLocation = window.location+"";

	
	<?php
	if ( is_numeric($refRegionId) && $refPage != "" ) {
		 echo "window.location = 'accounting.php?regionId=$refRegionId';";
	} else if ( (isset($_POST['refPage']) && $refPage == "") && $_GET['view']) {
		 echo "if(currLocation.indexOf('?regionId') == -1) {
							//window.location = '?regionId=$refRegionId';
						}";
	}
	echo "var refRegion = '$refRegionId';";
	?>

	$(function() {
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: "ajax.php",
			data: "tablename="+jsTableName+"&id="+id+"&field="+field+"&AJAXtask=deleteFile",
			cache: false,
			complete: function(){
				//alert("tablename="+jsTableName+"&id="+id+"&field="+field+"&AJAXtask=deleteFile");
			}
		});

	};	

	jQuery.deleteAllFiles = function(id,field) {
				
	};		
});
	</script>

	</body>
</html>