<?php

require_once("database.php");

/* Convert Region Table to Array */
$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
$regions[] = "0 index";
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionId = $SQLregionsArray["id"];
	$regionName = $SQLregionsArray["regionName"];
	$dropdownString .= "\t\t<option value='$regionId'>$regionName</option>\n";
	$regions[] = $regionName;
}
$sortByClause = "paid ASC, ";
$sortedString = "";
if( isset($_GET["sortBy"]) ) {
	$sortByClause = $_GET["sortBy"].", ";
	$sortedString = "Sorted By [".$_GET["sortField"]."]";
}

$regionClause = $regionTitle = "";
$headerString = "";
if( isset($_GET["regionId"]) ) {
	$currRegion = $_GET["regionId"];
	$regionName = $regions[$currRegion];
	$regionClause = "WHERE region = $currRegion";	
	$headerString = "<div id='membersHeader'>Viewing Members in <b>[$regionName]</b> Region $sortedString</div>";
	$regionTitle = "from [$regionName]";
} else if ( isset($_GET["view"]) ){
	$headerString = "<div id='membersHeader'>Viewing All Regions $sortedString</div>";
}
$bodyString .= $headerString;

// display results if and only if there is an explicit
// request for some or all of the results
if ( isset($_GET["view"]) || isset($_GET["regionId"]) ) { 
	$SQLlistQuery = "SELECT * FROM members $regionClause ORDER BY $sortByClause lastName ASC";
	$SQLlistResult = $db->query($SQLlistQuery);
	while ($rowArray = $db->fetch_array($SQLlistResult)) {
			$firstName = $rowArray["firstName"];
			$lastName = $rowArray["lastName"];
			$addressL1 = $rowArray["addressL1"];
			$city = $rowArray["city"];
			$state = $rowArray["state"];
			$zipcode = $rowArray["zipcode"];
			$phone = $rowArray["phone"];
			$email = $rowArray["email"];
			$contact = $rowArray["contact"];
			$region = $rowArray["region"];
			$yearsEnrolled = $rowArray["yearsEnrolled"];
			$lastEnrollment = $rowArray["lastEnrollment"];
			$membershipStatus = $rowArray["membershipStatus"];
			$fingerprint = $rowArray["fingerprint"];
			$paid = $rowArray["paid"];
			$id = $rowArray["id"];
	
			$isCorrupt = ( strlen($zipcode) != 5 || substr_count($phone,"*") >= 1 || substr_count($email,"@") != 1 || strlen($email) == 0 );
			
			$validEmail = ( substr_count($email,"@") == 1 ||  strlen($email) > 0  );
			if ($validEmail){
				$emailString = "\n<div class='backListElement member_mailto'><a href='mailto:$email'>$email</a></div>"; 
			} else {
				$emailString = "\n<div class='backListElement'><i>No Email</i></div>"; 			
				if ( strlen($email) > 0 ) {$emailString = "\n<div class='backListElement member_mailto'>Invalid : [$email]</div>";}
				$isCorrupt = true;
			}
			
			$corruptClass =  ($isCorrupt === true ) ? "corruptRecord" : "";

			if ( (!isset( $_GET["corrupt"] ))	||	(isset( $_GET["corrupt"] ) && $isCorrupt === true)	) 	{	
				$changeStatusBtn = "\n<button type='button' class='changeStatusBtn paid'>$$$<span class='statusTooltip'>Mark Unpaid</span></button>";	
				$paymentStatus = "";
				$reminderButton = "";
				if ($paid == 0) {
					$paymentStatus = paymentStatus($id,$lastEnrollment);
					$reminderButton = "\n<button type='button' class='paymentReminderBtn'>Remind</button>";
					$reminderButton .= "\n<div class='sendingMsg'>Sending <img src='images/ajax-loader.gif'/></div>";
					$reminderButton .= "\n<div class='sentMsg'>Reminder Sent!</div>";
					$changeStatusBtn = "\n<button type='button' class='changeStatusBtn unpaid'>$$$<span class='statusTooltip'>Mark Paid</span></button>";
				}
				$bodyString .= "\n<div class='backListRecord accounting_record $paymentStatus'>";
				$bodyString .= "\n<div class='editIconBtn'><a href='members_write.php?id=$id&ref=accounting&regionId=$region'><img src='images/pen-icon.png'></a></div>";
				$bodyString .= "\n<div class='backListElement accounting_name searchable_name'>$lastName, $firstName</div>";
				$bodyString .= "\n<input type='hidden' class='memberEmail' value='$email'>";
				$bodyString .= "\n<input type='hidden' class='memberId' value='$id'>";
	
				$validEmail = ( substr_count($email,"@") == 1);
				if (strlen($email) > 0){
					$emailString = "\n<div class='backListElement member_mailto'><a href='mailto:$email'>$email</a></div>"; 
					if ($validEmail === false) { $emailString = "\n<div class='backListElement member_mailto'>Invalid : [$email]</div>"; }
				} else {
					$emailString = "\n<div class='backListElement'><i>No Email</i></div>"; 			
				}
				$bodyString .= $emailString;
	
				$bodyString .= $reminderButton.$changeStatusBtn;
				$bodyString .= "</div>";
			}
	}
} //end !isset($_GET["view")
function paymentStatus ($memberId, $regDate) {
	$sevenDaysInSeconds = 60 * 60 * 24 * 7;
	$regDateInSecs = strtotime($regDate);

	$todaysDate = date('Y-n-j');
	$todaysDateInSecs = strtotime($todaysDate);
	
	if ( ($todaysDateInSecs - $regDateInSecs) > $sevenDaysInSeconds ) {
		//echo "OVERDUE >> regDate  : $regDate // todays date = $todaysDate {".($todaysDateInSecs - $regDateInSecs)."}<br />\n";
		return "overduePayment";
	} else {
		//echo "NOT OVERDUE BUT ANNOYING >> regDate  : $regDate // todays date = $todaysDate {".($todaysDateInSecs - $regDateInSecs)."}<br />\n";
		return "outstandingPayment";
	}
	
} // end function paymentStatus()

?>

<html>
	<head>
		<title>NYSSSWA.org > Accounting</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel="stylesheet" type="text/css" href="main.css" />
	</head>
	<body>
	
	<div class='navigation'>
		<?php $currentNavTable = 'accounting'; include 'navlist.php'; ?>
		<div class='siteName'>NYSSSWA.org</div>
		<div id='currentTableTitle'><a href='accounting.php'>Accounting</a></div>
		<div id='listBtns'>
			<div id='listViewBtn'><img src='images/list-icon.gif' alt='List View' /></div>
			<div id='gridViewBtn'><img src='images/grid-icon.gif' alt='Grid View' /></div>
		</div>
		
		<div id="search_box">
			<form>
				<label for="search" id="search_label">Search:</label>
				<input type="text" id="search_input" placeholder="Type to Search Names..." />
			</form>
		</div>
		
	</div>
	<div id="refreshBtn">Click to Refresh Results</div>
	<div id='accountingRecords' class='recordsList'>
		<div id="accountingRegionDropdown">
			<label>Select a Region to View: </label>
			<select class="required" name="region" id="accountRegionSelect">
				<option value="" selected="selected">- Choose a Location -</option>
					<?php echo $dropdownString; ?>
			</select>
		</div>
		<div id="orViewAll">
			or <button type="button" id="viewAllRegionsBtn">View All Regions</button>
		</div>
		<?php require_once("sortByDropdown.php"); ?>
		<div id="viewCorrupt">
<?php
	if (!isset( $_GET["corrupt"] ) ) { 
			echo '<button type="button" id="viewCorruptBtn">View Corrupt Only</button>';
	} else {
			echo '<button type="button" id="viewAllMembersBtn">View All Members</button>';
	}
?>
		</div>		
		<div class="clearDiv" style="clear:both">&nbsp;</div>
	<?php echo $bodyString; ?>
	</div>

	<?php include_once("uriJS.php");?>
	<script src="jquery.1.4.min.js" type='text/javascript'></script>
	<script src="jquery.ui.all.js" type='text/javascript'></script>
	<script src="jquery.quicksearch.js" type='text/javascript'></script>
	<script src="nav_actions.js" type='text/javascript'></script>
	<script src="list_actions.js" type='text/javascript'></script>
	<script src="../js/accounting.js" type='text/javascript'></script>

</body>
</html>

