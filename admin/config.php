<?php
include("global_vars.php");

// Database Constants
defined('DB_SERVER') ? null : define("DB_SERVER", $db_host);
defined('DB_USER') ? null : define("DB_USER", $db_username);
defined('DB_PASS') ? null : define("DB_PASS", $db_password);
defined('DB_NAME') ? null : define("DB_NAME", $db_name);


?>