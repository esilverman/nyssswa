/**
 * jqUploader (http://www.pixeline.be/experiments/jqUploader/)
 * A jQuery plugin to replace html-based file upload input fields with richer flash-based upload progress bar UI.
 *
 * Version 1.0.2.2
 * September 2007
 *
 * Copyright (c) 2007 Alexandre Plennevaux (http://www.pixeline.be)
 * Dual licensed under the MIT and GPL licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * using plugin "Flash" by Luke Lutman (http://jquery.lukelutman.com/plugins/flash)
 *
 * IMPORTANT:
 * The packed version of jQuery breaks ActiveX control
 * activation in Internet Explorer. Use JSMin to minifiy
 * jQuery (see: http://jquery.lukelutman.com/plugins/flash#activex).
 *
 **/
 jQuery.fn.jqUploader = function(options) {
    return this.each(function(index) {
        var $this = jQuery(this);
		// fetch label value if any, otherwise set a default one
		var $thisForm =  $(this).parents("form");
		var $thisInput = $(this);
		var $thisLabel = $("label",$this);
		//var recordId = $(this).
		var containerId = $(this).attr("id") || 'new_item_';
		var startMessage = ($thisLabel.text() =='') ? 'Please select a file' : $thisLabel.text();
		// get form action attribute value as upload script, appending to it a variable telling the script that this is an upload only functionality
		var actionURL = $thisForm.attr("action");
		// adds a var setting jqUploader to 1, so you can use it for serverside processing
		var prepender = (actionURL.lastIndexOf("?") != -1) ? "&": "?";
		actionURL = actionURL+prepender+'jqUploader=1';
		// check if max file size is set in html form
		//var maxFileSize = $("input[@name='MAX_FILE_SIZE']", $(this.form)).val();
		var opts = jQuery.extend({
				width:320,
				height:85,
				version: 8, // version 8+ of flash player required to run jqUploader
				background: 'FFFFFF', // background color of flash file
				src:    'jqUploader.swf',
				uploadScript:     actionURL,
				afterScript:      null, // if this is empty, jqUploader will replace the upload swf by a hidden input element
				varName:	        $thisInput.attr("name"),  //this holds the variable name of the file input field in your html form
				allowedExt:	      '*.jpg; *.jpeg; *.png; *.pdf', // allowed extensions
				allowedExtDescr:  'Images (*.jpg; *.jpeg; *.png)',
				params:           {menu:false},
				flashvars:        {},
				hideSubmit:       true,
				barColor:		      '0000CC',
				//maxFileSize:      maxFileSize,
				startMessage:     startMessage,
				errorSizeMessage: 'File is too big!',
				validFileMessage: 'now click Upload to proceed',
				progressMessage: 'Please wait, uploading ',
				endMessage:    'You\'re all done'
		}, options || {}
		);
		// disable form submit button
		if (opts.hideSubmit==true) {
			$("*[@type='submit']",this.form).hide();
		}
		// THIS WILL BE EXECUTED IN THE USECASE THAT THERE IS NO REDIRECTION TO BE DONE AFTER UPLOAD
		TerminateJQUploader = function(containerId,fileName,varName){
			var currFileType = $("#"+varName+" div").attr("class");
			//alert(varName + "_" + containerId);			
			//alert("currFileType: " + currFileType);
			//alert(varName);
			$.ajax({
				url: "ajax.php",
				dataType: "html",
				data: "fileName="+fileName+"&AJAXtask=upload&currFileType="+currFileType+"&varName="+varName+"&recordId="+containerId,
				cache: false,
				success: function(html){
					//alert("html :\n" + html);
					currentPlace = containerId + varName;
					//alert(currentPlace);
					$("#"+currentPlace+" div:first").replaceWith(html);
					$("#"+currentPlace+" .originalFile").replaceWith('');
					$("#"+currentPlace+" div:first").css({display: "none"});
					newImageSrc = $("#"+varName+" .cropbox").attr('src');
					//alert(newImageSrc);
					$("#"+currentPlace+"_thumb .preview").attr({src: newImageSrc}); //assign thumb to recently uploaded image
					$("#"+currentPlace+"_thumb").slideDown("slow");
					$("#"+currentPlace+" div:first").fadeIn(1200);
					
					//$("#"+currentPlace+" .cropbox").after("<div class='response'>RESPONSE</div>");

					$('.cropbox').each(function () {
						var myId = $(this).parent().attr('id'); // evaluates to "*_croppable"
						//alert("myId " + myId);
						//alert("currentPlace : " + currentPlace);
						var dimensionVarPrefix = currentPlace;
						//alert("dimensionVarPrefix : " + dimensionVarPrefix);
						var targetHeight = $("input[id='"+dimensionVarPrefix + "_height']").val();
						var targetWidth = $("input[id='"+dimensionVarPrefix + "_width']").val();
						//alert("targetHeight : " + targetHeight + "\ntargetWidth" + targetWidth);
						var aspectRatio = targetWidth / targetHeight;
						//alert("aspectRatio" + aspectRatio);
						$(this).Jcrop({
							aspectRatio: aspectRatio,
							onSelect: jcrop_target(myId),
							onChange: jcrop_target(myId),
							boxHeight: targetHeight
						});
						
						
						$("#" + currentPlace + " .preview").css({width: targetWidth,height: targetHeight});
						$("#" + currentPlace + " .preview").parent().css({width: targetWidth,height: targetHeight});

						$('.preview').each(function () {
							var thumbImg = $("#" + currentPlace).parent().find(".preview");
							//alert(thumbImg.attr('src'));
							newThumbImg = $("#" +currentPlace +' .cropbox').attr('src');
							//alert("newThumbImg : " + newThumbImg);
							thumbImg.attr({src: newThumbImg})
						});
					}); // end .cropBox.each()
				}
			});
/*
			window.onbeforeunload = function(){
				return "You have unsaved file uploads.";
			} 
*/
			var myForm = $this.parents("form");
			myForm.submit(function(){return true});
			$("*[@type='submit']",myForm).show();
		}
		var myParams = '';
		for (var p in opts.params){
				myParams += p+'='+opts.params[p]+',';
		}
		myParams = myParams.substring(0, myParams.length-1);
		// this function interfaces with the jquery flash plugin
		jQuery(this).flash(
		{
			src: opts.src,
			width: opts.width,
			height: opts.height,
			id:'movie_player-'+index,
			bgcolor:'#'+opts.background,
			flashvars: {
				containerId: containerId,
				uploadScript: opts.uploadScript,
				afterScript: opts.afterScript,
				allowedExt: opts.allowedExt,
				allowedExtDescr: opts.allowedExtDescr,
				varName :  opts.varName,
				barColor : opts.barColor,
				maxFileSize :opts.maxFileSize,
				startMessage : opts.startMessage,
				errorSizeMessage : opts.errorSizeMessage,
				validFileMessage : opts.validFileMessage,
				progressMessage : opts.progressMessage,
				endMessage: opts.endMessage
			},
			params: myParams
		},
		{
			version: opts.version,
			update: false
		},
			function(htmlOptions){
				var $el = $('<div id="'+containerId+'" class="flash-replaced"><div class="alt">'+this.innerHTML+'</div></div>');
					 $el.prepend($.fn.flash.transform(htmlOptions));
					 $('div.alt',$el).remove();
					 $(this).after($el).remove();
			}
		);
	});
};
