<?php

require_once("database.php");
require_once("io_library.php");
require_once("global_vars.php");

// Parse Get Vars
$memberId = $_GET['memberId'];
$reminderType = $_GET['type'];
$balance = $_GET['balance'];
$bodyString = "";
$todaysDate = date('Y-n-j');

// NYSSSWA Member Data Parsing
$SQLmemberQuery = "	SELECT * FROM members WHERE id = '$memberId' ";
$SQLmemberResults = $db->query($SQLmemberQuery);
while ($SQLmemberRow = $db->fetch_array($SQLmemberResults)) {
	$firstName = $SQLmemberRow["firstName"];
	$lastName = $SQLmemberRow["lastName"];
	$addressL1 = $SQLmemberRow["addressL1"];
	$city = $SQLmemberRow["city"];
	$state = $SQLmemberRow["state"];
	$zipcode = $SQLmemberRow["zipcode"];
	$phone = $SQLmemberRow["phone"];
	$email = $SQLmemberRow["email"];
	$contact = $SQLmemberRow["contact"];
	$region = $SQLmemberRow["region"];
	$yearsEnrolled = $SQLmemberRow["yearsEnrolled"];
	$lastEnrollment = $SQLmemberRow["lastEnrollment"];
	$membershipStatus = $SQLmemberRow["membershipStatus"];
	$fingerprint = $SQLmemberRow["fingerprint"];
	$paid = $SQLmemberRow["paid"];
	$id = $SQLmemberRow["id"];
}

switch ($reminderType) {
	// !Payment Reminder
	case "payment":
		require_once("../PHPMailer/class.phpmailer.php");
		$to = $email;
		//$to = "maugarten@nyssswa.org";
		//$to = "eli.s.silverman@gmail.com";
		$to_name = "$firstName $lastName";
		$subject = "Payment Reminder from NYSSSWA";
		$body = "
Dear Perspective Member,<br/><br/>

Your application for NYSSSWA Membership has been received but your payment through PayPal has not.  Your membership is not complete without payment.  If this is in error please email me and let me know. If not, you may follow this link to <a href='http://www.nyssswa.org/thankyou.php'>continue checking out with PayPal</a>. thank you for your interest.<br/><br/>

Sincerely,<br/><br/>

Hai Ping Yeh<br/>
Membership Chair
<br/><br/><br/><br/>
Visit our website at <a href='http://nyssswa.org/'>NYSSSWA.ORG</a>	
		";
		emailMember ($to, $to_name, $subject, $body);
		echo "<p>EMAIL SENT SUCCESSFULLY TO $to.</p>";
		echo $body;
		break;
	// !Membership Reminder
	case "membership":
		require_once("../PHPMailer/class.phpmailer.php");
		$to = $email;
		//$to = "maugarten@nyssswa.org";
		//$to = "eli.s.silverman@gmail.com";
		$to_name = "$firstName $lastName";
		$subject = "Membership Renewal Reminder from NYSSSWA";
		$body = "Dear School Social Work Colleagues:<br/><br/>

Each year at this time, The New York State School Social Workers Association invites current members to renew their membership.  <b>NYSSSWA</b> is the <b>only professional organization</b> in <b>New York State dedicated solely</b> to <b>advancing</b> the <b>interests of school social workers</b> and <b>our profession</b>.<br/><br/>
 
Your NYSSSWA membership provides:<br/>
<ul>
	<li><b>An outstanding website</b> filled with professional resources.  After December 1 most of the website will be available to <b>members only</b>.</li>
	<li><b>NYSSSWA is your school social work problem solver</b>.  Professionals are available to address your queries.</li>
	<li><b>Regional meetings</b> in most areas of the State provide great opportunities for professional growth through networking and stimulating speakers.</li>
	<li><b>Annual Conference</b> with <b>Member Discounts</b>.</li>
	<li><b>Regular Newsletters, which help, keep </b>you <b>current</b> at a glance.</li>
	<li>Your <b>Voice</b> with <b>The New York State Department of Education</b>.</li>
	<li>Your <b>Voice</b> with most <b>professional state education associations</b>.</li>
	<li>Working closely with our <b>National counterpart, School Social Workers Association of America</b>.</li>
	<li><b>Position Papers</b>.  Our Paper on School Certification delineates the role of the school social worker based on our Social Work License.</li>
	<li><b>Regional Blogs</b>, coming soon.</li>
	<li><b>Representation, enhancement, protection</b> and <b>growth advocacy</b> for the field of <b>School Social Work</b>.</li>
</ul>
<br/>
You can renew your membership directly on line (which we encourage) or print out a membership form and renew by mail. Your continued membership is always appreciated and is essential to keeping our organization active and vital.<br/><br/>

Sincerely, <br/>
Christine Plackis, LCSW, Ph.D.<br/>
President
<br/><br/><br/><br/>
Visit our website at <a href='http://nyssswa.org/'>NYSSSWA.ORG</a>	

";
		emailMember ($to, $to_name, $subject, $body);
		echo "<p>EMAIL SENT SUCCESSFULLY TO $to.</p>";
		echo $body;
		break;
	default:
		echo "<h2>ERROR: Reminder Type not recognized...</h2>";
		break;
} //end switch statement

function emailMember ($to, $to_name, $subject, $body) {		
		$from_name = "NYSSSWA";
		$from = "info@nyssswa.org";
		$replyTo_name = "NYSSSWA Info";

		// PHPMailer SMTP version 
		$mail = new PHPMailer();
		
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Host		= "smtp.accountsupport.com";
		$mail->Port		= 587;
		
		$mail->Username = "support@nyssswa.org";
		$mail->Password	= "nyssswa341";
		/* $mail->SMTPDebug  = 2;  */
		
		$mail->SetFrom($from, $from_name);
		$mail->AddReplyTo($from, $replyTo_name);
		$mail->AddAddress($to, $to_name);
		$mail->Subject	= $subject;
		$mail->MsgHTML($body);		
		
		$result = $mail->Send();
		return $result ? 'Sent' : $mail->ErrorInfo;
		
} //end function emailMember()

?>