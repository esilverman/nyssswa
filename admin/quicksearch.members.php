<?php

require_once('database.php');

$table_body = "";


/* Convert Region Table to Array */
$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
$regions[] = "0 index";
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionId = $SQLregionsArray["id"];
	$regionName = $SQLregionsArray["regionName"];
	$dropdownString .= "\t\t<option value='$regionId'>$regionName</option>\n";
	$regions[] = $regionName;
}

$regionClause = $regionTitle = "";
$headerString = "";
if( strlen($_GET["regionId"]) > 0 ) {
	$currRegion = $_GET["regionId"];
	$regionName = $regions[$currRegion];
	$regionClause = "WHERE region = $currRegion";	
}

if( isset($_GET["sortBy"]) ) {
	$sortByClause = $_GET["sortBy"].", ";
	$sortedString = "Sorted By [".$_GET["sortField"]."]";
}

unset($members);

$SQLlistQuery = "SELECT * FROM members $regionClause ORDER BY $sortByClause membershipStatus DESC, lastName ASC";
$SQLlistResult = $db->query($SQLlistQuery);
while ($rowArray = $db->fetch_array($SQLlistResult)) {
	$firstName = $rowArray["firstName"];
	$lastName = $rowArray["lastName"];
	$addressL1 = $rowArray["addressL1"];
	$city = $rowArray["city"];
	$state = $rowArray["state"];
	$zipcode = $rowArray["zipcode"];
	$phone = $rowArray["phone"];
	$email = $rowArray["email"];
	$contact = $rowArray["contact"];
	$region = $rowArray["region"];
	$yearsEnrolled = $rowArray["yearsEnrolled"];
	$newOrRenewed = ($yearsEnrolled > 1) ? "Renewed" : "New";
	$newOrRenewedClass = ($yearsEnrolled > 1) ? "renewedMembership" : "newMembership";
	$lastEnrollment = $rowArray["lastEnrollment"];
	$membershipStatus = $rowArray["membershipStatus"];
	$fingerprint = $rowArray["fingerprint"];
	$paid = $rowArray["paid"];
	$id = $rowArray["id"];

	$table_body .= "
		<tr>
			<td>$lastName</td>
			<td>$firstName</td>
			<td>".strftime('%m',strtotime($lastEnrollment))."-".strftime('%d',strtotime($lastEnrollment))."-".strftime('%Y',strtotime($lastEnrollment))."</td>
			<td><a href='http://localhost:8888/nyssswa/admin/members_write.php?id=$id'>Edit Entry</a></td>
		</tr>";
	
	$members[] = array(
		"lastName" => $lastName,
		"firstName" => $firstName,
		"lastEnrollment" => strftime('%m',strtotime($lastEnrollment))."-".strftime('%d',strtotime($lastEnrollment))."-".strftime('%Y',strtotime($lastEnrollment)),
		"link" => "http://localhost:8888/nyssswa/admin/members_write.php?id=$id",
	);

}
?>
{
	members :<?php echo json_encode($members); ?>
}
<?php //echo $SQLlistQuery; ?>
<?php //print_r($_GET); ?>
