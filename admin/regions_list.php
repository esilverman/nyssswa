<?php
require_once("database.php");
$_SESSION[$sessionDepth] == "";
$bodyString = "";
if ($_SERVER['REQUEST_METHOD']=="POST") {
	$id = $_POST['id'];

	if ($id) {
		$regionName = htmlentities($_POST["regionName"],ENT_QUOTES);
		$managerName = htmlentities($_POST["managerName"],ENT_QUOTES);
		$managerEmail = htmlentities($_POST["managerEmail"],ENT_QUOTES);

		$SQLeditQuery = "UPDATE regions SET ";	
		$SQLeditQuery .= "  regionName = '$regionName', managerName = '$managerName', managerEmail = '$managerEmail'   "; 
		$SQLeditQuery .= " WHERE id = '$id' ";
		$SQLeditResult = $db->query($SQLeditQuery);
	}
	else {
		$regionName = htmlentities($_POST["regionName"],ENT_QUOTES);
		$managerName = htmlentities($_POST["managerName"],ENT_QUOTES);
		$managerEmail = htmlentities($_POST["managerEmail"],ENT_QUOTES);

		$SQLnewQuery = "INSERT INTO regions (regionName, managerName, managerEmail) VALUES ('$regionName','$managerName','$managerEmail')";
		$SQLnewResult = $db->query($SQLnewQuery);
	} //end if(id) else
} // end if (request method = POST)

$SQLlistQuery = "SELECT * FROM regions ORDER BY regionName";
$SQLlistResult = $db->query($SQLlistQuery);
while ($rowArray = $db->fetch_array($SQLlistResult)) {
		$regionName = $rowArray["regionName"];
		$id = $rowArray["id"];

	$bodyString .= "\n\n <div class='backListRecord regions_record' id='$id'>";

		$bodyString .= "\n<div class='backListElement'>Region Name: $regionName</div>";
						
		$bodyString .= "\n<div class='listPageButtons'><a class='editBtn positiveBtns' href='regions_write.php?id=$id'>EDIT</a><div class='deleteBtn'><a class='negativeBtns'>DELETE</a>";
		$bodyString .= "\n<div class='deleteConfirmation' id='deleteConfirmation$id'>Do you really want to delete? <a class='deleteYes'>YES</a> // <a class='deleteNo'>NO</a></div>";
		$bodyString .= "</div></div></div>";
}
?>
<html>
	<head>
		<title>NYSSSWA.org > Regions</title>
	<link rel='icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' /> 
	<link rel="stylesheet" type="text/css" href="main.css" />
	</head>
	<body>
	<div class='navigation'>
<?php $currentNavTable = 'regions'; include 'navlist.php'; ?>
	<div class='siteName'>NYSSSWA.org</div><div id='currentTableTitle'><a href='regions_list.php'>Regions</a></div>
	<div id='listBtns'><div id='listViewBtn'><img src='images/list-icon.gif' alt='List View' /></div><div id='gridViewBtn'><img src='images/grid-icon.gif' alt='Grid View' /></div></div>
	</div><div class='recordsList '>
	<?php echo $bodyString; ?>
		<a class='newBtn positiveBtns' href='regions_write.php'>NEW RECORD</a>
	</div>
	
	<?php include_once("uriJS.php");?>
	<script src="jquery.js" type='text/javascript'></script>
	<script src="jquery.ui.all.js" type='text/javascript'></script>
	<script src="nav_actions.js" type='text/javascript'></script>
	<script src="list_actions.js" type='text/javascript'></script>
	<script type='text/javascript'>
	var jsTableName = 'regions';

	$(function() {
		jQuery.fileDelete = function(id,field) {
		$.ajax({
			url: "ajax.php",
			data: "tablename="+jsTableName+"&id="+id+"&field="+field+"&AJAXtask=deleteFile",
			cache: false,
			complete: function(){
				//alert("tablename="+jsTableName+"&id="+id+"&field="+field+"&AJAXtask=deleteFile");
			}
		});

	};	

	jQuery.deleteAllFiles = function(id,field) {
				
	};		
});
	</script>

	</body>
</html>