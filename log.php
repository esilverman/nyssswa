<?php

$logData = "<br/>\n----------------------------------------------------<br/>\n";
$ip = $_SERVER['REMOTE_ADDR']; //Get there ip address.
$agent = $_SERVER['HTTP_USER_AGENT']; //Get there user agent, Firefox etc, and some other info about it.
$ref = $_SERVER['HTTP_REFERER']; // Referer, how they got to your website, who linked them, where they clicked that
$date = date("m-d-y g:i:s A T"); //Get the date and time.
				
$logData .= "<b>IP Address:</b> " .$ip . "<br/>\n"; 	//print / write the ip address.
$logData .= "<b>Referer:</b> ". $ref . "<br/>\n"; 			//print / write the referer.
$logData .= "<b>UserAgent:</b> ". $agent. "<br/>\n"; 	//print / write thier useragent.
$logData .= "<b>Date & Time:</b> ". $date. "<br/>\n"; 	//print / write the date and time they viewed the log.

$logData .= "<b>SQLrenewQuery</b>: [$SQLrenewQuery]<br/>\n";
$logData .= "<b>SQLnewQuery</b>: [$SQLnewQuery]<br/>\n";

$logFile = "membershipLog.html"; //Where the log will be saved.
$fh = fopen($logFile, 'a') or die("can't open file");
fwrite($fh, $logData);
fclose($fh);

?>