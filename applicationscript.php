<?PHP
######################################################
#                                                    #
#                Forms To Go 4.1.0                   #
#             http://www.bebosoft.com/               #
#                                                    #
######################################################




define('kOptional', true);
define('kMandatory', false);

define('kStringRangeFrom', 1);
define('kStringRangeTo', 2);
define('kStringRangeBetween', 3);
        
define('kYes', 'yes');
define('kNo', 'no');

define('kNumberRangeFrom', 1);
define('kNumberRangeTo', 2);
define('kNumberRangeBetween', 3);

define('kTextDumpFieldSeparator', 'csv');




error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('track_errors', true);

function DoStripSlashes($fieldValue)  { 
 if ( get_magic_quotes_gpc() ) { 
  if (is_array($fieldValue) ) { 
   return array_map('DoStripSlashes', $fieldValue); 
  } else { 
   return stripslashes($fieldValue); 
  } 
 } else { 
  return $fieldValue; 
 } 
}

function FilterCChars($theString) {
 return preg_replace('/[\x00-\x1F]/', '', $theString);
}

function ProcessTABCVSField($theString, $textSeparator) {
 if ($textSeparator == 'tab') {
  $theString = preg_replace('/\t/', ' ', $theString);
 }
 $theString = preg_replace('/\"/', '""', $theString);

 return $theString;
}
function ProcessPHPFile($PHPFile) {
 ob_start();
 require $PHPFile;
 return ob_get_clean();
}

function CheckString($value, $low, $high, $mode, $limitAlpha, $limitNumbers, $limitEmptySpaces, $limitExtraChars, $optional) {
 if ($limitAlpha == kYes) {
  $regExp = 'A-Za-z';
 }
 
 if ($limitNumbers == kYes) {
  $regExp .= '0-9'; 
 }
 
 if ($limitEmptySpaces == kYes) {
  $regExp .= ' '; 
 }

 if (strlen($limitExtraChars) > 0) {
 
  $search = array('\\', '[', ']', '-', '$', '.', '*', '(', ')', '?', '+', '^', '{', '}', '|', '/');
  $replace = array('\\\\', '\[', '\]', '\-', '\$', '\.', '\*', '\(', '\)', '\?', '\+', '\^', '\{', '\}', '\|', '\/');

  $regExp .= str_replace($search, $replace, $limitExtraChars);

 }

 if ( (strlen($regExp) > 0) && (strlen($value) > 0) ){
  if (preg_match('/[^' . $regExp . ']/', $value)) {
   return false;
  }
 }

 if ( (strlen($value) == 0) && ($optional === kOptional) ) {
  return true;
 } elseif ( (strlen($value) >= $low) && ($mode == kStringRangeFrom) ) {
  return true;
 } elseif ( (strlen($value) <= $high) && ($mode == kStringRangeTo) ) {
  return true;
 } elseif ( (strlen($value) >= $low) && (strlen($value) <= $high) && ($mode == kStringRangeBetween) ) {
  return true;
 } else {
  return false;
 }

}


function CheckNumeric($value, $low, $high, $mode, $optional) {
 if ( (strlen($value) == 0) && ($optional === kOptional) ) {
  return true;
 } elseif (!is_numeric($value)) {
  return false;
 } elseif ( ($value >= $low) && ($mode == kNumberRangeFrom) ) {
  return true;
 } elseif ( ($value <= $high) && ($mode == kNumberRangeTo) ) {
  return true;
 } elseif ( ($value >= $low) && ($value <= $high) && ($mode == kNumberRangeBetween) ) {
  return true;
 } else {
  return false;
 }
}


function CheckEmail($email, $optional) {
 if ( (strlen($email) == 0) && ($optional === kOptional) ) {
  return true;
 } elseif ( eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email) ) {
  return true;
 } else {
  return false;
 }
}


function CheckEqualTo($original, $repeated) {
 if ($original == $repeated) {
  return true;
 } else {
  return false;
 }
}


function CheckValueList_membership_status($values, $valType, $optional) {

 $selCnt = 0;

 $valueList[] = 'New Membership';
 $valueList[] = 'Membership Renewal';
  

 if (!is_array($values)) {
  if (strlen($values) > 0) {
   $values = array($values);
  } else {
   $values = array();
  }
 }

 foreach ($values as $valuesKey => $valuesVal) {
  foreach ($valueList as $valueListKey => $valueListVal) {  
   if ($valueListVal == $valuesVal) {
    $selCnt++;
    break;
   }
  } 
  reset($valueList);
 }

 if ((count($values) == 0) && ($optional === kOptional)) {
  return true;
 } elseif (($valType == 1) && ($selCnt > 0)) {
  return true;
 } elseif (($valType == 2) && ($selCnt == count($valueList))) {
  return true;
 } elseif (($valType == 3) && ($selCnt == 0)) {
  return true;
 } else {
  return false;
 }
 
}
function CheckValueList_address_state($values, $valType, $optional) {

 $selCnt = 0;

 $valueList[] = 'NY';
 $valueList[] = 'AL';
 $valueList[] = 'AK';
 $valueList[] = 'AZ';
 $valueList[] = 'AR';
 $valueList[] = 'CA';
 $valueList[] = 'CO';
 $valueList[] = 'CT';
 $valueList[] = 'DE';
 $valueList[] = 'FL';
 $valueList[] = 'GA';
 $valueList[] = 'HI';
 $valueList[] = 'ID';
 $valueList[] = 'IL';
 $valueList[] = 'IN';
 $valueList[] = 'IA';
 $valueList[] = 'KS';
 $valueList[] = 'KY';
 $valueList[] = 'LA';
 $valueList[] = 'ME';
 $valueList[] = 'MD';
 $valueList[] = 'MI';
 $valueList[] = 'MN';
 $valueList[] = 'MS';
 $valueList[] = 'MO';
 $valueList[] = 'MT';
 $valueList[] = 'NE';
 $valueList[] = 'NV';
 $valueList[] = 'NH';
 $valueList[] = 'NJ';
 $valueList[] = 'NM';
 $valueList[] = 'NC';
 $valueList[] = 'ND';
 $valueList[] = 'OH';
 $valueList[] = 'OK';
 $valueList[] = 'OR';
 $valueList[] = 'PA';
 $valueList[] = 'RI';
 $valueList[] = 'SC';
 $valueList[] = 'SD';
 $valueList[] = 'TN';
 $valueList[] = 'TX';
 $valueList[] = 'UT';
 $valueList[] = 'VT';
 $valueList[] = 'VA';
 $valueList[] = 'WA';
 $valueList[] = 'WV';
 $valueList[] = 'WI';
 $valueList[] = 'WY';
  

 if (!is_array($values)) {
  if (strlen($values) > 0) {
   $values = array($values);
  } else {
   $values = array();
  }
 }

 foreach ($values as $valuesKey => $valuesVal) {
  foreach ($valueList as $valueListKey => $valueListVal) {  
   if ($valueListVal == $valuesVal) {
    $selCnt++;
    break;
   }
  } 
  reset($valueList);
 }

 if ((count($values) == 0) && ($optional === kOptional)) {
  return true;
 } elseif (($valType == 1) && ($selCnt > 0)) {
  return true;
 } elseif (($valType == 2) && ($selCnt == count($valueList))) {
  return true;
 } elseif (($valType == 3) && ($selCnt == 0)) {
  return true;
 } else {
  return false;
 }
 
}
function CheckValueList_contacted($values, $valType, $optional) {

 $selCnt = 0;

 $valueList[] = 'yes';
 $valueList[] = 'no';
  

 if (!is_array($values)) {
  if (strlen($values) > 0) {
   $values = array($values);
  } else {
   $values = array();
  }
 }

 foreach ($values as $valuesKey => $valuesVal) {
  foreach ($valueList as $valueListKey => $valueListVal) {  
   if ($valueListVal == $valuesVal) {
    $selCnt++;
    break;
   }
  } 
  reset($valueList);
 }

 if ((count($values) == 0) && ($optional === kOptional)) {
  return true;
 } elseif (($valType == 1) && ($selCnt > 0)) {
  return true;
 } elseif (($valType == 2) && ($selCnt == count($valueList))) {
  return true;
 } elseif (($valType == 3) && ($selCnt == 0)) {
  return true;
 } else {
  return false;
 }
 
}
function CheckValueList_networking_location($values, $valType, $optional) {

 $selCnt = 0;

 $valueList[] = 'New York City';
 $valueList[] = 'Nassau/Suffolk';
 $valueList[] = 'Westchester';
 $valueList[] = 'Mid-Hudson';
 $valueList[] = 'Capital Region (Albany Area)';
 $valueList[] = 'Mohawk Valley (Utica Area)';
 $valueList[] = 'North Central (Oswego Area)';
 $valueList[] = 'Syracuse';
 $valueList[] = 'Rochester';
 $valueList[] = 'Buffalo';
 $valueList[] = 'So. Tier- Binghamton';
 $valueList[] = 'Please Choose For Me';
  

 if (!is_array($values)) {
  if (strlen($values) > 0) {
   $values = array($values);
  } else {
   $values = array();
  }
 }

 foreach ($values as $valuesKey => $valuesVal) {
  foreach ($valueList as $valueListKey => $valueListVal) {  
   if ($valueListVal == $valuesVal) {
    $selCnt++;
    break;
   }
  } 
  reset($valueList);
 }

 if ((count($values) == 0) && ($optional === kOptional)) {
  return true;
 } elseif (($valType == 1) && ($selCnt > 0)) {
  return true;
 } elseif (($valType == 2) && ($selCnt == count($valueList))) {
  return true;
 } elseif (($valType == 3) && ($selCnt == 0)) {
  return true;
 } else {
  return false;
 }
 
}


if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
 $clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
 $clientIP = $_SERVER['REMOTE_ADDR'];
}

$FTGmembership_status = DoStripSlashes( $_POST['membership_status'] );
$FTGfirst_name = DoStripSlashes( $_POST['first_name'] );
$FTGlast_name = DoStripSlashes( $_POST['last_name'] );
$FTGaddress_street = DoStripSlashes( $_POST['address_street'] );
$FTGaddress_city = DoStripSlashes( $_POST['address_city'] );
$FTGaddress_state = DoStripSlashes( $_POST['address_state'] );
$FTGaddress_zip = DoStripSlashes( $_POST['address_zip'] );
$FTGaddress_county = DoStripSlashes( $_POST['address_county'] );
$FTGcounty_employed = DoStripSlashes( $_POST['county_employed'] );
$FTGboces_district = DoStripSlashes( $_POST['boces_district'] );
$FTGhome_email = DoStripSlashes( $_POST['home_email'] );
$FTGhome_email_confirm = DoStripSlashes( $_POST['home_email_confirm'] );
$FTGhomephone_areacode = DoStripSlashes( $_POST['homephone_areacode'] );
$FTGhomephone_3digit = DoStripSlashes( $_POST['homephone_3digit'] );
$FTGhomephone_4digit = DoStripSlashes( $_POST['homephone_4digit'] );
$FTGcontacted = DoStripSlashes( $_POST['contacted'] );
$FTGnetworking_location = DoStripSlashes( $_POST['networking_location'] );
$FTGbutton2 = DoStripSlashes( $_POST['button2'] );
$FTGbutton = DoStripSlashes( $_POST['button'] );



$validationFailed = false;

# Fields Validations


if (!CheckValueList_membership_status($FTGmembership_status, 1, kMandatory)) {
 $FTGErrorMessage['membership_status'] = '• Please let us know if this is a new membership or a renewal';
 $validationFailed = true;
}

if (!CheckString($FTGfirst_name, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['first_name'] = '• Please provide your first name';
 $validationFailed = true;
}

if (!CheckString($FTGlast_name, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['last_name'] = '• Please provide your last name';
 $validationFailed = true;
}

if (!CheckString($FTGaddress_street, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['address_street'] = '• Please provide your street address';
 $validationFailed = true;
}

if (!CheckString($FTGaddress_city, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['address_city'] = '• Please provide your city';
 $validationFailed = true;
}

if (!CheckValueList_address_state($FTGaddress_state, 1, kMandatory)) {
 $FTGErrorMessage['address_state'] = '• Please choose your state';
 $validationFailed = true;
}

if (!CheckNumeric($FTGaddress_zip, 500, 99499, kNumberRangeBetween, kMandatory)) {
 $FTGErrorMessage['address_zip'] = '• Pleas provide a valide zip code';
 $validationFailed = true;
}

if (!CheckString($FTGaddress_county, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['address_county'] = '• Please provide your county';
 $validationFailed = true;
}

if (!CheckString($FTGcounty_employed, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['county_employed'] = '• Please let us know what county you are employed in';
 $validationFailed = true;
}

if (!CheckString($FTGboces_district, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['boces_district'] = '• Please let us know what BOCES district you are associated with';
 $validationFailed = true;
}

if (!CheckEmail($FTGhome_email, kMandatory)) {
 $FTGErrorMessage['home_email'] = '• Please provide a valid home email address';
 $validationFailed = true;
}

if (!CheckEqualTo($FTGhome_email_confirm, $FTGhome_email)) {
 $FTGErrorMessage['home_email_confirm'] = '• The home email address you provided does not match';
 $validationFailed = true;
}

if (!CheckString($FTGhomephone_areacode, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['homephone_areacode'] = '• Please Provide your home phone area code';
 $validationFailed = true;
}

if (!CheckString($FTGhomephone_3digit, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['homephone_3digit'] = '• Please provide the first 3 digits of your home phone number';
 $validationFailed = true;
}

if (!CheckString($FTGhomephone_4digit, 1, 0, kStringRangeFrom, kNo, kNo, kNo, '', kMandatory)) {
 $FTGErrorMessage['homephone_4digit'] = 'please provide the last 4 digits of your home phone number';
 $validationFailed = true;
}

if (!CheckValueList_contacted($FTGcontacted, 1, kMandatory)) {
 $FTGErrorMessage['contacted'] = '• Please choose if you would like to be contacted by a regional representative';
 $validationFailed = true;
}

if (!CheckValueList_networking_location($FTGnetworking_location, 1, kMandatory)) {
 $FTGErrorMessage['networking_location'] = '• Please Choose a Networking Area';
 $validationFailed = true;
}



# Embed error page and dump it to the browser

if ($validationFailed === true) {

 $fileErrorPage = 'error.shtml';

 if (file_exists($fileErrorPage) === false) {
  echo '<html><head><title>Error</title></head><body>The error page: <b>' . $fileErrorPage. '</b> cannot be found on the server.</body></html>';
  exit;
 }

 $errorPage = ProcessPHPFile($fileErrorPage);

 $errorList = @implode("<br />\n", $FTGErrorMessage);
 $errorPage = str_replace('<!--VALIDATIONERROR-->', $errorList, $errorPage);

 $errorPage = str_replace('<!--FIELDVALUE:membership_status-->', $FTGmembership_status, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:first_name-->', $FTGfirst_name, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:last_name-->', $FTGlast_name, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:address_street-->', $FTGaddress_street, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:address_city-->', $FTGaddress_city, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:address_state-->', $FTGaddress_state, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:address_zip-->', $FTGaddress_zip, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:address_county-->', $FTGaddress_county, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:county_employed-->', $FTGcounty_employed, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:boces_district-->', $FTGboces_district, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:home_email-->', $FTGhome_email, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:home_email_confirm-->', $FTGhome_email_confirm, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:homephone_areacode-->', $FTGhomephone_areacode, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:homephone_3digit-->', $FTGhomephone_3digit, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:homephone_4digit-->', $FTGhomephone_4digit, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:contacted-->', $FTGcontacted, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:networking_location-->', $FTGnetworking_location, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:button2-->', $FTGbutton2, $errorPage);
 $errorPage = str_replace('<!--FIELDVALUE:button-->', $FTGbutton, $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:membership_status-->', $FTGErrorMessage['membership_status'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:first_name-->', $FTGErrorMessage['first_name'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:last_name-->', $FTGErrorMessage['last_name'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:address_street-->', $FTGErrorMessage['address_street'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:address_city-->', $FTGErrorMessage['address_city'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:address_state-->', $FTGErrorMessage['address_state'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:address_zip-->', $FTGErrorMessage['address_zip'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:address_county-->', $FTGErrorMessage['address_county'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:county_employed-->', $FTGErrorMessage['county_employed'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:boces_district-->', $FTGErrorMessage['boces_district'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:home_email-->', $FTGErrorMessage['home_email'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:home_email_confirm-->', $FTGErrorMessage['home_email_confirm'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:homephone_areacode-->', $FTGErrorMessage['homephone_areacode'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:homephone_3digit-->', $FTGErrorMessage['homephone_3digit'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:homephone_4digit-->', $FTGErrorMessage['homephone_4digit'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:contacted-->', $FTGErrorMessage['contacted'], $errorPage);
 $errorPage = str_replace('<!--ERRORMSG:networking_location-->', $FTGErrorMessage['networking_location'], $errorPage);
  

 echo $errorPage;

}

if ( $validationFailed === false ) {

 # Email to Form Owner
  
 $emailSubject = FilterCChars("NYSSSWA Member Application Submission");
  
 $emailBody = "Below is the membership application information submitted from the website. You should get a Paypal confirmation that confirms what type of membership they purchased.\n"
  . "\n"
  . "\n"
  . "Member Status: \n"
  . "$FTGmembership_status\n"
  . "\n"
  . "Members Name: \n"
  . "$FTGfirst_name $FTGlast_name\n"
  . "\n"
  . "Members Address:\n"
  . "$FTGaddress_street\n"
  . "$FTGaddress_city, $FTGaddress_state $FTGaddress_zip\n"
  . "\n"
  . "Members County : \n"
  . "$FTGaddress_county\n"
  . "\n"
  . "The County the Member is employed in: \n"
  . "$FTGcounty_employed\n"
  . "\n"
  . "BOCES District: \n"
  . "$FTGboces_district\n"
  . "\n"
  . "Preffered E-Mail: \n"
  . "$FTGhome_email\n"
  . "\n"
  . "Preffered Phone Number: \n"
  . "($FTGhomephone_areacode) $FTGhomephone_3digit - $FTGhomephone_4digit\n"
  . "\n"
  . "Would like to be Contacted by a Regional Representative: \n"
  . "$FTGcontacted\n"
  . "\n"
  . "Networking Location: \n"
  . "#networking_location#";
  $emailTo = 'Hai-Ping Yeh <haiping@frontiernet.net>';
   
  $emailFrom = FilterCChars("$FTGhome_email");
   
  $emailHeader = "From: $emailFrom\n"
   . "MIME-Version: 1.0\n"
   . "Content-type: text/plain; charset=\"ISO-8859-1\"\n"
   . "Content-transfer-encoding: 7bit\n";
   
  mail($emailTo, $emailSubject, $emailBody, $emailHeader);
  
  
   # Confirmation Email to User
  
 $confEmailTo = FilterCChars($FTGhome_email);
  
 $confEmailSubject = FilterCChars("NYSSSWA Membership Application");
  
 $confEmailBody = "Hello $FTGfirst_name $FTGlast_name,\n"
  . "\n"
  . "Thank you for your interest in becoming a New York State School Social Worker Association Member!\n"
  . "\n"
  . "Your application information is being processed and we will be in contact with you soon.\n"
  . "\n"
  . "If you did not yet make your membership payment with paypal you can do so by going to: http://www.nyssswa.org/checkout.shtml\n"
  . "\n"
  . "If you would like to pay by check. Please make checks payable to NYSSSWA and mail to: \n"
  . "Marty Augarten \n"
  . "145-07 Neponsit Ave. \n"
  . "Neponsit, NY 11694 \n"
  . "\n"
  . "If you need immediate assistance please email Marty Augarten at Maugarten@nyssswa.org with any questions or concerns about your membership.\n"
  . "\n"
  . "\n"
  . "\n"
  . "Below is a copy of the information you submitted on your application for your records.\n"
  . "____________________________________\n"
  . "\n"
  . "\n"
  . "Member Status: \n"
  . "$FTGmembership_status\n"
  . "\n"
  . "Members Name: \n"
  . "$FTGfirst_name $FTGlast_name\n"
  . "\n"
  . "Members Address:\n"
  . "$FTGaddress_street\n"
  . "$FTGaddress_city, $FTGaddress_state $FTGaddress_zip\n"
  . "\n"
  . "Members County : \n"
  . "$FTGaddress_county\n"
  . "\n"
  . "The County the Member is employed in: \n"
  . "$FTGcounty_employed\n"
  . "\n"
  . "BOCES District: \n"
  . "$FTGboces_district\n"
  . "\n"
  . "Preffered E-Mail: \n"
  . "$FTGhome_email\n"
  . "\n"
  . "Preffered Phone Number: \n"
  . "($FTGhomephone_areacode) $FTGhomephone_3digit - $FTGhomephone_4digit\n"
  . "\n"
  . "Would like to be Contacted by a Regional Representative: \n"
  . "$FTGcontacted\n"
  . "\n"
  . "Networking Location: \n"
  . "#networking_location#\n"
  . "";
  
 $confEmailHeader = "From: haiping@frontiernet.net\n"
  . "MIME-Version: 1.0\n"
  . "Content-type: text/plain; charset=\"ISO-8859-1\"\n"
  . "Content-transfer-encoding: 7bit\n";
  
 mail($confEmailTo, $confEmailSubject, $confEmailBody, $confEmailHeader);
  
  #====================================================
# Dump field values to a text file                  =
#====================================================

$dumpRecord = sprintf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"",
              ProcessTABCVSField($FTGfirst_name, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGlast_name, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGaddress_street, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGaddress_city, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGaddress_state, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGaddress_zip, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGaddress_county, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGcounty_employed, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGboces_district, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGhome_email, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGhome_email_confirm, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGhomephone_areacode, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGhomephone_3digit, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGhomephone_4digit, kTextDumpFieldSeparator), 
              ProcessTABCVSField($FTGcontacted, kTextDumpFieldSeparator), 
              date('Y-m-d'), 
              date('H:i:s'));

$dumpRecord = str_replace("\n", "\\n", $dumpRecord);
$dumpRecord = str_replace("\r", "\\r", $dumpRecord);
$dumpRecord = $dumpRecord . "\n";

$fileDump = 'member_applications.txt';
$fileHandle = @fopen($fileDump, 'a');

if ($fileHandle === false) {

 echo '<b>Text Dump Error</b>: Cannot write to the text file: <b>' . $fileDump . '</b><br />';

 if (ini_get('track_errors')) {
  echo '<b>PHP Error</b>: ' . $php_errormsg;
 }

 exit;

} else {

 fwrite($fileHandle, $dumpRecord);
 fclose($fileHandle);

}
# Embed success page and dump it to the browser

$fileSuccessPage = '/home/users/web/b683/as.nyssswao/public_html/checkout.shtml';

if (file_exists($fileSuccessPage) === false) {
 echo '<html><head><title>Error</title></head><body>The success page: <b> ' . $fileSuccessPage . '</b> cannot be found on the server.</body></html>';
 exit;
}

$successPage = ProcessPHPFile($fileSuccessPage);

$successPage = str_replace('<!--FIELDVALUE:membership_status-->', $FTGmembership_status, $successPage);
$successPage = str_replace('<!--FIELDVALUE:first_name-->', $FTGfirst_name, $successPage);
$successPage = str_replace('<!--FIELDVALUE:last_name-->', $FTGlast_name, $successPage);
$successPage = str_replace('<!--FIELDVALUE:address_street-->', $FTGaddress_street, $successPage);
$successPage = str_replace('<!--FIELDVALUE:address_city-->', $FTGaddress_city, $successPage);
$successPage = str_replace('<!--FIELDVALUE:address_state-->', $FTGaddress_state, $successPage);
$successPage = str_replace('<!--FIELDVALUE:address_zip-->', $FTGaddress_zip, $successPage);
$successPage = str_replace('<!--FIELDVALUE:address_county-->', $FTGaddress_county, $successPage);
$successPage = str_replace('<!--FIELDVALUE:county_employed-->', $FTGcounty_employed, $successPage);
$successPage = str_replace('<!--FIELDVALUE:boces_district-->', $FTGboces_district, $successPage);
$successPage = str_replace('<!--FIELDVALUE:home_email-->', $FTGhome_email, $successPage);
$successPage = str_replace('<!--FIELDVALUE:home_email_confirm-->', $FTGhome_email_confirm, $successPage);
$successPage = str_replace('<!--FIELDVALUE:homephone_areacode-->', $FTGhomephone_areacode, $successPage);
$successPage = str_replace('<!--FIELDVALUE:homephone_3digit-->', $FTGhomephone_3digit, $successPage);
$successPage = str_replace('<!--FIELDVALUE:homephone_4digit-->', $FTGhomephone_4digit, $successPage);
$successPage = str_replace('<!--FIELDVALUE:contacted-->', $FTGcontacted, $successPage);
$successPage = str_replace('<!--FIELDVALUE:networking_location-->', $FTGnetworking_location, $successPage);
$successPage = str_replace('<!--FIELDVALUE:button2-->', $FTGbutton2, $successPage);
$successPage = str_replace('<!--FIELDVALUE:button-->', $FTGbutton, $successPage);


echo $successPage;

}

?>