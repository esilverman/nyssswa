<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Join NYSSSWA :: The New York School Social Workers Association</title>
<link href="css/kids_first.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/register.js"></script>
</head>

<body>
<div id="wrapper">
	<div id="joinHeader"></div>
  <div id="layout_wrapper">
    <div id="left_spacer"><img src="images/l_gradient_overlay.jpg" width="22" height="261" /></div>
    <div id="right_spacer"><img src="images/r_gradient_overlay.jpg" width="21" height="263" /></div>
    
  <div id="content_wrapper">
    	<div id="main_content">

    	  <h1>Become a Member</h1>
    	    <p><span class="bold"><br />
   	        Full Membership:</span> LMSW/LCSW and School Social Work certification, employed by a school district, BOCES, special education settings.</p>
   	      <p><span class="bold">Associate Membership:</span> Non-Voting—but informed—BSW or Bachelors/Masters Degree in related profession, interested others.</p>
    	    <p><span class="bold">Student / Retirees:</span> Interning at a school setting and former school employees. <br />
                <br />
                <span class="bold">&quot;Information on SSWAA membership&quot;</span><br />
    	      As you may know the &quot;Golden Partnership&quot; pilot project allowing for joint membership with NYSSWAA and SSWAA was discontinued by the National organization. If you are interested in continuing membership with <a href="http://www.sswaa.org" target="_blank">SSWAA Click Here</a></p>
    	    <p>&nbsp;</p>
    	    <p class="bold_underline">There are two ways to join NYSSSWA. </p>
    	    <p>1. <a href="uploads/membershipform.pdf">Download the PDF version</a> of the Membership Form and mail it to us with your check.</p>
    	    <p>2. Use the online membership application form below and make your payment online with a credit card using PayPal.</p>
    	    <p>&nbsp;</p>
    	    <h2>Online Membership Application</h2>
   	      <p>&nbsp;</p>
   	      <div id="newOrRenew">
	   	      <p>Already a member and looking to renew your membership? Please <a href="renew.php">follow this link</a>.</p>
	   	      <p>If you're registering for the first time, click the button below.<br/></p>
						<p style="text-align:center;"><button id="newMemberBtn" type="button">New Member Registration</button></p>
					</div>
					<?php include("register.php"); ?>
   	      <h4>&nbsp;</h4>
      </div>
   	  <div id="navigation">
<div id="googlesearch"><!-- Google CSE Search Box Begins  -->
<form action="http://www.nyssswa.org/search.shtml" id="cse-search-box">
  <input type="hidden" name="cx" value="004633215117489972450:kxjchwu5hqa" />
    <input type="hidden" name="cof" value="FORID:11" />
  <input type="text" name="q" size="15" />
  <input type="submit" name="sa" value="Search" />
</form>
<!-- <script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&lang=en"></script> -->
<!-- Google CSE Search Box Ends --></div>

<?php include("main_nav.shtml"); ?>
<div id="wildcard">
<?php include("wildcard.shtml"); ?>
 </div>
      	</div>
      </div>
  </div>
    <div id="footer">
<?php include("footer.shtml"); ?>
    </div>
</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-206169-10");
pageTracker._trackPageview();
</script>
</body>
</html>
