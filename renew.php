<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Join NYSSSWA :: The New York School Social Workers Association</title>
<link href="css/kids_first.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/renew.js"></script>
</head>

<body>
<div id="wrapper">
	<div id="joinHeader"></div>
  <div id="layout_wrapper">
    <div id="left_spacer"><img src="images/l_gradient_overlay.jpg" width="22" height="261" /></div>
    <div id="right_spacer"><img src="images/r_gradient_overlay.jpg" width="21" height="263" /></div>
    
  <div id="content_wrapper">
    	<div id="main_content">
	    	<?php
				if ( isset($_GET['id']) ) {
					include_once('renewalForm.php');
				} else {
					include_once('sendRenewalLink.php');
				}
				?>
    </div> <!-- end main_content -->
 	  <div id="navigation">
<div id="googlesearch"><!-- Google CSE Search Box Begins  -->
<form action="http://www.nyssswa.org/search.shtml" id="cse-search-box">
  <input type="hidden" name="cx" value="004633215117489972450:kxjchwu5hqa" />
    <input type="hidden" name="cof" value="FORID:11" />
  <input type="text" name="q" size="15" />
  <input type="submit" name="sa" value="Search" />
</form>
<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&lang=en"></script>
<!-- Google CSE Search Box Ends --></div>

<?php include("main_nav.shtml"); ?>
<div id="wildcard">
<?php include("wildcard.shtml"); ?>
 </div>
      	</div>
      </div>
  </div>
    <div id="footer">
<?php include("footer.shtml"); ?>
    </div>
</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-206169-10");
pageTracker._trackPageview();
</script>

</body>
</html>
