<?php
	if ($_GET['page']) {
		$page = $_GET['page'];
	} else {
		$page = "home";
	}
	$pageName = "$page.php";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta name="generator" content="InstantBlueprint.com - Create a web project framework in seconds." />
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>Join NYSSSWA :: The New York School Social Workers Association</title>
 <meta name="description" content="" />
 <meta name="keywords" content="" />
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
  <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
  <link href="http://nyssswa.org/kids_first.css" rel="stylesheet" type="text/css" media="all" /> 


  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/hoverIntent.js"></script>
  <script type="text/javascript" src="js/browserDetect.js"></script>
  <script type="text/javascript" src="js/front.js"></script>
  <script type="text/javascript">
   <?php echo "var pageName = '$page';";?>
  </script>
</head>
<!--[if lt IE 7]>  <body id='<?php echo $page."Body";?>' class="ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <body id='<?php echo $page."Body";?>' class="ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <body id='<?php echo $page."Body";?>' class="ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <body id='<?php echo $page."Body";?>' class="ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <body id='<?php echo $page."Body";?>'> <![endif]-->
<!--[if !IE]><!--> <body id='<?php echo $page."Body";?>'>             <!--<![endif]-->

<!-- Start: Center Wrap -->
<div id="wrapper" class="<?php echo $page."Wrapper"; ?>">
	<div id="pageContainer">
		<div id="pageBody" > <!-- Start: #pageBody -->
			<div id="pageContent">
