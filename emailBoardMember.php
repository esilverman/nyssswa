<?php
//require_once("admin/database.php");
//require_once("PHPMailer/class.phpmailer.php");

$managerName = $regions[$region]["managerName"];
$managerEmail = $regions[$region]["managerEmail"];
$emailBody = "
$managerName,<br/><br/>

This email has been sent to inform you of a new or renewed membership in your region.<br/><br/> 
To view the membership for $memberRegion, <a href='http://nyssswa.org/admin/members_list.php?_=a3j0ss3&regionId=$region'>please follow this link</a>. 
<br/><br/>"
  . "Below is a copy of the information submitted on the member's application\n<br/>"
  . "____________________________________\n<br/>"
  . "\n<br/>"
  . "$firstName $lastName\n<br/>"
  . "$addressL1\n<br/>"
  . "$city, $state $zipcode\n<br/>"
  . "\n<br/>"
  . "Preffered E-Mail: \n<br/>"
  . "$email\n<br/>"
  . "\n<br/>"
  . "Preffered Phone Number: \n<br/>"
  . "$phone\n<br/>"
  . "\n<br/>"
  . "Would You Like to be Contacted by a Regional Representative?: \n<br/>"
  . "$contactPref\n<br/>"
  . "\n<br/>"
  . "Networking Region: \n<br/>"
  . "$memberRegion\n<br/>"
  . "\n<br/>"
  . "\n<br/>";


$to_name = "$managerName";
$to = ($managerEmail != "") ? "$managerEmail" : "haiping@frontiernet.net";
$subject = "NYSSSWA - New Memebership or Renewal in Your Region";

$from_name = "NYSSSWA Membership";
$from = "no-reply@nyssswa.org";
$replyTo_name = "NYSSSWA Membership";

/*
$emailHeader = "From: no-reply@nyssswa.org\n"
. "MIME-Version: 1.0\n"
. "Content-type: text/plain; charset=\"ISO-8859-1\"\n"
. "Content-transfer-encoding: 7bit\n";
  
mail($to, $subject, $emailBody, $emailHeader);
*/


// PHP SMTP version 
/*
$mail = new PHPMailer();

$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->SMTPSecure = "tls";
$mail->Host		= "smtp.accountsupport.com";
$mail->Port		= 587;

$mail->Username = "support@nyssswa.org";
$mail->Password	= "nyssswa341";
*/
/* $mail->SMTPDebug  = 2;  */

$mail->SetFrom($from, $from_name);
$mail->AddReplyTo($from, $replyTo_name);
$mail->AddAddress($to, $to_name);
$mail->Subject	= $subject;
$mail->MsgHTML($emailBody);


$result = $mail->Send();
//echo $result ? 'Sent' : $mail->ErrorInfo;


?>