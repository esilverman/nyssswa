			<div id="renewYourMembership">
    	  <h1>Renew Your Membership</h1>
    	  <h4>Find Your Membership Renewal Link</h4>
    	  <br/>
  	    <p>In order to renew your membership, you'll need to follow the unique renewal link found in the membership confirmation email sent when you first signed up. If you're unable to locate that link, just reenter the email address you used to sign up and we'll send you the link again.</p>
  	    <form name="renewalEmail" class="nyssswaForm" id="renewalEmail" method="post" action="">
					<div id="renewalError">
						<img src="images/warning-icon.gif" alt="Warning!" width="35" height="31" style="float:left; margin: -5px 10px 0px 0px; " />
						<span>Sorry but we can't find that email address in our records. Please try a different one.</span>
					</div>
					<div id="formBody">
						<label>
							Membership Email<br/>
							<input type="text" class="required" name="email" id="email" size="30" maxlength="50"/>
						</label>
						<div style="clear:both"></div>
						<label>
							Confirm Email<br/>
							<input type="text" class="required" name="emailConfirm" id="emailConfirm" size="30" maxlength="50"/>
						</label>
						<div style="clear:both"></div>
						<input type="submit" name="renewalSubmit" id="renewalSubmit" class="button" value="Send Me the Link, Please!" />
					</div> <!-- end formBody -->
	  	    <div class="ajaxLoader"><p>Please wait a moment...</p><img src="images/ajaxloader3.gif"></div>
  	    </form>
	    </div>
	    <div id="validationMsg">
	    	<h1>Success!</h1>
				<h4>An email has been sent.</h4>
	    	<p>You may check your email but please allow up to 15 minutes for processing. After you have received the message, follow the instructions to complete your membership renewal.</p>
	    </div>
