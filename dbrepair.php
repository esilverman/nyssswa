<?php
require_once("admin/database.php");

$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionName = $SQLregionsArray["regionName"];
	$id = $SQLregionsArray["id"];
	$regionName = strtoupper($regionName);
	$regionsArray["$regionName"] = $id;				  
}

//print_r($regionsArray);

$sameEmailArray = array();
$SQLmembersQuery = "SELECT * FROM members_dec2010";
$SQLmembersResult = $db->query($SQLmembersQuery);
while ( $SQLmembersArray = $db->fetch_array($SQLmembersResult) ) {
		$firstName = htmlentities($SQLmembersArray["firstName"],ENT_QUOTES);
		$lastName = htmlentities($SQLmembersArray["lastName"],ENT_QUOTES);
		$email = htmlentities($SQLmembersArray["email"],ENT_QUOTES);
		$addressL1 = htmlentities($SQLmembersArray["addressL1"],ENT_QUOTES);
		$city = htmlentities($SQLmembersArray["city"],ENT_QUOTES);
		$state = htmlentities($SQLmembersArray["state"],ENT_QUOTES);
		$zipcode = htmlentities($SQLmembersArray["zipcode"],ENT_QUOTES);
		$phone = htmlentities($SQLmembersArray["phone"],ENT_QUOTES);
		$contact = htmlentities($SQLmembersArray["contact"],ENT_QUOTES);
		$region = strtoupper($SQLmembersArray["region"]);
		$membershipStatus = strtoupper($SQLmembersArray["membershipStatus"]);
		$lastEnrollment = $SQLmembersArray["lastEnrollment"];
		$years = $SQLmembersArray["yearsEnrolled"];
/*
		$years = $SQLmembersArray["years"];
		$firstName = $SQLmembersArray["firstName"];
		$lastName = $SQLmembersArray["lastName"];
		$city = $SQLmembersArray["city"];
		$addressL1 = $SQLmembersArray["addressL1"];
		$email = $SQLmembersArray["email"];
		$phone = $SQLmembersArray["phone"];
		$lastEnrollment = $SQLmembersArray["lastEnrollment"];
		$region = strtoupper($SQLmembersArray["region"]);
*/
		$id = $SQLmembersArray["id"];		
		//state zipcode email region
		$SQLmembersUpdate = "".
"UPDATE members_dec2010_repaired
SET
state = \"$state\", 
zipcode = \"$zipcode\", 
email = \"$email\", 
region = \"$region\", 
addressL1 = \"$addressL1\", 
contact = \"$contact\"
WHERE id=$id";
		//$db->query($SQLmembersUpdate);
		
		// !Add wkEmail if no email is present

		$SQLwkEmailQuery = "SELECT wkEmail FROM members_wkEmail WHERE id = $id";
		$SQLwkEmailResult = $db->query($SQLwkEmailQuery);
		while ( $SQLwkEmailArray = $db->fetch_array($SQLwkEmailResult) ) {
			$wkEmail = htmlentities($SQLwkEmailArray["wkEmail"],ENT_QUOTES);
		}
		
		//echo "Email = [$email] <> wkEmail = [$wkEmail]\n<br/>";
		if ($email == $wkEmail ) {
			$sameEmailArray[] = "$id";
			//echo "<b>Same email where id = $id</b><br/>\n";
		}
		if ($email == "" || $email == NULL || isset($email) === false ) {
			//get the work email for current id
			$SQLupdateEmailQuery = "UPDATE members SET email = \"$wkEmail\" WHERE id = $id";
			if ( isset($email) )  {echo "$SQLupdateEmailQuery;<br/>\n";}
			//$SQLupdateEmailResult = $db->query($SQLupdateEmailQuery);
		}
		
		// !Repair States SQL statement below
/*
		UPDATE members_dec2010_repaired SET contact = 1 WHERE contact != "n";
		UPDATE members_dec2010_repaired SET contact = 0 WHERE contact = "n";
		UPDATE members_dec2010_repaired SET state = "NY" WHERE state = "New York" OR state = "New Yokr" OR state = "Orchard Park";
		UPDATE members_dec2010_repaired SET state = "NJ" WHERE state = "New Jersey";
		UPDATE members_dec2010_repaired SET state = "FL" WHERE state = "Florida";
*/




		
		// !Parse regions
		
		$regionId = $regionsArray["$region"];	  
		if ($regionId == "") { // if the region is not named as one of the pre-determined regions
			$badRegionsArray["$region"] = $id;				  
			switch ($region) {
				case "NYC/ROCKLAND":
				case "NEW YORK CITY/NASSAU SUFFOLK":
					$region = "NEW YORK CITY";
					$regionId = $regionsArray["$region"];	  
					$SQLregionsUpdate .= "UPDATE members_dec2010_repaired SET region = $regionId WHERE id=$id;\n";
					break;
				case "SUFFOLK":
					$region = "NASSAU/SUFFOLK";
					$regionId = $regionsArray["$region"];
					$SQLregionsUpdate .= "UPDATE members_dec2010_repaired SET region = $regionId WHERE id=$id;\n";
					break;
				case "SO. TIER-BINGHAMTON":
					$region = "BINGHAMTON";
					$regionId = $regionsArray["$region"];
					$SQLregionsUpdate .= "UPDATE members_dec2010_repaired SET region = $regionId WHERE id=$id;\n";
					break;
				default:
					$regionId = -1;
					$SQLregionsUpdate .= "UPDATE members_dec2010_repaired SET region = -1 WHERE id=$id;\n";
					$bodyString .= "REGION ERROR: Unrecognized region! Region = ' $region ' AND id=$id\n";
					break;
			} // end switch($region)
		} else { // if region is properly formatted already
			$SQLregionsUpdate .= "UPDATE members_dec2010_repaired SET region = $regionId WHERE id=$id;\n";
		}
				
		// !Set New Fingerprint
		// >>For old records that don't have them
		$fingerprint = md5(time().$email.$lastName.$id);
		$SQLfingerprintQuery = "UPDATE members_dec2010_repaired SET fingerprint = '$fingerprint' WHERE id=$id";
		//$db->query($SQLfingerprintQuery);

		// !Repair Uppercase entries (Full Name, Address)
/*
		$firstName = ucwords(strtolower($firstName));
		//$lastName = ucwords(strtolower($lastName));
		$lastName = format_name($lastName);
		$city = ucwords(strtolower($city));
		$addressL1 = ucwords(strtolower($addressL1));
		$SQLnamesQuery = "UPDATE members_dec2010_repaired SET firstName = \"$firstName\", lastName = \"$lastName\", city = \"$city\", addressL1 = \"$addressL1\" WHERE id=$id";
		$db->query($SQLnamesQuery);
*/

		// !Repair phone numbers
/*
		$newPhone = format_phone($phone, $id);
		$bodyString .= "$id : phone = $phone \t\t newPhone = $newPhone<br/>\n";
		$SQLphoneQuery = "UPDATE members_dec2010_repaired SET phone = '$newPhone' WHERE id=$id";
		$db->query($SQLphoneQuery);
*/

		//$bodyString .= "UPDATE members_dec2010_repaired SET phone = '$newPhone' WHERE id=$id;<br/>\n";

		// !Parse lastEnrollment dates
		$dateArray = explode("/",$lastEnrollment);
		$month = $dateArray[0];
		$day = $dateArray[1];
		$year = $dateArray[2];
		$sqlDate = "$year-$month-$day";
		//$bodyString .= "UPDATE members_dec2010_repaired SET lastEnrollment = $sqlDate WHERE id=$id;\n";	
		$SQLenrollmentQuery = "UPDATE members_dec2010_repaired SET lastEnrollment = '$sqlDate' WHERE id=$id";
		//$db->query($SQLenrollmentQuery);

		
		// !Parse Years
		$sanitizedYears = ereg_replace("[\, ][ \,]", ",", $years );
		$sanitizedYears = ereg_replace(" ", "", $sanitizedYears );
		$sanitizedYears = ereg_replace("[^A-Za-z0-9\-]", ",", $sanitizedYears );
		
		$totalYears = 0;
		$yearInfo .= "Year info for id=$id : ";
		$yearInfo .= "original string = $years\n";
		$yearInfo .= "sanitized string = $sanitizedYears\n";
		$yearsTokens = explode(",", $sanitizedYears);
		//print_r($yearsTokens);
		$yearInfo .= "yearElems: ";
		foreach ($yearsTokens as $yearElem) {
			$pos = strpos($yearElem, "-");
			if ( is_numeric($yearElem) || $pos !== false) {
				$yearInfo += "$yearElem|";
				if ($pos !== false) {
					$yearRange = explode("-", $yearElem);
					$earlierYear = $yearRange[0];
					$laterYear = $yearRange[1];
					if ($earlierYear > $laterYear) {
						$laterYear += 100;
					}
					$totalYears += ($laterYear - $earlierYear) + 1;								
				} else { 
					$totalYears++;
				}
			}
		
		}
		$yearInfo .= "\nTotal years = $totalYears";
		$yearInfo .= "\n\n";
		//$bodyString .= "UPDATE members_dec2010_repaired SET yearsEnrolled = $totalYears WHERE id=$id;\n";
		$SQLyearsQuery = "UPDATE members_dec2010_repaired SET yearsEnrolled = $totalYears WHERE id=$id";
		//$db->query($SQLyearsQuery);

		$regionsData["$region"] = $regionsData["$region"]+1;
		
		// !Insert membershipStatus
		$membershipStatus = ($membershipStatus[0] == "L") ? 1 : 0;
		$SQLstatusQuery = "UPDATE members_dec2010_repaired SET membershipStatus = '$membershipStatus' WHERE id=$id";
		//$db->query($SQLstatusQuery);
		
}
//print_r($regionsData);
//print_r($regionsArray);
//print_r($badRegionsArray);
//print_r($sameEmailArray);
/*
//Same email print outs
$emailQuery = "SELECT * FROM members_dec2010_repaired WHERE ";
foreach ($sameEmailArray as $id) {
	$emailQuery .= "id = $id OR ";
}

$emailQuery .= "id=0";
echo $emailQuery;
*/
//echo $yearInfo;
//$bodyString = $SQLregionsUpdate;
echo $bodyString;

function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);

	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
	elseif(strlen($phone) > 10) // case where there are two numbers
		return format_phone(substr($phone, 0, 10));
	elseif( strlen($phone) !== 7 && strlen($phone) !== 10 ) // case where one or 2 digits are missing
		return "**".$phone;
	else
		return $phone;
		
}

function format_name($name=NULL) {
  /* Formats a first or last name, and returns the formatted version */
  if (empty($name))
      return false;
  // Initially set the string to lower, to work on it
  $name = strtolower($name);
  // Run through and uppercase any multi-barrelled names
  $names_array = explode('-',$name);
  for ($i = 0; $i < count($names_array); $i++) {           
    // "McDonald", "O'Conner"..
    if (strncmp($names_array[$i],'mc',2) == 0 || ereg('^[oO]\'[a-zA-Z]',$names_array[$i])) {
      $names_array[$i][2] = strtoupper($names_array[$i][2]);
    }
    // Always set the first letter to uppercase, no matter what
    $names_array[$i] = ucfirst($names_array[$i]);
  }
  // Piece the names back together
  $name = implode('-',$names_array);
  // Return upper-casing on all missed (but required) elements of the $name var
  return ucwords($name);
}

?>