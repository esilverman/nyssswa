
<?php

	require_once('recaptcha-php/recaptchalib.php');

	// Get a key from https://www.google.com/recaptcha/admin/create
	$publickey = "6Le10sASAAAAADeLLXhJj6r8daIpN1cVmLMvqoo6";
	$privatekey = "6Le10sASAAAAAEOu2MMUlf1jB19LQuBsQqVPWhK4";
	
	# the response from reCAPTCHA
	$resp = null;
	# the error code from reCAPTCHA, if any
	$error = null;
	
	# was there a reCAPTCHA response?
	if ($_POST["recaptcha_response_field"]) {
	    $resp = recaptcha_check_answer ($privatekey,
	                                    $_SERVER["REMOTE_ADDR"],
	                                    $_POST["recaptcha_challenge_field"],
	                                    $_POST["recaptcha_response_field"]);
	
		 if (!$resp->is_valid) {
		    // What happens when the CAPTCHA was entered incorrectly
		    die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
		         "(reCAPTCHA said: " . $resp->error . ")");
		  } else {
		    // Your code here to handle a successful verification
		  }
	}
	echo recaptcha_get_html($publickey, $error);

?>

<!--
  <h1>Final Step</h1>
	<form name="recaptchaForm" method="post" action="">
		<div id="recaptchaTxt" class="formLabel">Please type the words that appear in the box below to verify your registration. If you are having trouble reading the words, click the refresh button (<img src="http://www.google.com/recaptcha/api/img/clean/refresh.png">) to generate a new pair. </div>
		<div id="recaptcha">
		  <script type="text/javascript"
		     src="http://www.google.com/recaptcha/api/challenge?k=6Le10sASAAAAADeLLXhJj6r8daIpN1cVmLMvqoo6">
		  </script>
		  <script type="text/javascript">
		  </script>
-->
<!--
		  <noscript>
		     <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Le10sASAAAAADeLLXhJj6r8daIpN1cVmLMvqoo6"
		         height="300" width="500" frameborder="0"></iframe><br>
		     <textarea name="recaptcha_challenge_field" rows="3" cols="40">
		     </textarea>
		     <input type="hidden" name="recaptcha_response_field"
		         value="manual_challenge">
		  </noscript>	
-->
<!--
	  </div>
	</form>
-->
