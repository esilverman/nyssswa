<?php

require_once("admin/database.php");
require_once("PHPMailer/class.phpmailer.php");

 $emailBody = "
$firstName $lastName,<br/><br/>

On behalf of the NYSSSWA board and membership, please allow us to express our appreciation for joining and/or renewing your membership in our organization. As you know your membership is important to us. Our board meets at least four times a year, working diligently to sustain and support the essential work of school social workers.  NYSSSWA will continue to advocate for high quality services, promote professional development and disseminate information about school social work throughout the state.<br/><br/>  
Your membership card and additional information will be mailed to you shortly.
<br/><br/>
Sincerely,<br/>
Peg Trinkaus, LCSW, Ph.D. President<br/>
President<br/>
NYSSSWA.ORG
<br/><br/>
Members only section<br/>
Invitation code: nyssswamember<br/><br/>"
  . "Below is a copy of the information you submitted on your application for your records.\n<br/>"
  . "____________________________________\n<br/>"
  . "\n<br/>"
  . "$firstName $lastName\n<br/>"
  . "$addressL1\n<br/>"
  . "$city, $state $zipcode\n<br/>"
  . "\n<br/>"
  . "Preffered E-Mail: \n<br/>"
  . "$email\n<br/>"
  . "\n<br/>"
  . "Preffered Phone Number: \n<br/>"
  . "$phone\n<br/>"
  . "\n<br/>"
  . "Would You Like to be Contacted by a Regional Representative?: \n<br/>"
  . "$contactPref\n<br/>"
  . "\n<br/>"
  . "Networking Region: \n<br/>"
  . "$memberRegion\n<br/>"
  . "\n<br/>"
  . "\n<br/>"
  . "When you are ready to renew your membership, please visit us by <a href='http://www.nyssswa.org/renew.php?id=$fingerprint'>following this link</a>\n<br/>"
  . "\n<br/>"
  . "Thanks for your continued support!\n<br/>"
  . "-NYS School School Social Workers Association\n<br/>
  <br/><br/><br/><br/>
Visit our website at <a href='http://nyssswa.org/'>NYSSSWA.ORG</a>	
";


$to_name = "$firstName $lastName";
$to = "$email";
$subject = "NYSSSWA Thank You For Your Membership";

$from_name = "NYSSSWA";
$from = "no-reply@nyssswa.org";
$replyTo_name = "NYSSSWA Info";

/*
$emailHeader = "From: no-reply@nyssswa.org\n"
. "MIME-Version: 1.0\n"
. "Content-type: text/plain; charset=\"ISO-8859-1\"\n"
. "Content-transfer-encoding: 7bit\n";
  
mail($to, $subject, $emailBody, $emailHeader);
*/


// PHP SMTP version 
$mail = new PHPMailer();

$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->SMTPSecure = "tls";
$mail->Host		= "smtp.accountsupport.com";
$mail->Port		= 587;

$mail->Username = "support@nyssswa.org";
$mail->Password	= "nyssswa341";
/* $mail->SMTPDebug  = 2;  */

$mail->SetFrom($from, $from_name);
$mail->AddReplyTo($from, $replyTo_name);
$mail->AddAddress($to, $to_name);
$mail->Subject	= $subject;
$mail->MsgHTML($emailBody);


$result = $mail->Send();
//echo $result ? 'Sent' : $mail->ErrorInfo;


?>