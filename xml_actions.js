/*
7-24-09 Added entrymethod option "hidden" to dropdown (ESS)
7-23-09 optionsArray was not resetting, therefore not allowing multiple dropDown menus on the spec sheet > FIXED (ESS)
*/


	//#############################################################################
	//################## 		 BEGIN SET BEHAVIORS 		  #####################
	//#############################################################################
	
	var fieldClone, tableClone;
	
	/* #####################################
	* function setTableBehaviors()
	*		- cloneTable button
	*		- deleteTable button
	*		- tableCollapse button
	*
	########################################*/	
	jQuery.setTableBehaviors = function () {
		//alert("setTableBehaviors()");
				
		// ### UNBIND ALL BEHAVIORS FROM BUTTONS ###
		$(".cloneTable").unbind();
		$(".deleteTable").unbind();
		$(".tableCollapse").unbind();
		
		// ### REDEFINE ALL BEHAVIORS FOR BUTTONS ###
		$(".cloneTable").click(function () {
			tableClone = $(this).parent().clone();
			$(this).parent().after(tableClone);
			$.setFieldBehaviors();	
			$.setTableBehaviors();
		}); //end .cloneTable function
		
		$(".deleteTable").click(function () {
			$(this).parent().remove();
		}); //end .deleteTable function
		
		$(".tableCollapse").toggle(
			function () {
				//alert($(this).attr("class"));
				$(this).html("&nbsp;+");
				$(this).parent().find(".deleteTable").hide();
				$(this).parent().find(".newField").hide();
				$(this).parent().find(".cloneTable").hide();
				$(this).parent().find(".collapseAllFields").hide();
				$(this).parent().find(".field").each(function() {
					$(this).slideUp("fast");
				});
			},
			function () {
				$(this).html("&#151;");
				$(this).parent().find(".deleteTable").show();
				$(this).parent().find(".newField").show();
				$(this).parent().find(".cloneTable").show();
				$(this).parent().find(".collapseAllFields").show();
				$(this).parent().find(".field").each(function() {
					$(this).slideDown("fast");
				});
			}
		); //end .tableCollapse function
		$.setFieldBehaviors();
	}; //end setTableBehaviors()
	
	/* #####################################
	* function setTableBehaviors()
	*		- newField button
	*		- cloneField button
	*		- deleteField button
	*		- fieldCollapse button
	*		- addOption button
	*		- deleteOption button
	*		- .entryMethod dropDown handler
	*		- .collapseAllFields button
	########################################*/	
	jQuery.setFieldBehaviors = function () {
		//alert("setFieldBehaviors()");
		
		var optContainerStr = "<div class='optionsContainer' id='options'>"+
			"<div class='addOption addButton' id'='addOption'>+ Option</div><input class='inputField' type=\"hidden\" name=\"optionsEnd\"></div>";
				
		var optionHTML = "<div class='dropDownOption' id='option 1_1_1'>Option : <input class='inputField' type='text' name='option'><div class='deleteOption'>Delete</div></div>";
				
		var relatedChildHTML = "<div class='relatedChild'>Child Table Name : <input class='inputField' type='text' name='child_tablename'><br/>Child Field Name : <input class='inputField' type='text' name='child_fieldname'></div>";

		var thumbFieldHTML = "<div class='thumbChild'>"+
			"Width : <input class='inputField' type='text' name='width'>Height : <input class='inputField' type='text' name='height'>" + 
			"<div class='thumbFrontlist'>Thumbnail Front List : <select class='inputField' name='thumb_frontlist'>" + 
				"<option value='text'>Text</option>" + 
				"<option value='image'>Image</option>" + 
				"<option value='date'>Date</option>" + 
				"<option value='time'>Time</option>" + 
				"<option value='dateTime'>Date Time</option>" + 
				"<option value='movie'>Movie</option>" + 
				"<option value='relatedData'>Related Data</option>" +
				"<option value='none'>None</option>" + 
			"</select></div>" + 
			"<div class='thumbBacklist'>Thumbnail Back List : <select class='inputField' name='thumb_backlist'>" + 
				"<option value='text'>Text</option>" + 
				"<option value='image'>Image</option>" + 
				"<option value='date'>Date</option>" + 
				"<option value='time'>Time</option>" +
				"<option value='dateTime'>Date Time</option>" + 
				"<option value='movie'>Movie</option>" +
				"<option value='relatedData'>Related Data</option>" +
				"<option value='none'>None</option>" + 
			"</select></div>" +
			"</div>";
		
		var linkedTableHTML = "<div class='linkedChildTable'>"+
			"Child Table Name : <input class='inputField link_child_table' type='text' name='child_tablename'>" +
			"<br/>Child Field Name : <input class='inputField link_child_field' type='text' name='child_fieldname'>" + 
			"<input type='hidden' class='inputField' name='link_tablename' value='startVal'>"+
			"</div>";
	

		// ### UNBIND ALL BEHAVIORS FROM BUTTONS ###
		$(".newField").unbind();
		$(".cloneField").unbind();
		$(".deleteField").unbind();
		$(".fieldCollapse").unbind();
		$(".addOption").unbind();
		$(".deleteOption").unbind();
		$(".entrymethod select").unbind();
		$(".collapseAllFields").unbind();		
		
		// ### REDEFINE ALL BEHAVIORS FOR BUTTONS ###
		
		$(".newField").click(function() {
			newField = $(this).prev().clone();
			
			var prevClass = $(this).prev().attr('class');
			//alert("prevClass = " + prevClass);
			var prevClassArray = new Array();
			prevClassArray = prevClass.split(' ');
			var prevRowType = prevClassArray[1];
			var prevClass = prevClassArray[0];
			var newRowType;
			
			if(prevRowType == 'rowA')
				newRowType = 'field rowB';
			else
				newRowType = 'field rowA';
				
			newField.attr({class: newRowType});
			$(this).before(newField);
			$.setFieldBehaviors();
		}); //end function newField.click()
		
		$(".cloneField").click(function () {
			fieldClone = $(this).parent().clone();
			
			var prevClass = $(this).parent().attr('class');
			//alert("prevClass = " + prevClass);
			var prevClassArray = new Array();
			prevClassArray = prevClass.split(' ');
			var prevRowType = prevClassArray[1];
			var prevClass = prevClassArray[0];
			var newRowType;
			
			if(prevRowType == 'rowA')
				newRowType = 'field rowB';
			else
				newRowType = 'field rowA';
				
			fieldClone.attr({class: newRowType});
			
			oldSQLdatatype = $(this).parent().find(".sqldatatype select").val();
			fieldClone.find(".sqldatatype select").val(oldSQLdatatype);

			oldEntryMethod = $(this).parent().find(".entrymethod select").val();
			fieldClone.find(".entrymethod select").val(oldEntryMethod);
			
			oldFrontList = $(this).parent().find(".frontlist select").val();
			fieldClone.find(".frontlist select").val(oldFrontList);
			
			oldBackList = $(this).parent().find(".backlist select").val();
			fieldClone.find(".backlist select").val(oldBackList);

			$(this).parent().after(fieldClone);
			
			$.setFieldBehaviors();
		}); //end .cloneField function
		
		$(".deleteField").click(function() {
			numOfFields = $(this).parent().parent().find(".field").length;
			//alert(numOfFields + " fields");
			if (numOfFields <= 1){
				//alert("last field...deleting table");
				$(this).parent().parent().remove();
			} else
				$(this).parent().remove();
		}); //end .deleteField function
		
		$(".fieldCollapse").toggle(
			function () {
				//alert($(this).attr("class"));
				$(this).html("&nbsp;+");
				$(this).parent().find(".deleteField").hide();
				$(this).parent().find(".cloneField").hide();
				$(this).parent().find(".fieldContents").hide("fast");
			},
			function () {
				//alert($(this).attr("class"));
				$(this).html("&#151;");
				$(this).parent().find(".deleteField").show();
				$(this).parent().find(".cloneField").show();
				$(this).parent().find(".fieldContents").show("fast");
			}
		); //end .fieldCollapse function

		$(".addOption").click(function() {
			$(this).before(optionHTML);
			$.setFieldBehaviors();
		}); //end .addOption function
		
		$(".deleteOption").click(function() {
			numOfOpts = $(this).parent().parent().children().length - 2;
			//alert(numOfOpts + " options");
			if (numOfOpts <= 1) {
				$(this).parent().parent().parent().find(".entrymethod select").val('textField');
				$(this).parent().parent().remove();
			} else
				$(this).parent().remove();		
		}); //end .deleteOption function
		
		$(".entrymethod select").bind("change", function(){
			selectedValue = $(this).attr('value');
			previousSelection = $(this).parent().next().attr('class');
			if ((previousSelection == "optionsContainer") || (previousSelection == "thumbChild") || (previousSelection == "relatedChild") || (previousSelection == "linkedChildTable")) {
				$(this).parent().next().remove();
			}

			switch (selectedValue) {
				case "dropDown":
					var optsExist = $(this).parent().next().hasClass("optionsContainer");
					//if an options list doesn't exist already -> make one
					if (!optsExist) {
						//alert("creating opts list");
						$(this).parent().after(optContainerStr);
						$(this).parent().next().find('.addOption').before(optionHTML);
						$.setFieldBehaviors();	
					}	
					break;
				case "fileUploadCrop":
					$(this).parent().after(thumbFieldHTML);
					break;
				case "dropDown_related":
					$(this).parent().after(relatedChildHTML);
					break;
				case "date":
					if (selectedValue == 'date') {
						currentField = $(this).parent().parent();
						currentField.find(".sqldatatype select").val('DATE');
						currentField.find(".frontlist select").val('Date');
						currentField.find(".backlist select").val('Date');	
					} 
					break;
				case "dateTime":
					if (selectedValue == 'dateTime') {
						currentField = $(this).parent().parent();
						currentField.find(".sqldatatype select").val('DATETIME');
						currentField.find(".frontlist select").val('Date Time');
						currentField.find(".backlist select").val('Date Time');	
					}
								case "groupItemList":
					
					break;
				case "linkedTableList":
					$(this).parent().after(linkedTableHTML);
					$(".link_child_field").blur( function () {
						parent_fieldname = $(this).parent().parent().parent().find("[@name=field]").val();
						child_tablename = $(this).parent().find(".link_child_table").val();
						link_tablename = parent_fieldname + "_LINK_" + child_tablename;
						$(this).parent().find("[@name=link_tablename]").val(link_tablename);
						//alert("Hello World!");
					} );
					break;
				default:
					break;
			} //end switch			
		}); //end dropDown select function
		
		$(".collapseAllFields").toggle(
			function () {
				//alert($(this).attr("class"));
				$(this).html("+ Expand All Fields");
				$(this).parent().find(".fieldCollapse").html("&nbsp;+");
				$(this).parent().find(".deleteField").hide();
				$(this).parent().find(".cloneField").hide();
				$(this).parent().find(".fieldContents").slideUp("fast");
				$.setFieldBehaviors();
			},
			function () {
				//alert($(this).attr("class"));
				$(this).html("- Collapse All Fields");
				$(this).parent().find(".fieldCollapse").html("&#151;");
				$(this).parent().find(".deleteField").show();
				$(this).parent().find(".cloneField").show();
				$(this).parent().find(".fieldContents").slideDown("fast");
				$.setFieldBehaviors();
			}		
		); //end .collapseAllFields function
		
	}; //end setFieldBehaviors()
	
	//#############################################################################
	//##################### 		END SET BEHAVIORS 		  #####################
	//#############################################################################

$(function() {
	var tableNum = 0;
	var fieldNum = 1;
	var globalNum = 1;
	
	var newTableStrHead = "<div class='table' id=\"";
	var newTableStrEnd = "\"><div class='nameField'>Table Name : <input class='inputField' type='text' name='tablename'></div>" + 
						"<div class='nameField'>Display Name : <input class='inputField' type='text' name='tabledisplayname'></div>"+
						"<div class='collapseBtn tableCollapse'>&#151;</div>" +
						"<div class='newField addButton'>+ New Field</div>"+
						"<div class='addButton collapseAllFields'>- Collapse All Fields</div><div></div>"+
						"<div class=\"cloneTable cloneButton\"> Clone Table</div>"+
						"<div class='deleteTable deleteButton'>- Table</div>"+
						"<input class='inputField' type='hidden' name='tableEnd'>"+
						"</div>";
	
	var newFieldStr = "<div class=\"field rowA\" id=\"\">" +
						"<div class='nameField'>Field Name : <input class='inputField' type='text' name='field'></div>" +
						"<div class='nameField'>Display Name : <input class='inputField' type='text' name='fielddisplayname'></div>" +
						"<div class='collapseBtn fieldCollapse'>&#151;</div>" + 
						"<div class='fieldContents'>" + 
							"<div class='sqldatatype'>SQL Data Type : <select class='inputField' name='sqldatatype'>" +
								"<option value='TEXT'>TEXT</option>" +
								"<option value='VARCHAR'>VARCHAR</option>" +
								"<option value='DATE'>DATE</option>" +
								"<option value='TIME'>TIME</option>" +
								"<option value='DATETIME'>DATETIME</option>" +
								"<option value='INT'>INT</option>" +
							"</select></div>" +
							"<div class='entrymethod'>Entry Method : <select class='inputField' id='select' name='entrymethod'>" +
								"<option value='textField'>Text Field</option>" +
								"<option value='textArea'>Text Area</option>" +
								"<option value='fileUpload'>File Upload</option>" +
								"<option value='fileUploadCrop'>File Upload-Crop</option>" +
								"<option value='dropDown'>Drop Down</option>" +
								"<option value='dropDown_related'>Drop Down Related</option>" +
								"<option value='date'>Date</option>" +
								"<option value='time'>Time</option>" +
								"<option value='dateTime'>Date Time</option>" +
								"<option value='groupItemList'>groupItemList</option>" +
								"<option value='linkedTableList'>linkedTableList</option>" +
								"<option value='hidden'>Hidden</option>" +
							"</select></div>" +
							"<div class='frontlist'>Front List : <select class='inputField' name='frontlist'>" + 
								"<option value='text'>Text</option>" + 
								"<option value='image'>Image</option>" + 
								"<option value='date'>Date</option>" +
								"<option value='time'>Time</option>" +
								"<option value='dateTime'>Date Time</option>" + 
								"<option value='movie'>Movie</option>" + 
								"<option value='relatedData'>Related Data</option>" +
								"<option value='none'>None</option>" + 
							"</select></div>" + 
							"<div class='backlist'>Back List : <select class='inputField' name='backlist'>" + 
								"<option value='text'>Text</option>" + 
								"<option value='image'>Image</option>" + 
								"<option value='date'>Date</option>" +
								"<option value='time'>Time</option>" +
								"<option value='dateTime'>Date Time</option>" + 
								"<option value='movie'>Movie</option>" +
								"<option value='relatedData'>Related Data</option>" +
								"<option value='none'>None</option>" + 
							"</select></div>" +
						"</div>" +
						"<div class=\"cloneField cloneButton\"> Clone Field</div>"+
						"<div class=\"deleteField deleteButton\">- Field</div>"+
						"<input class='inputField' type=\"hidden\" name=\"fieldEnd\">"+
					"</div>";
		
	var globalHTML = "<div class='globalVar'>Name : <input class='inputField' type='text' name='globalName'> Value : <input class='inputField' type='text' name='globalValue'><div class='deleteGlobalBtn deleteButton' id=''>Delete</div></div>";
	
	tableNum++;
	newTableStrFull = newTableStrHead + tableNum + newTableStrEnd;
	
	$("#newTable").before(newTableStrFull);
	$(".newField").before(newFieldStr);
	$.setTableBehaviors();

	$("#newTable").click(function() {
		tableNum++;
		newTableStrFull = newTableStrHead + tableNum + newTableStrEnd;
		$(this).before(newTableStrFull);
		$(this).prev().find(".newField").before(newFieldStr);
		
		$.setTableBehaviors();
		
		// prevent the default click
		return false;
		
	}); //end function newTable.click()
	
	
	$("#addGlobal").click(function() {
		$(this).before(globalHTML);
		
		newDelGlobalBtn = $(this).prev().find('.deleteGlobalBtn'); 	//find the NEW DELETE GLOBALS BUTTON
		newDelGlobalBtnId = "delGlobalBtn" + globalNum;				//determine the NEW DELETE GLOBALS BUTTON id and assign to var
		newDelGlobalBtn.attr({id: newDelGlobalBtnId});				//set the NEW DELETE GLOBALS BUTTON id
		globalNum++;
		
		$("#" + newDelGlobalBtnId).click(function() {
			$(this).parent().remove();
		}); //end function delGlobal.click()
	}); //end function addGlobal.click()
	
	$("#submitBtn").click(function() {
		var xmlContent = "";
		var optionString = "";
		var thumbFieldXML = "";
		var globalNameString, fieldNameString, fieldDisplayString, sqlDataTypeString, entryMethodStrHead, entryMethodStrEnd, frontlistString, backListString;	
		var optionsArray = [];
		
		$(".inputField").each(function () {
			var inputName = $(this).attr('name');
			var value = $(this).val();
			//alert("inputName : " + inputName +"\nvalue : " + value + "\n" + xmlContent);	
			//alert(xmlContent);		
			switch(inputName) {
				case "globalsStart":
					xmlContent += "\t<Globals>\n";
					break;
				case "globalName":
					globalNameString = value;
					break;
				case "globalValue":
					xmlContent += "\t\t<"+globalNameString+">"+value+"</"+globalNameString+">\n";
					break;
				case "globalsEnd":
					xmlContent += "\t</Globals>\n"
					break;
				case "sitename":
					siteNameString = "\n<site name=\""+value+"\">\n";
					xmlContent += siteNameString;
					break;
				case "tablename":
					tableNameString = "\t<table name=\""+value+"\"";
					xmlContent += tableNameString;
					break;
				case "tabledisplayname":
					tableDisplayNameString = " displayname=\""+value+"\">\n";
					xmlContent += tableDisplayNameString;
					break;
				case "field":
					fieldNameString = "\t\t<"+inputName+" name=\""+value+"\"";
					thumbParentName = value;
					break;
				case "fielddisplayname":
					fieldDisplayString = " displayname=\""+value+"\">\n";
					thumbParentDisplayName = value;
					break;
				case "sqldatatype":
					sqlDataTypeString = "\t\t\t<"+inputName+">"+value+"</"+inputName+">\n";
					break;		
				case "entrymethod":
					entryMethodStrHead = "\t\t\t<"+inputName;
					entryMethodStrBody = "";
					entryMethodStrEnd = ">"+value+"</"+inputName+">\n";
					break;
				case "width":
					entryMethodStrBody = " " + inputName+"=\""+value+"\" ";
					break;
				case "height":
					entryMethodStrBody += " " + inputName+"=\""+value+"\"";
					break;
				case "child_tablename":
					entryMethodStrBody = " " + inputName+"=\""+value+"\" ";
					break;
				case "child_fieldname":
					entryMethodStrBody += " " + inputName+"=\""+value+"\"";
					break;
				case "link_tablename":
					entryMethodStrBody += " " + inputName+"=\""+value+"\"";
					break;
				case "thumb_frontlist":
					thumbFrontList = value;
					break;
				case "thumb_backlist":
					thumbBackList = value;
					thumbFieldXML = "" + 
					"\t\t<field name=\"" + thumbParentName +"_thumb\" displayname=\"" + thumbParentDisplayName +" Thumbnail\">\n"+
						"\t\t\t<sqldatatype>TEXT</sqldatatype>\n" +
						"\t\t\t<entrymethod>fileUploadThumb</entrymethod>\n" +
						"\t\t\t<frontlist>"+thumbFrontList+"</frontlist>\n"+
						"\t\t\t<backlist>"+thumbBackList+"</backlist>\n"+
					"\t\t</field>\n";
					break;
				case "frontlist":
					frontlistString = "\t\t\t<"+inputName+">"+value+"</"+inputName+">\n";
					break;
				case "backlist":
					backListString = "\t\t\t<"+inputName+">"+value+"</"+inputName+">\n\t\t</field>\n";
					break;
				case "option":
					optionsArray.push(value);
					break;
				case "optionsEnd":
					break;
				case "globalsEnd":
					break; 
				case "fieldEnd":
					for ( var i in optionsArray ) {
						optionString += "\t\t\t<option>"+optionsArray[i]+"</option>\n";
						optionsArray[i] = null;
					}		
					xmlContent += fieldNameString + fieldDisplayString + optionString + sqlDataTypeString + entryMethodStrHead + entryMethodStrBody + entryMethodStrEnd + frontlistString + backListString + thumbFieldXML;
					thumbFieldXML = " ";
					optionsArray = [];
					optionString = "";
					break; 
				case "tableEnd":
					xmlContent += "\t</table>\n";
					break;
				case "siteEnd":
					xmlContent += "</site>\n";
					break;		
				case "submitBtn":					
					break;
				default:
					xmlContent += "!Unrecognized Node!: "+inputName+" / " +value+"<br/>";
			} //end switch statement
		}); //end $("input").each()
		
		//$("#submitBtn").before("<input type='hidden' name='xmlContent' value='"+xmlContent+"'>");
		
		$.ajax({
		   type: "POST",
		   url: "write_xml.php",
		   data: "xmlContent="+xmlContent,
		   success: function(html){
				$("#linkToXML").html(html);
				$("#buildMetaBtn").click(function() {
				alert('hey');
						$.ajax({
				   type: "POST",
				   url: "meta.php",
				   data: "xmlFile=newSpecs.xml",
				   success: function(html){
						alert(html);
				   }
			   });
		
			});
		   }
		});
		
		return false;
	}); //end #submitButton function

});
