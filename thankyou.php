<?php
require_once("admin/database.php");

// ############ START RECAPTCHA VALIDATION ############

require_once('recaptcha-php/recaptchalib.php');
$privatekey = "6Le10sASAAAAAEOu2MMUlf1jB19LQuBsQqVPWhK4";
$resp = recaptcha_check_answer ($privatekey,
                              $_SERVER["REMOTE_ADDR"],
                              $_POST["recaptcha_challenge_field"],
                              $_POST["recaptcha_response_field"]);
$captchaResponse = "";

$validResponse = (bool)$resp->is_valid;
if ($validResponse) {
  // Your code here to handle a successful verification

	$validCaptcha = true;
	$validCaptchaMsg = "Your captcha was entered correctly";
} else {
  // What happens when the CAPTCHA was entered incorrectly
  //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
  //     "(reCAPTCHA said: " . $resp->error . ")");
	$validCaptcha = false;
	$validCaptchaMsg = "Your captcha was invalid (reCAPTCHA said: " . $resp->error . ")";
	if ($_POST['recaptchaSubmit']){ $captchaResponse = "<p style='color:red;'>Sorry, but your response didn't match up. Please try again.</p>"; }
}

$validCaptchaMsg = "<h2>$validCaptchaMsg</h2>";

// ############ END RECAPTCHA VALIDATION ############

/* Convert Region Table to Array */
$SQLregionsQuery = "SELECT * FROM regions";
$SQLregionsResult = $db->query($SQLregionsQuery);
while ( $SQLregionsArray = $db->fetch_array($SQLregionsResult) ) {
	$regionName = $SQLregionsArray["regionName"];
	$regions[ $SQLregionsArray["id"] ] =
		array( "name" => $SQLregionsArray["regionName"],
						"managerName" => $SQLregionsArray["managerName"],
						"managerEmail" =>$SQLregionsArray["managerEmail"]);
}

/* Set all post args to variables */
$firstName = htmlentities($_POST["firstName"],ENT_QUOTES);
$lastName = htmlentities($_POST["lastName"],ENT_QUOTES);
$email = htmlentities($_POST["email"],ENT_QUOTES);
$addressL1 = htmlentities($_POST["street"],ENT_QUOTES);
$city = htmlentities($_POST["city"],ENT_QUOTES);
$state = htmlentities($_POST["state"],ENT_QUOTES);
$zipcode = htmlentities($_POST["zipcode"],ENT_QUOTES);
$phone = htmlentities($_POST["phone"],ENT_QUOTES);
$phone = format_phone($phone);
$contact = htmlentities($_POST["contact"],ENT_QUOTES);
$contactPref = ($contact == 1) ? "Yes" : "No"; 
$region = htmlentities($_POST["region"],ENT_QUOTES);
$memberRegion = $regions[$region]["name"];

$isRenewal = htmlentities($_POST["renewal"], ENT_QUOTES);

/* Set fingerprint to md5 of current time + email */
$fingerprint = $isRenewal ? htmlentities($_POST["fingerprint"],ENT_QUOTES) : md5(time().$email);

/* echo "<pre>".print_r($_POST, TRUE)."</pre>"; */


if ($isRenewal && $validCaptcha) { // if a forms been submitted and it is a renewal
	$isRenewalMsg = "<p>Form has been submitted, and its a renewal.</p>";
	$yearsEnrolled = htmlentities($_POST["yearsEnrolled"],ENT_QUOTES);
	$yearsEnrolled++; // add one year if someone is renewing
	$SQLrenewQuery = "UPDATE members SET ";	
	$SQLrenewQuery .= "  firstName = '$firstName',  lastName = '$lastName',  addressL1 = '$addressL1',  city = '$city',  state = '$state',  zipcode = '$zipcode',  phone = '$phone',  email = '$email',  contact = '$contact',  region = '$region',  yearsEnrolled = '$yearsEnrolled',  lastEnrollment = CURDATE(),  membershipStatus = '1',  paid = '0'   "; 
	$SQLrenewQuery .= " WHERE fingerprint = '$fingerprint' ";
/* 	echo "<pre>$SQLrenewQuery</pre>"; */
	$SQLrenewResult = $db->query($SQLrenewQuery);
		
	//require_once("emailConfirmation.php");
	//require_once("emailBoardMember.php");
}
/* Insert New Member to DB */
$hasPostData = (count($_POST) > 0 );
$newMember = !$isRenewal && ($validCaptcha && isset($_POST["email"]) && !isset($_POST["id"]) );
/* echo "<h2>hasPostData = " . (int)$hasPostData."</h2>"; */
/* echo "<p>isRenewal? : [".(int)$isRenewal."]"; */
/* echo "<p>Form submitted, and new member? : [".(int)$newMember."]"; */
if ($newMember && $validCaptcha) { // if a forms been submitted, but its not a renewal (i.e. new member)
	$isRenewalMsg = "<p>Form has been submitted, but its not a renewal.</p>";
	$SQLnewQuery = "INSERT INTO members (firstName, lastName, email, addressL1, city, state, zipcode, phone, contact, region, yearsEnrolled, lastEnrollment, membershipStatus, fingerprint, paid) VALUES ('$firstName', '$lastName', '$email', '$addressL1', '$city', '$state', '$zipcode', '$phone', '$contact', '$region', '1', CURDATE(), '1', '$fingerprint', '0')";
	$SQLinsertResult = $db->query($SQLnewQuery);
	
	//require_once("emailConfirmation.php");
	//require_once("emailBoardMember.php");
}

function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);

	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
	else
		return $phone;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Join NYSSSWA :: The New York School Social Workers Association</title>
<link href="css/kids_first.css" rel="stylesheet" type="text/css" media="all" />
<script type='text/javascript'>
<?php if ($_GET["paid"]) { echo "var paid = true;";} ?>
</script>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/register.js"></script>
</head>

<body>
<div id="wrapper">
	<div id="joinHeader"></div>
  <div id="layout_wrapper">
    <div id="left_spacer"><img src="images/l_gradient_overlay.jpg" width="22" height="261" /></div>
    <div id="right_spacer"><img src="images/r_gradient_overlay.jpg" width="21" height="263" /></div>
    
  <div id="content_wrapper">
    	<div id="main_content">
		  <?php
    	  //echo $isRenewalMsg;
    	  //echo $validCaptchaMsg;
    	  if ( $validCaptcha || !$hasPostData) { ?>
			    	<div id="membershipPayment">
			    	  <h1>Membership Payment</h1>
			    	  <?php
			    	  //echo $isRenewalMsg;
			    	  if ( isset($_POST['email']) ) {
			    	  	// Update the registration table
								$SQLregistrationQuery = "INSERT INTO registration (fingerprint, date) VALUES ( '$fingerprint', CURDATE() )";
								$db->query($SQLregistrationQuery);
			    	  ?>
								<h3>Thank You!</h3>
								<h3>&nbsp;</h3>
								<p>Your Information has been received successfully!</p>
							<?php
							}
							?>
							<p>You must now complete your Membership Application by selecting a Membership Type below, then making your credit card payment with Paypal.</p>
							<p id="paypalButton">
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="S8WLAQQE7M63C">
								<table>
								<tr><td><input type="hidden" name="on0" value="Membership Type"></td></tr><tr><td><select name="os0">
									<option value="Full Membership">Full Membership $75.00</option>
									<option value="Associate Membership">Associate Membership $65.00</option>
									<option value="Student / Retiree">Student / Retiree $45.00</option>
								</select> </td></tr>
								</table>
								<input type="hidden" name="currency_code" value="USD">
								<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form>
			    	  </p>
		    	  </div> <!-- end membershipPayment -->
				<?php
					require_once("log.php");
				} else {
				?>
		  <h1>Final Step</h1>
  		<div id="recaptchaTxt" class="formLabel">Please type the words that appear in the box below to verify your registration. If you are having trouble reading the words, click the refresh button (<img src="http://www.google.com/recaptcha/api/img/red/refresh.gif">) to generate a new pair. </div>

      <form method="post" action="">
        <?php
        	//echo $captchaResponse;
          require_once('recaptcha-php/recaptchalib.php');
          $publickey = "6Le10sASAAAAADeLLXhJj6r8daIpN1cVmLMvqoo6"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
          
					$firstName = htmlentities($_POST["firstName"],ENT_QUOTES);
					$lastName = htmlentities($_POST["lastName"],ENT_QUOTES);
					$email = htmlentities($_POST["email"],ENT_QUOTES);
					$addressL1 = htmlentities($_POST["street"],ENT_QUOTES);
					$city = htmlentities($_POST["city"],ENT_QUOTES);
					$state = htmlentities($_POST["state"],ENT_QUOTES);
					$zipcode = htmlentities($_POST["zipcode"],ENT_QUOTES);
					$phone = htmlentities($_POST["phone"],ENT_QUOTES);
					$contact = htmlentities($_POST["contact"],ENT_QUOTES);
					$region = htmlentities($_POST["region"],ENT_QUOTES);
					$yearsEnrolled = htmlentities($_POST["yearsEnrolled"],ENT_QUOTES);
					$fingerprint = htmlentities($_POST["fingerprint"],ENT_QUOTES);
					$renewal = htmlentities($_POST["renewal"],ENT_QUOTES);
					
					// prevent submission if there's no actual data
					if ( $firstName != "" ) {
						echo "<input type='hidden' name='firstName' value='$firstName'/>";
						echo "<input type='hidden' name='lastName' value='$lastName'/>";
						echo "<input type='hidden' name='email' value='$email'/>";
						echo "<input type='hidden' name='street' value='$addressL1'/>";
						echo "<input type='hidden' name='city' value='$city'/>";
						echo "<input type='hidden' name='state' value='$state'/>";
						echo "<input type='hidden' name='zipcode' value='$zipcode'/>";
						echo "<input type='hidden' name='phone' value='$phone'/>";
						echo "<input type='hidden' name='contact' value='$contact'/>";
						echo "<input type='hidden' name='region' value='$region'/>";
						echo "<input type='hidden' name='fingerprint' value='$fingerprint'/>";
						echo "<input type='hidden' name='yearsEnrolled' value='$yearsEnrolled'/>";
						if ($isRenewal) {echo "<input type='hidden' name='renewal' value='$renewal'/>";}
					}
        ?>
					<input type='hidden' name='recaptchaSubmit' value='true'/>
        <input type="submit" />
				<?php
				}
				?>
    	  <div id="paymentComplete">
	    	  <h1>Payment Complete</h1>
					<h3>Thanks for your payment!</h3>
					<h3>&nbsp;</h3>
					<p>We appreciate your continued support. You will receive your membership card and confirmation in the mail shortly.</p>
					<p>For any questions, comments, or concerns, please contact us at <a href="mailto:info@nyssswa.org">info@nyssswa.org</a>.</p>
    	  </div> <!-- end paymentComplete -->
      </div>
   	  <div id="navigation">
<div id="googlesearch"><!-- Google CSE Search Box Begins  -->
<form action="http://www.nyssswa.org/search.shtml" id="cse-search-box">
  <input type="hidden" name="cx" value="004633215117489972450:kxjchwu5hqa" />
    <input type="hidden" name="cof" value="FORID:11" />
  <input type="text" name="q" size="15" />
  <input type="submit" name="sa" value="Search" />
</form>
<!-- <script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&lang=en"></script> -->
<!-- Google CSE Search Box Ends --></div>

<?php include("main_nav.shtml"); ?>
<div id="wildcard">
<?php include("wildcard.shtml"); ?>
 </div>
      	</div>
      </div>
  </div>
    <div id="footer">
<?php include("footer.shtml"); ?>
    </div>
</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-206169-10");
pageTracker._trackPageview();
</script>
</body>
</html>
